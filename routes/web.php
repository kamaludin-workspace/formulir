<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm')->name('login');

Route::group(['middleware' => ['revalidate']], function (){
    Auth::routes();
});
Route::group(['middleware' => ['auth','revalidate']], function (){
    Route::get('home', 'HomeController@index')->name('home');
});

Route::group(['middleware' => ['auth','revalidate']], function (){
    Route::get('permohonan/{id}/kualifikasi', 'KualifikasiController@getcualification');
    Route::get('klasifikasi/{id}/subklasifikasi', 'SubKlasifikasiController@getsubcualification');
    Route::get('klasifikasi/{id}/subklasifikasi/{jenis}',
        'SubKlasifikasiController@getsubcualification');
});

Route::group(['middleware' => ['auth','revalidate']], function (){
    Route::get('subklasifikasi/{jenis}', 'SubKlasifikasiController@getsubcualificationmanual');
    Route::get('pengalaman/{klasifikasi}/tahun/{tahun}/daerah/{daerah}/limit/{limit}', 'TalinController@pengalamanManual');
});

// Json Daerah
Route::group(['middleware' => ['auth','revalidate']], function (){
    Route::prefix('daerah')->group(function () {
    Route::get('kabupaten/{id}', 'DaerahController@getkabupaten');
    Route::get('kecamatan/{id}', 'DaerahController@getkecamatan');
    Route::get('kelurahan/{id}', 'DaerahController@getkelurahan');
    });
});

Route::group(['middleware' => ['auth','revalidate']], function (){
    Route::prefix('pengajuan')->group(function () {
        Route::get('proses', 'PengajuanController@proses');
        Route::get('selesai', 'PengajuanController@selesai');
        Route::get('', 'PengajuanController@index');
        Route::get('cari', 'PengajuanController@cari');
        Route::get('form', 'PengajuanController@create');
        Route::get('form-ktp', 'PengajuanController@createktp');
        Route::post('checknik', 'PengajuanController@checkNik');
        Route::post('store', 'PengajuanController@store');
        Route::get('edit/{id}', 'PengajuanController@edit');
        Route::get('pengalaman/{id}', 'PengajuanController@show');
        Route::get('detail/{id}', 'PengajuanController@detail');
        Route::get('delete/{id}', 'PengajuanController@destroy');
        Route::post('pengalaman/store', 'PekerjaanController@create');
        Route::post('pengalaman/storemanual', 'PekerjaanController@createmanual');
        Route::match(['post', 'delete'], 'pengalaman/destroy/single/{id}', 'PekerjaanController@singleDestroy');
        Route::match(['put', 'patch'], 'updateStatus/{id}', 'PengajuanController@UpdateStatus');
        Route::match(['put', 'patch'], 'update/{id}', 'PengajuanController@update');
        Route::delete('deleteAll', 'PengajuanController@deleteAll');
    });
});

Route::group(['middleware' => ['auth','revalidate']], function (){
    Route::prefix('pengajuan/cetak')->group(function () {
        Route::get('formulir/{id}', 'PrintController@formulir');
        Route::get('suratpernyataan/{id}', 'PrintController@pernyataan');
        Route::get('suratpermohonan/{id}', 'PrintController@permohonan');
        Route::get('cv/{id}', 'PrintController@cv');
        Route::get('dokumen/{id}', 'PrintController@dokumen');
        Route::get('asosiasi/{id}', 'PrintController@asosiasi');
    });
});

Route::group(['middleware' => ['auth','revalidate']], function (){
    Route::prefix('laporan')->group(function () {
    Route::get('ustkm', 'LaporanController@ustkm');
    Route::post('ustkm/view', 'LaporanController@storeUstkm');
    Route::post('ustkm/cetak', 'LaporanController@getUstkm');

    Route::get('dpp', 'LaporanController@dpp');
    Route::post('dpp/view', 'LaporanController@storeDpp');
    Route::post('dpp/cetak', 'LaporanController@getDpp');

    Route::get('periode', 'LaporanController@periode');
    Route::post('periode/print', 'LaporanController@getPeriode');
    });
});

Route::group(['middleware' => ['auth','revalidate']], function (){
    Route::prefix('provinsi')->group(function(){
        Route::get('', 'ProvinsiController@index');
        Route::get('cari', 'ProvinsiController@cari');
        Route::get('tambah','ProvinsiController@create');
        Route::post('tambah/save', 'ProvinsiController@store');
        Route::get('edit/{id_provinsi}','ProvinsiController@edit');
        Route::patch('edit/{id_provinsi}/save', 'ProvinsiController@update');
        Route::delete('delete/{id_provinsi}', 'ProvinsiController@destroy');
    });
});


Route::group(['middleware' => ['auth','revalidate']], function (){
    Route::prefix('kabkota')->group(function(){
        Route::get('', 'KabkotaController@index');
        Route::get('cari', 'KabkotaController@cari');
        Route::get('tambah','KabkotaController@create');
        Route::post('tambah/save', 'KabkotaController@store');
        Route::get('edit/{id_kabkota}','KabkotaController@edit');
        Route::patch('edit/{id_kabkota}/save', 'KabkotaController@update');
        Route::delete('delete/{id_kabkota}', 'KabkotaController@destroy');
    });
});

Route::group(['middleware' => ['auth','revalidate']], function (){
    Route::prefix('kecamatan')->group(function(){
        Route::get('', 'KecamatanController@index');
        Route::get('cari', 'KecamatanController@cari');
        Route::get('tambah','KecamatanController@create');
        Route::post('tambah/save', 'KecamatanController@store');
        Route::get('edit/{id_provinsi}','KecamatanController@edit');
        Route::patch('edit/{id_provinsi}/save', 'KecamatanController@update');
        Route::delete('delete/{id_provinsi}', 'KecamatanController@destroy');
    });
});

Route::group(['middleware' => ['auth','revalidate']], function (){
    Route::prefix('keldes')->group(function(){
        Route::get('', 'KeldesController@index');
        Route::get('cari', 'KeldesController@cari');
        Route::get('tambah','KeldesController@create');
        Route::post('tambah/save', 'KeldesController@store');
        Route::get('edit/{id_keldes}','KeldesController@edit');
        Route::patch('edit/{id_keldes}/save', 'KeldesController@update');
        Route::delete('delete/{id_keldes}', 'KeldesController@destroy');
    });
});

Route::group(['middleware' => ['auth','revalidate']], function (){
    //route menajemen user
    Route::get('/pengguna', 'UserController@index');
    Route::get('/pengguna/tambah', 'UserController@create');
    Route::post('/pengguna/tambah/save', 'UserController@store');
    Route::get('/pengguna/edit/{id}', 'UserController@edit');
    Route::patch('/pengguna/edit/{id}/save', 'UserController@update');
    Route::delete('/pengguna/delete/{id}', 'UserController@destroy');

Route::group(['middleware' => ['auth','revalidate']], function(){
    Route::prefix('ska')->group(function(){
        Route::get('', 'SkaController@index');
        Route::get('cari', 'SkaController@cari');
        Route::get('tambah', 'SkaController@create');
        Route::post('tambah/save', 'SktController@store');
        Route::get('edit/{sk_id}', 'SkaController@edit');
        Route::patch('edit/{sk_id}/save', 'SktController@update');
        Route::delete('delete/{sk_id}', 'SktController@destroy');
    });
});

Route::group(['middleware' => ['auth','revalidate']], function(){
    Route::prefix('skt')->group(function(){
        Route::get('', 'SktController@index');
        Route::get('cari', 'SktController@cari');
        Route::get('tambah', 'SktController@create');
        Route::post('tambah/save', 'SktController@store');
        Route::get('edit/{sk_id}', 'SktController@edit');
        Route::patch('edit/{sk_id}/save', 'SktController@update');
        Route::delete('delete/{sk_id}', 'SktController@destroy');
    });
});
    // //route data ska
    // Route::get('/ska', 'SkaController@index');
    // Route::get('/ska/cari', 'SkaController@cari');
    // Route::get('/ska/tambah', 'SkaController@create');
    // Route::post('/ska/tambah/save', 'SktController@store');
    // Route::get('/ska/edit/{sk_id}', 'SkaController@edit');
    // Route::patch('/ska/edit/{sk_id}/save', 'SktController@update');
    // Route::delete('/ska/delete/{sk_id}', 'SktController@destroy');

    // //route data skt
    // Route::get('/skt', 'SktController@index');
    // Route::get('/skt/cari', 'SktController@cari');
    // Route::get('/skt/tambah', 'SktController@create');
    // Route::post('/skt/tambah/save', 'SktController@store');
    // Route::get('/skt/edit/{sk_id}', 'SktController@edit');
    // Route::patch('/skt/edit/{sk_id}/save', 'SktController@update');
    // Route::delete('/skt/delete/{sk_id}', 'SktController@destroy');

    //route data arsitektur
    Route::get('/arsitektur', 'ArsitekturController@index');
    Route::get('/arsitektur/cari', 'ArsitekturController@cari');
    Route::get('/arsitektur/tambah', 'ArsitekturController@create');
    Route::post('/arsitektur/tambah/save', 'ArsitekturController@store');
    Route::get('/arsitektur/edit/{rp_id}', 'ArsitekturController@edit');
    Route::patch('/arsitektur/edit/{rp_id}/save', 'ArsitekturController@update');
    Route::delete('/arsitektur/delete/{rp_id}', 'ArsitekturController@destroy');
    Route::get('/arsitektur/detail/{rp_id}', 'ArsitekturController@detail');

    //route data sipil
    Route::get('/sipil', 'SipilController@index');
    Route::get('/sipil/cari', 'SipilController@cari');
    Route::get('/sipil/tambah', 'SipilController@create');
    Route::post('/sipil/tambah/save', 'ArsitekturController@store');
    Route::get('/sipil/edit/{rp_id}', 'SipilController@edit');
    Route::patch('/sipil/edit/{rp_id}/save', 'ArsitekturController@update');
    Route::delete('/sipil/delete/{rp_id}', 'ArsitekturController@destroy');
    Route::get('/sipil/detail/{rp_id}', 'SipilController@detail');


    //route data mekanikal
    Route::get('/mekanikal', 'MekanikalController@index');
    Route::get('/mekanikal/cari', 'MekanikalController@cari');
    Route::get('/mekanikal/tambah', 'MekanikalController@create');
    Route::post('/mekanikal/tambah/save', 'ArsitekturController@store');
    Route::get('/mekanikal/edit/{rp_id}', 'MekanikalController@edit');
    Route::patch('/mekanikal/edit/{rp_id}/save', 'ArsitekturController@update');
    Route::delete('/mekanikal/delete/{rp_id}', 'ArsitekturController@destroy');
    Route::get('/mekanikal/detail/{rp_id}', 'MekanikalController@detail');

    //route data Elektrikal
    Route::get('/elektrikal', 'ElektrikalController@index');
    Route::get('/elektrikal/cari', 'ElektrikalController@cari');
    Route::get('/elektrikal/tambah', 'ElektrikalController@create');
    Route::post('/elektrikal/tambah/save', 'ArsitekturController@store');
    Route::get('/elektrikal/edit/{rp_id}', 'ElektrikalController@edit');
    Route::patch('/elektrikal/edit/{rp_id}/save', 'ArsitekturController@update');
    Route::delete('/elektrikal/delete/{rp_id}', 'ArsitekturController@destroy');
    Route::get('/elektrikal/detail/{rp_id}', 'ElektrikalController@detail');

    //route data Tata Lingkungan
    Route::get('/tata-lingkungan', 'TalinController@index');
    Route::get('/tata-lingkungan/cari', 'TalinController@cari');
    Route::get('/tata-lingkungan/tambah', 'TalinController@create');
    Route::post('/tata-lingkungan/tambah/save', 'ArsitekturController@store');
    Route::get('/tata-lingkungan/edit/{rp_id}', 'TalinController@edit');
    Route::patch('/tata-lingkungan/edit/{rp_id}/save', 'ArsitekturController@update');
    Route::delete('/tata-lingkungan/delete/{rp_id}', 'ArsitekturController@destroy');
    Route::get('/tata-lingkungan/detail/{rp_id}', 'TalinController@detail');

    //route data manajemen
    Route::get('/manajemen', 'ManajemenController@index');
    Route::get('/manajemen/cari', 'ManajemenController@cari');
    Route::get('/manajemen/tambah', 'ManajemenController@create');
    Route::post('/manajemen/tambah/save', 'ArsitekturController@store');
    Route::get('/manajemen/edit/{rp_id}', 'ManajemenController@edit');
    Route::patch('/manajemen/edit/{rp_id}/save', 'ArsitekturController@update');
    Route::delete('/manajemen/delete/{rp_id}', 'ArsitekturController@destroy');
    Route::get('/manajemen/detail/{rp_id}', 'ManajemenController@detail');
});

Route::group(['middleware' => ['auth','revalidate']], function (){
    Route::get('dokumentasi', function () {
        return view('dokumentasi.index');
    });
    Route::get('security', function () {
        return view('users.password');
    });
    Route::post('password', 'UserController@ChangePassword');
});
