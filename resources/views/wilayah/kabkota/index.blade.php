@extends('layouts.app')

@section('dropdown-wilayah') active
@endsection

@section('status-kabkota') active
@endsection

@section('title-page') Kabupaten/Kota
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="button">
                    <a href="/kabkota/tambah" class="btn btn-icon btn-primary"><i class="fa fa-plus"></i>
                        Tambah Data</a>
                </div>
            </div>
            <div class="card-header">
                <h4>Data Kabupaten/Kota</h4>
                <div class="card-header-form">
                    <form action="/kabkota/cari" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" name="cari" placeholder="Search"
                                value="{{ old('cari') }}">
                            <div class="input-group-btn">
                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-striped">

                        <tr>

                            <th>No</th>
                            <th>Provinsi</th>
                            <th>Nama Kabupaten</th>
                            <th>Kode Kabupaten</th>

                            <th>Aksi</th>
                        </tr>
                        <?php $no = $kabkota->firstItem(); ?>
                        @foreach ($kabkota as $t)

                        <tr>

                            <td>{{$no++}}.</td>
                            <td>{{$t->provinsi}}</td>
                            <td>{{$t->kabkota}}</td>
                            <td>{{$t->kode_kabkota}}</td>

                            @if (Auth::user()->level == "admin")

                            <td class="datatable-ct" align="center">
                                <a href="/kabkota/edit/{{ $t->id_kabkota }}" class="btn btn-info my-3">Edit</a>

                                <form method="POST" onclick="return confirm('Yakin Ingin Menghapus Data Ini ?')"
                                    action="/kabkota/delete/{{ $t->id_kabkota }}">
                                    {{csrf_field()}} {{method_field('DELETE')}}

                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                </form>
                            </td>

                            @endif



                        </tr>
                        @endforeach

                    </table>

                </div>
            </div>
            <div class="card-footer text-right">
                <nav class="d-inline-block">
                    {{ $kabkota->links() }}
                </nav>
            </div>
        </div>
    </div>
</div>

@endsection
