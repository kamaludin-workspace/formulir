@extends('layouts.app')

@section('dropdown-wilayah') active
@endsection

@section('status-provinsi') active
@endsection

@section('title-page') Provinsi
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="button">
                    <a href="/provinsi/tambah" class="btn btn-icon btn-primary"><i class="fa fa-plus"></i>
                        Tambah Data</a>
                </div>
            </div>
            <div class="card-header">
                <h4>Data Provinsi</h4>
                <div class="card-header-form">
                    <form action="/provinsi/cari" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" name="cari" placeholder="Search"
                                value="{{ old('cari') }}">
                            <div class="input-group-btn">
                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>No</th>
                            <th>Nama Provinsi</th>
                            <th>Kode Provinsi</th>

                            <th>Aksi</th>
                        </tr>
                        <?php $no = $prov->firstItem(); ?>
                        @foreach ($prov as $t)

                        <tr>

                            <td>{{$no++}}.</td>
                            <td>{{$t->provinsi}}</td>
                            <td>{{$t->kode_provinsi}}</td>

                            @if (Auth::user()->level == "admin")

                            <td class="datatable-ct" align="center">
                                <a href="/provinsi/edit/{{ $t->id_provinsi }}" class="btn btn-info my-3">Edit</a>

                                <form method="POST" onclick="return confirm('Yakin Ingin Menghapus Data Ini ?')"
                                    action="/provinsi/delete/{{ $t->id_provinsi }}">
                                    {{csrf_field()}} {{method_field('DELETE')}}

                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                </form>
                            </td>
                            @endif
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="card-footer text-right">
                <nav class="d-inline-block">
                    {{ $prov->links() }}
                </nav>
            </div>
        </div>
    </div>
</div>

@endsection
