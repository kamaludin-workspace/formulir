@extends('layouts.app')

@section('status-dashboard') active @endsection

@section('title-page') Dashboard @endsection

@section('content')
<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-primary">
                <i class="far fa-user"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>Pengajuan</h4>
                </div>
                <div class="card-body">
                    {{ number_format($pengajuan) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-danger">
                <i class="far fa-newspaper"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>Klasifikasi</h4>
                </div>
                <div class="card-body">
                    {{ number_format($klasifikasi) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-warning">
                <i class="far fa-file"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>Sub Klasifikasi</h4>
                </div>
                <div class="card-body">
                    {{ number_format($subklasifikasi) }}
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6 col-12">
        <div class="card card-statistic-1">
            <div class="card-icon bg-success">
                <i class="fas fa-circle"></i>
            </div>
            <div class="card-wrap">
                <div class="card-header">
                    <h4>Riwayat <small>Pekerjaan</small> </h4>
                </div>
                <div class="card-body">
                    {{ number_format($pengalaman) }}
                </div>
            </div>
        </div>
    </div>
</div>
{{-- <div class="row">
<div class="col-md-12">
  <div class="card">
    <div class="card-header">
      <h4>Activities</h4>
      <div class="card-header-action">
        <a href="#" class="btn btn-danger">View More <i class="fas fa-chevron-right"></i></a>
      </div>
    </div>
    <div class="card-body p-0">
      <div class="table-responsive table-invoice">
        <table class="table table-striped">
          <tr>
            <th>Action</th>
            <th>Due Date</th>
          </tr>
          <tr>
            <td class="font-weight-600">Kusnadi mencetak resi</td>
            <td>July 19, 2018</td>
          </tr>
          <tr>
            <td class="font-weight-600">Hasan Basri masak nasi</td>
            <td>July 21, 2018</td>
          </tr>
          <tr>
            <td class="font-weight-600">Muhamad Nuruzzaki mengetik kalimat yang sukar dipahami</td>
            <td>July 22, 2018</td>
          </tr>
          <tr>
            <td class="font-weight-600">Agung Ardiansyah menambah 2 pengajuan baru</td>
            <td>July 22, 2018</td>
          </tr>
          <tr>
            <td class="font-weight-600">Ardian Rahardiansyah menghapus riwayat pengajuan</td>
            <td>July 28, 2018</td>
          </tr>
        </table>
      </div>
    </div>
  </div>
</div>
</div> --}}
@endsection
