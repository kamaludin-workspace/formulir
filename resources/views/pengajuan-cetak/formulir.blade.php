<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Formulir Permohonan SKA/SKT</title>
  </head>
  <style media="screen">
    .logo-left{width:25%;float:left;text-align:center}.title-center{width:50%;float:left;text-align:center}.logo-right{width:25%;text-align:center;float:left}.bold{font-weight:700}.clear-left{clear:left}.title-center h3{padding-top:20px}.border-less{border:0!important}.tg{border-spacing:1;font-size:12px!important}.tg td{border-color:#000;border-style:solid;border-width:1px;font-family:Arial,sans-serif;font-size:14px;overflow:hidden;padding:2px 5px;word-break:normal}.tg th{border-color:#000;border-style:solid;border-width:1px;font-family:Arial,sans-serif;font-size:14px;font-weight:400;overflow:hidden;padding:4px 5px;word-break:normal}.tg .tg-0lax{text-align:left;vertical-align:top}.sg{border-spacing:2;width:100%}.sg td{border-color:#000;border-style:solid;border-width:1px;font-family:Arial,sans-serif;font-size:14px;overflow:hidden;padding:4px 9px;word-break:normal}.sg th{border-color:#000;border-style:solid;border-width:1px;font-family:Arial,sans-serif;font-size:14px;font-weight:400;overflow:hidden;padding:4px 9px;word-break:normal}.sg .sg-0pky{border-color:inherit;text-align:left;vertical-align:top}.uppercase{text-transform: uppercase;}

  </style>
  <body>
    <div class="container">
      <div class="logo-left">
        @if($pengajuan->no_reg_asosiasi == '152')
        <img width="150px" src="http://localhost:8888/ataksi/formataksi/public/img/ataksi.jpg" alt="Logo Asosiasi">
        @elseif($pengajuan->no_reg_asosiasi == '144')
          <img width="150px" src="http://localhost:8888/ataksi/formataksi/public/img/asdamkindo.jpg" alt="Logo Asosiasi">
        @elseif($pengajuan->no_reg_asosiasi == '180')
          <img width="150px" src="http://localhost:8888/ataksi/formataksi/public/img/proteksi.jpg" alt="Logo Asosiasi">
        @endif
      </div>
      <div class="title-center">
        <h2 style="margin-top: -5px; font-size: 1.3em;">FORMULIR PERMOHONAN SKA/SKT</h2>
        <p style="">
          Nomor Permohonan : __________________ <br>
          Tanggal Permohonan : <u>{{ tanggal_indonesia($pengajuan->tgl_surat, false) }}</u> </span>
        </p>
      </div>
      <div class="logo-right">
        {{-- <img width="150px" src="http://localhost:8888/ataksi/formataksi/public/img/default.png" alt="Logo FOto"> --}}
      </div>
    </div>
    <hr class="clear-left">
    <div class="content">
      <table class="tg">
        <tr>
          <th class="tg-0lax" rowspan="2" style="border: 0px;" >Permohonan<br> <small> <i>(Centang yang perlu)</i> </small> </th>
          <th class="tg-0lax" style="font-family: DejaVu Sans, sans-serif; width:20px;text-align: center;">@if($pengajuan->tipe_permohonan == 'ska') ✔ @endif</th>
          <th class="tg-0lax" style="width: 350px; border: 0px;"><span style="font-weight:bold">SKA</span> (Sertifikat Tenaga Ahli)</th>
        </tr>
        <tr>
          <td class="tg-0lax" style="font-family: DejaVu Sans, sans-serif; width:20px;text-align: center;">@if($pengajuan->tipe_permohonan == 'skt') ✔ @endif</td>
          <td class="tg-0lax" style="border: 0px;"><span style="font-weight:bold">SKT</span> (Sertifikat Tenaga Terampil)</td>
        </tr>
      </table>
      <br>
      <table class="sg">
        <tr>
          <th class="sg-0pky" style="border: 0px">Nama Lengkap</th>
          <th class="sg-0pky uppercase" colspan="4">{{ $pengajuan->nama_pemohon }}</th>
        </tr>
        <tr>
          <td class="sg-0pky border-less">Tempat Lahir</td>
          <td class="sg-0pky uppercase" colspan="2">{{ $pengajuan->tempat_lahir }}</td>
          <td class="sg-0pky border-less">Tgl lahir <small>('D/M/Y')</small> </td>
          <td class="sg-0pky uppercase">{{ date('d-m-Y', strtotime($pengajuan->tgl_lahir)) }}</td>
        </tr>
        <tr>
          <td class="sg-0pky border-less" rowspan="3">Alamat</td>
          <td class="sg-0pky uppercase" colspan="4">{{ $pengajuan->alamat }}</td>
        </tr>
        <tr>
          <td class="sg-0pky border-less">Kelurahan</td>
          <td class="sg-0pky uppercase">{{ $daerah[0]->keldes }}</td>
          <td class="sg-0pky border-less">Kecamatan</td>
          <td class="sg-0pky uppercase">{{ $daerah[0]->kecamatan }}</td>
        </tr>
        <tr>
          <td class="sg-0pky border-less">Kab/Kota</td>
          <td class="sg-0pky uppercase">{{ $daerah[0]->kabkota }}</td>
          <td class="sg-0pky border-less">Provinsi</td>
          <td class="sg-0pky uppercase">{{ $daerah[0]->provinsi }}</td>
        </tr>
        <tr>
          <td class="sg-0pky border-less">No. KTP</td>
          <td class="sg-0pky" colspan="4">{{ $pengajuan->nik }}</td>
        </tr>
        <tr>
          <td class="sg-0pky border-less">No. NPWP</td>
          <td class="sg-0pky" colspan="4">{{ $pengajuan->npwp }}</td>
        </tr>
        <tr>
          <td class="sg-0pky border-less">No. Reg <small>Asosiasi</small></td>
          <td class="sg-0pky uppercase" colspan="4">{{ $pengajuan->no_reg_asosiasi }}</td>
        </tr>
        <tr>
          <td class="sg-0pky border-less">Telp/Hp</td>
          <td class="sg-0pky uppercase" colspan="4">{{ $pengajuan->no_hp }}</td>
        </tr>
        <tr>
          <td class="sg-0pky border-less">Email</td>
          <td class="sg-0pky" colspan="4">{{ $pengajuan->email }}</td>
        </tr>
      </table>
      <p class="bold">Klasifikasi dan Kualifikasi SKA <small> <i>(Centang yang perlu)</i> </small> </p>
      <table class="tg">
        <tr>
          <th class="tg-0lax" style="font-family: DejaVu Sans, sans-serif; width:20px;text-align: center;">✔</th>
          <th class="tg-0lax" style="width: 350px; border: 0px;">Permohonan Baru > Daftar yang dimohonkan terlampir</th>
        </tr>
        <tr>
          <td class="tg-0lax" style="font-family: DejaVu Sans, sans-serif; width:20px;text-align: center;"></td>
          <td class="tg-0lax" style="border: 0px;">Permohonan Tambahan > Daftar yang dimohonkan terlampir</td>
        </tr>
        <tr>
          <td class="tg-0lax" style="font-family: DejaVu Sans, sans-serif; width:20px;text-align: center;"></td>
          <td class="tg-0lax" style="border: 0px;">Permohonan perubahan > Daftar yang dimohonkan terlampir</td>
        </tr>
        <tr>
          <td class="tg-0lax" style="font-family: DejaVu Sans, sans-serif; width:20px;text-align: center;"></td>
          <td class="tg-0lax" style="border: 0px;">Permohonan konversi > Daftar yang dimohonkan terlampir</td>
        </tr>
        <tr>
          <td class="tg-0lax" style="font-family: DejaVu Sans, sans-serif; width:20px;text-align: center;"></td>
          <td class="tg-0lax" style="border: 0px;">Permohonan perpanjangan > Daftar yang dimohonkan terlampir</td>
        </tr>
      </table>
      <p class="bold">Curriculum vitae <small> <i>(Centang yang ada)</i> </small> </p>
      <table class="tg">
        <tr>
          <th class="tg-0lax" style="font-family: DejaVu Sans, sans-serif; width:20px;text-align: center;">✔</th>
          <th class="tg-0lax" style="width: 350px; border: 0px;">Riwayat pendidikan > terlampir</th>
        </tr>
        <tr>
          <td class="tg-0lax" style="font-family: DejaVu Sans, sans-serif; width:20px;text-align: center;"></td>
          <td class="tg-0lax" style="border: 0px;">Riwayat kursur & pelatihan > terlampir</td>
        </tr>
        <tr>
          <td class="tg-0lax" style="font-family: DejaVu Sans, sans-serif; width:20px;text-align: center;"></td>
          <td class="tg-0lax" style="border: 0px;">Riwayat  pengalaman organisasi / perusahaan / instansi > terlampir</td>
        </tr>
        <tr>
          <td class="tg-0lax" style="font-family: DejaVu Sans, sans-serif; width:20px;text-align: center;">✔</td>
          <td class="tg-0lax" style="border: 0px;">Riwayat pengalaman proyek > terlampir</td>
        </tr>
        <tr>
          <td class="tg-0lax" style="font-family: DejaVu Sans, sans-serif; width:20px;text-align: center;"></td>
          <td class="tg-0lax" style="border: 0px;">Riwayat study kasus & Karya tulis terlampir</td>
        </tr>
      </table>

    </div>
  </body>
</html>
