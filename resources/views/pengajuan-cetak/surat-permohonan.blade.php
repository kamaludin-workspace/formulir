<html>

<head>
    <title>Surat Permohonan</title>
    <style type="text/css">
        .tg{width:100%;border-collapse:collapse}.tg td{font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000}.tg th{font-family:Arial,sans-serif;font-size:14px;font-weight:400;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000}.tg .tg-xi66{font-weight:700;background-color:#8fd8ff;text-align:center;vertical-align:top}.tg .tg-e76x{text-align:left;vertical-align:top}.tg .tg-b06p{background-color:#8fd8ff;text-align:left;vertical-align:top}.tg .tg-0lax{text-align:left;vertical-align:top}
    </style>
</head>

<body>
    <div class="content">
        <p>{{ tanggal_indonesia($pengajuan->tgl_surat, false) }}</p>
        <p>
            Nomor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<br>
            Lampiran&nbsp;&nbsp;:
        </p>
        <p>
            Kepada Yth, <br>
            Manajer Eksekutif <br>
            Lembaga Pengembangan Jasa Konstruksi Provinsi Riau
        </p>
        <p>
            Di &mdash; <br>
            <span style="padding-left: 45px;">Pekanbaru</span>
        </p>
        <br>
        <p>
            Perihal <span style="padding-left: 50px;">:&nbsp;&nbsp;<span style="font-weight: bold; text-decoration: underline;">Permohonan <span style="text-transform: uppercase">{{ $pengajuan->tipe_permohonan }}</span></span>
        </p>
        <p>Dengan hormat,</p>
        <p>
            Dengan ini kami mengajukan permohonan untuk mendapatkan <span style="text-transform: uppercase">{{ $pengajuan->tipe_permohonan }}</span> untuk :
        </p>
        <table class="tg">
            <tr>
                <th class="tg-xi66" style="text-align: center; vertical-align:middle">No.</th>
                <th class="tg-b06p" style="text-align: center; vertical-align:middle">Kode Kualifikasi</th>
                <th class="tg-b06p" style="text-align: center; vertical-align:middle">Klasifikasi</th>
                <th class="tg-b06p" style="vertical-align:middle">Sub Klasifikasi</th>
                <th class="tg-b06p" style="vertical-align:middle; text-align: center;">Kode Klasifikasi</th>
                <th class="tg-b06p" style="vertical-align:middle">Kualifikasi</th>
                <th class="tg-b06p" style="vertical-align:middle">Ket.</th>
            </tr>
            @php($no = 1)
            @php($countAr = 0)
            @php($k = 0)
            @foreach ($kualifikasi as $value)
              <tr>
                  <td class="tg-0lax" style="text-align: center">{{ $no++ }}.</td>
                  <td class="tg-0lax" style="text-align: center">{{ $value->sk_klasifikasi }}</td>
                  <td class="tg-0lax" style="text-align: center">@switch($value->sk_kl_id)
                      @case(1)
                      <span class="badge badge-primary">Arsitektur</span>
                      @break
                      @case(2)
                      <span class="badge badge-secondary">Mekanikal</span>
                      @break
                      @case(3)
                      <span class="badge badge-success">Elektrikal</span>
                      @break
                      @case(4)
                      <span class="badge badge-danger">Tata Lingkungan</span>
                      @break
                      @case(5)
                      <span class="badge badge-warning">Manajemen</span>
                      @break
                      @case(6)
                      <span class="badge badge-info">Sipil</span>
                      @break

                      @default
                      <span class="badge badge-outline-danger">Kesalahan</span>
                      @endswitch</td>
                  <td class="tg-e76x">{{ $value->sk_nama }}</td>
                  <td class="tg-e76x" style="text-align: center">{{ $value->sk_kode }}</td>
                  <td class="tg-0lax">
                      {{ Kualifikasi(($pengajuan->id_kualifikasi[$k++]), $pengajuan->no_reg_asosiasi) }}
                  </td>
                  <td class="tg-0lax">{{ $pengajuan->keterangan[$countAr++] }}</td>
              </tr>
            @endforeach

        </table>
        <p>
            Sesuai dengan dokumen terlampir. <br>
            Demikian permohonan ini kami sampaikan dan atas perhatian kami ucapkan terima kasih.
        </p>
        <div style="text-align: right;">
            <p>Hormat kami,<br>
                Pemohon
            </p>
            <br> <br> <br>
            <p>{{ $pengajuan->nama_pemohon }}</p>
        </div>
    </div>
</body>

</html>
