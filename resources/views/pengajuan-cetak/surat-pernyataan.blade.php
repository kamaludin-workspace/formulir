<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
    <title>Surat Pernyataan Kebenaran Data</title>
    <style type="text/css">
      body{font-family:Arial,sans-serif}.title{font-weight:700;font-family:Arial,sans-serif;font-size:17px;text-align:center}.content{padding-top:20px}.tg{border-collapse:collapse;border-spacing:0}.tg td{font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000}.tg th{font-family:Arial,sans-serif;font-size:14px;font-weight:400;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000}.tg .tg-nu5p{background-color:#fff;color:#000;border-color:#fff;text-align:left;vertical-align:top}.tg .tg-8xd9{background-color:#fff;color:#000;border-color:#fff;text-align:left;vertical-align:middle}.ng{border-collapse:collapse;border-spacing:0}.ng td{font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000}.ng th{font-family:Arial,sans-serif;font-size:14px;font-weight:400;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000}.ng .ng-fbeb{background-color:#fff;color:#333;border-color:#fff;text-align:left;vertical-align:top}.uppercase{text-transform: uppercase;}
    </style>
</head>
<body>
    <div>
        <p class="title">SURAT PERNYATAAN KEBENARAN DATA</p>

        <div class="content">
            <p>Yang bertanda tangan dibawah ini:</p>
            <table class="tg" width="100%">
                <tr>
                    <th class="tg-8xd9" style="width: 120px">Nama</th>
                    <th class="tg-8xd9" style="width: 5px">:</th>
                    <th class="tg-nu5p uppercase">{{ $pengajuan->nama_pemohon }}</th>
                </tr>
                <tr>
                    <td class="tg-8xd9">Jabatan</td>
                    <td class="tg-8xd9">:</td>
                    <td class="tg-nu5p uppercase">{{ $kualifikasi[0]->sk_nama }}</td>
                </tr>
                <tr>
                    <td class="tg-nu5p">Alamat</td>
                    <td class="tg-nu5p">:</td>
                    <td class="tg-nu5p uppercase">{{ $pengajuan->alamat }}</td>
                </tr>
                <tr>
                    <td class="tg-nu5p">No. KTP</td>
                    <td class="tg-nu5p">:</td>
                    <td class="tg-nu5p">{{ $pengajuan->nik }}</td>
                </tr>
                <tr>
                    <td class="tg-nu5p">Telp/HP</td>
                    <td class="tg-nu5p">:</td>
                    <td class="tg-nu5p">{{ $pengajuan->no_hp }}</td>
                </tr>
                <tr>
                    <td class="tg-nu5p">E-Mail</td>
                    <td class="tg-nu5p">:</td>
                    <td class="tg-nu5p">{{ $pengajuan->email }}</td>
                </tr>
                <tr>
                    <td class="tg-nu5p">NRTA</td>
                    <td class="tg-nu5p">:</td>
                    <td class="tg-nu5p"></td>
                </tr>
            </table>
            <p>Menyatakan dengan sesungguhnya bahwa :</p>
            <table style="padding-left: 20px; font-weight: 120%;">
                <tr>
                    <td></td>
                    <td class="reset" valign="top">a.</td>
                    <td valign="top">Seluruh data dalam dokumen yang disampaikan adalah benar</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td valign="top">b.</td>
                    <td valign="top" align="justify">Bersedia mematuhi Kode Etik dan Kode Tata Laku serta semua ketentuan yang telah ditetapkan oleh Pemerintah/LPJK/Asosiasi Profesi</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td valign="top">c.</td>
                    <td valign="top" align="justify">Apabila kemudian hari ditemukan bahwa data dalam dokumen yang diberikan tidak benar dan / atau melanggar kode etik dan kode tata laku serta melanggar ketentuanyang telah ditetapkan oleh
                        Pemerintah/LPJK/Asosiasi Profesi, saya,bersedia
                        dikenakansanksi sesuai ketentuan dan diinformasikan melalui SIKI-LPJK Nasional</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </table>
            <p>Demikian pernyataan ini dibuat dengan sebenarnya dan penuh tanggung jawab, untuk dipergunakan sebagaimana mestinya.</p>

            <div style="padding-right: 20px; text-align: right;">
                <p>{{ $daerah[0]->provinsi }}, {{ tanggal_indonesia($pengajuan->tgl_surat, false) }}<br> <small>Yang membuat pernyataan,</small></p>
                <br><br> <br> <br>
                <p class="uppercase">{{ $pengajuan->nama_pemohon }}</p>
            </div>
        </div>
    </div>
</body>

</html>
