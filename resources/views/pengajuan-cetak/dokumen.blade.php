<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Hasil Pemeriksaan Kelengkapan dan Kebenaran Data Dokumen Permohonan SKT</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
        body {
            font-family: 'Helvetica';
        }
        p .title {
            font-size: 15px;
            padding-left: 10px;
        }

        .info tr td {
            font-size: 18px;
        }

        .info td {
            padding-top: 7px;
            padding-bottom: 7px;
        }

        .body {
            font-size: 12pt;
        }
        .img-lpjk{width:20%;float:left; margin-right: 20px;}.-dokumen{width:70%;float:left}.clear{clear:left}.{font-size:20px}.height{line-height:190%;font-size:18px;margin-top:30px}.ttd{width:240px;text-align:center}.tg{border-collapse:collapse;border-spacing:0}.tg td{font-family:Arial,sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000}.tg th{font-family:Arial,sans-serif;font-size:14px;font-weight:400;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000}.tg .tg-cly1{text-align:left;vertical-align:middle}.tg .tg-baqh{text-align:center;vertical-align:top}.tg .tg-nrix{text-align:center;vertical-align:middle}.tg .tg-0lax{text-align:left;vertical-align:top}
    </style>
</head>

<body>
    <div class="content">
        <div class="img-lpjk">
            <img width="140" src="http://localhost:8888/ataksi/formataksi/public/img/lpjk.png" alt="Jika gambar tidak muncul, perbaiki link gambar">
        </div>
        <div class="title-dokumen">
            <p class="title">
                HASIL PEMERIKSAAN <br>
                KELENGKAPAN DAN KEBENARAN <br>
                DATA DOKUMEN PERMOHONAN SKT
            </p>
        </div>
        <div class="clear">
            <table class="info" style="margin-top: 60px;">
                <tr>
                    <td>Tanggal</td>
                    <td style="padding-left: 35px">:</td>
                    <td style="font-weight: bold;">
                        {{ $pengajuan->tgl_surat }}
                    </td>
                </tr>
                <tr>
                    <td>ASOSIASI</td>
                    <td style="padding-left: 35px">:</td>
                    <td style="font-weight: bold;">
                        {{ $pengajuan->no_reg_asosiasi }}
                    </td>
                </tr>
                <tr>
                    <td>No./Tgl Surat</td>
                    <td style="padding-left: 35px">:</td>
                    <td>{{ $pengajuan->nomor_surat }}/{{ $pengajuan->tgl_surat }}</td>
                </tr>
                <tr>
                    <td>Nama Pemohon</td>
                    <td style="padding-left: 35px;">:</td>
                    <td style="text-transform: uppercase;">{{ $pengajuan->nama_pemohon }}</td>
                </tr>
                <tr>
                    <td>ID Personal</td>
                    <td style="padding-left: 35px">:</td>
                    <td>{{ $pengajuan->nik }}</td>
                </tr>
                <tr>
                    <td>Tgl Permohonan</td>
                    <td style="padding-left: 35px">:</td>
                    <td>{{ $pengajuan->tgl_surat }}</td>
                </tr>
                <tr>
                    <td>Pemeriksa</td>
                    <td style="padding-left: 35px;">:</td>
                    <td width="350" style="text-transform: uppercase;">{{ $pengajuan->pemeriksa }}</td>
                </tr>
                <tr>
                    <td>Tempat USTK </td>
                    <td style="padding-left: 35px">:</td>
                    <td>
                        @switch($kualifikasi[0]->sk_kl_id)
                      @case(1)
                      {{-- Arsitektur --}}
                      <span class="badge badge-primary">116</span>
                      @break
                      @case(2)
                      {{-- Mekanikal --}}
                      <span class="badge badge-secondary">118</span>
                      @break
                      @case(3)
                      {{-- Elektrikal --}}
                      <span class="badge badge-success">119</span>
                      @break
                      @case(4)
                      {{-- Tata Lingkungan --}}
                      <span class="badge badge-danger">111</span>
                      @break
                      @case(5)
                      {{-- Manajemen --}}
                      <span class="badge badge-warning">112</span>
                      @break
                      @case(6)
                      {{-- Sipil --}}
                      <span class="badge badge-info">117</span>
                      @break

                      @default
                      <span class="badge badge-outline-danger">Kesalahan</span>
                      @endswitch
                    </td>
                </tr>
            </table>

        </div>
        <div>
            <table class="tg" style="font-size: 15pt;">
                <tr style="border: 0px">
                    <th class="tg-nrix" style="border: 0px">NO</th>
                    <th class="tg-nrix" style="border: 0px">Dokumen</th>
                    <th class="tg-nrix" style="border: 0px">Ada</th>
                    <th class="tg-nrix" style="border: 0px">Tidak</th>
                    <th class="tg-nrix" style="border: 0px">Valid</th>
                    <th class="tg-nrix" style="border: 0px">Tidak</th>
                    <th class="tg-baqh" style="border: 0px">Keterangan</th>
                </tr>
                <tr>
                    <td class="tg-nrix">1.</td>
                    <td class="tg-cly1">Surat Pernyataan Kebenaran Data Pemohon</td>
                    <td class="tg-cly1" style="font-family: DejaVu Sans, sans-serif; text-align: center;">v</td>
                    <td class="tg-cly1"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-cly1"></td>
                    <td class="tg-0lax"></td>
                </tr>
                <tr>
                    <td class="tg-nrix">2.</td>
                    <td class="tg-cly1">Photo Copy KTP</td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-cly1"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-cly1"></td>
                    <td class="tg-0lax"></td>
                </tr>
                <tr>
                    <td class="tg-baqh">3.</td>
                    <td class="tg-0lax">Photo Copy NPWP Perorangan</td>
                    <td class="tg-0lax"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                </tr>
                <tr>
                    <td class="tg-baqh">4.</td>
                    <td class="tg-0lax">Daftar Riwayat Hidup</td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td class="tg-0lax"></td>
                </tr>
                <tr>
                    <td class="tg-baqh">5. </td>
                    <td class="tg-0lax">Pas Photo Pemohon</td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td class="tg-0lax"></td>
                </tr>
                <tr>
                    <td class="tg-baqh">6.</td>
                    <td class="tg-0lax">Data Kursus</td>
                    <td class="tg-0lax"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                </tr>
                <tr>
                    <td class="tg-baqh">7.</td>
                    <td class="tg-0lax">Fotocopy Ijazah legalisir</td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td class="tg-0lax"></td>
                </tr>
                <tr>
                    <td class="tg-baqh">8.</td>
                    <td class="tg-0lax">Surat Keterangan dari Universitas/Perguruan Tinggi/Sekolah</td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td class="tg-0lax"></td>
                </tr>
                <tr>
                    <td class="tg-baqh">9.</td>
                    <td class="tg-0lax">Data Pendidikan</td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td class="tg-0lax"></td>
                </tr>
                <tr>
                    <td class="tg-baqh">10.</td>
                    <td class="tg-0lax">Data Pengalaman Organisasi</td>
                    <td class="tg-0lax"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                </tr>
                <tr>
                    <td class="tg-baqh">11.</td>
                    <td class="tg-0lax">Data Pengalaman Kerja di Proyek</td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td class="tg-0lax"></td>
                </tr>
                <tr>
                    <td class="tg-baqh">12.</td>
                    <td class="tg-0lax">Berita Acara Verifikasi &amp; Validasi Awal</td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td class="tg-0lax"></td>
                </tr>
                <tr>
                    <td class="tg-baqh">13. </td>
                    <td class="tg-0lax">Surat Permohonan</td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td class="tg-0lax"></td>
                </tr>
                <tr>
                    <td class="tg-baqh">14.</td>
                    <td class="tg-0lax">Surat Pengantar Permohonan Asosiasi</td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td class="tg-0lax"></td>
                </tr>
                <tr>
                    <td class="tg-baqh">15.</td>
                    <td class="tg-0lax">Photo Copy Sertifikat Keahlian (SKA) / Asli</td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td class="tg-0lax"></td>
                </tr>
                <tr>
                    <td class="tg-baqh">16.</td>
                    <td class="tg-0lax">Penilaian Mandiri Pemohon (Self Asessment)</td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td style="text-align: center;">v</td>
                    <td class="tg-0lax"></td>
                    <td class="tg-0lax"></td>
                </tr>
            </table>
            <br>
            <div class="ttd" style="margin-top: 20px;">
                <p style="font-size: 18px;">
                    Pemeriksa Kelengkapan
                </p>
                <div style="border-style: ridge; padding-bottom: 7px; margin-top: -10px;">
                    <br><br><br><br>
                    <p style="font-size: 17px; margin: -2px; text-transform: uppercase;">{{ $pengajuan->pemeriksa }}</p>
                    (......................)
                </div>
            </div>
        </div>
    </div>
</body>

</html>
