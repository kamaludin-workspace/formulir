<!DOCTYPE html>
<html lang="en">

<head>
    <title>Data Asosiasi Profesi Tenaga Ahli Konstruksi</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
        body {
            font-family: 'Helvetica';
        }

        .img-lpjk {
            width: 20%;
            float: left
        }

        .info td {
            font-size: 17px;
            padding-top: 8px;
            padding-bottom: 8px;
        }

        .pendidikan tr td {
            padding: 5px 6px;
            border-collapse: collapse;
            border: 1.5px solid black;
        }

        .tg {
            border-collapse: collapse;
            border-spacing: 0;
        }

        .tg td {
            border-color: black;
            border-style: solid;
            border-width: 1px;
            font-family: Arial, sans-serif;
            font-size: 14px;
            overflow: hidden;
            padding: 10px 5px;
            word-break: normal;
        }

        .tg th {
            border-color: black;
            border-style: solid;
            border-width: 1px;
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            overflow: hidden;
            padding: 10px 5px;
            word-break: normal;
        }

        .tg .tg-c3ow {
            border-color: inherit;
            text-align: center;
            vertical-align: top
        }

        .tg .tg-0pky {
            border-color: inherit;
            text-align: left;
            vertical-align: top
        }

    </style>
</head>

<body>
    <div class="content">
        <div class="img-lpjk">
            <img src="https://formulir.dpdataksiriau.id/img/lpjk.png"
                alt="Jika gambar tidak muncul, perbaiki link gambar">
        </div>
        <div style="margin-top: 60px; margin-left: -50px;">
            <p style="font-size: 22px;letter-spacing: 1px;">
                Data Asosiasi Profesi <br>
                Tenaga Ahli Kontruksi <br>
            </p>
        </div>
        <table class="info" style="margin-top: 60px; margin-left: 10px;">
            <tr>
                <td width="175">1. Nama</td>
                <td>:</td>
                <td style="width: 350; text-transform: uppercase;">{{ $pengajuan->nama_pemohon }}</td>
            </tr>
            <tr>
                <td>2. No. KTP</td>
                <td>:</td>
                <td>{{ $pengajuan->nik }}</td>
            </tr>
            <tr>
                <td>3. Tanggal Permohonan</td>
                <td>:</td>
                <td>{{ $pengajuan->tgl_surat }}</td>
            </tr>
            <tr>
                <td>4. Tempat &amp; Tanggal Lahir</td>
                <td>:</td>
                <td>{{ $pengajuan->tempat_lahir }}, {{ $pengajuan->tgl_lahir }}</td>
            </tr>
            <tr>
                <td>5. Alamat<br>Jalan<br>Kota / Kabupaten<br>Kodepos<br>Propinsi</td>
                <td><br>:<br>:<br>:<br>:</td>
                <td><br>{{ $pengajuan->alamat }}<br>{{ $daerah[0]->kabkota }}<br>{{ $pengajuan->pos }}<br>{{ $daerah[0]->provinsi }}
                </td>
            </tr>
            <tr>
                <td>6. NPWP</td>
                <td>:</td>
                <td>{{ $pengajuan->npwp }}</td>
            </tr>
            <tr>
                <td>7. Detail Data Personal</td>
                <td>:</td>
                <td></td>
            </tr>
        </table>
        <div class="clear">
            <p style="text-align: center;">PENDIDIKAN</p>
            <table class="pendidikan" style="text-transform: uppercase; width: 100%; border-collapse: collapse; ">
                <tr>

                    <td style="width: 20;vertical-align: top;"> No.</td>
                    <td style="width: 90;  font-size: 16px; text-align: left; vertical-align: top;">Tingkat <br>
                        Pendidikan</td>
                    <td style="width: 150; vertical-align: top;">Nama Perguruan <br> Tinggi/Sekolah</td>
                    <td style="width: 90; vertical-align: top;">Jurusan</td>
                    <td style="text-align: center; vertical-align: top;">Kota</td>
                    <td style="vertical-align: top; width: 60;">Tahun <br>Lulus</td>
                    <td style="text-align: center; vertical-align: top;">No Ijazah</td>
                </tr>
                <tr>
                    <td style="vertical-align: top;">1.</td>
                    <td style="vertical-align: top;">{{ $pengajuan->jenjang_pendidikan }}</td>
                    <td style="vertical-align: top; text-transform: uppercase">{{ $pengajuan->nama_sekolah }}</td>
                    <td style="vertical-align: top; text-transform: uppercase">{{ $pengajuan->alamat_sekolah }}</td>
                    <td style="vertical-align: top;">{{ $pengajuan->jurusan }}</td>
                    <td style="vertical-align: top;">{{ $pengajuan->tahun_lulus }}</td>
                    <td style="text-align: center; vertical-align: top;">{{ $pengajuan->nomor_ijazah }}</td>
                </tr>
            </table>
            <p style="text-align: center;">PENGALAMAN PROYEK</p>
            <table class="pendidikan" style="width: 100%; border-collapse: collapse; ">
                <tr>
                    <td width="20"><span style="font-style:normal">No.</span></td>
                    <td>Nama Proyek</td>
                    <td width="80">Lokasi Proyek</td>
                    <td>Nilai Kontrak (Dalam Ribu)</td>
                    <td width="60">Mulai</td>
                    <td width="80">Selesai</td>
                    <td>Jabatan</td>
                </tr>
                @php($no = 1)
                @foreach ($pekerjaan as $value)
                <tr>
                    <td style="vertical-align: top;">{{ $no++ }}.</td>
                    <td style="vertical-align: top;">{{ $value->rp_namaProyek }}</td>
                    <td style="vertical-align: top;">{{ $value->rp_lokasiProp }}</td>
                    <td style="vertical-align: top;">Rp. {{ number_format($value->rp_nilai) }}</td>
                    <td style="vertical-align: top;">{{ $value->rp_tglMulai }}</td>
                    <td style="vertical-align: top;">{{ $value->rp_tglSelesai }}</td>
                    <td style="vertical-align: top;">{{ $value->sk_nama }}</td>
                </tr>
                @endforeach
            </table>
            <p style="text-align: center;">KLASIFIKASI KUALIFIKASI</p>
            <table class="pendidikan" style="width: 100%; border-collapse: collapse; ">
                <tr>
                    <td class="tg-mza1"><span style="font-style:normal">No.</span></td>
                    <td class="tg-4yhk">ID Personal</td>
                    <td class="tg-4yhk">Nama</td>
                    <td class="tg-4yhk">ID Sub Bidang</td>
                    <td class="tg-4yhk">ID ASOSIASI</td>
                    <td class="tg-4yhk">Kualifikasi</td>
                    <td class="tg-4yhk">Permohonan</td>
                </tr>
                @php($no = 1)
                @php($j = 0)
                @php($k = 0)

                @foreach ($kualifikasi as $value)

                <tr>
                    <td class="tg-9wq8" width="word-break">{{ $no++ }}.</td>
                    <td class="tg-lboi" width="word-break">{{ $pengajuan->nik }}</td>
                    <td class="tg-lboi" style="text-transform: uppercase">{{ $pengajuan->nama_pemohon }}</td>
                    <td class="tg-lboi">{{ $value->sk_kode }}</td>
                    <td class="tg-lboi">
                        {{ asosiasi($pengajuan->no_reg_asosiasi) }}
                    </td>
                    <td class="tg-lboi">
                        {{ Kualifikasi($pengajuan->id_kualifikasi[$k++], $pengajuan->no_reg_asosiasi) }}
                    </td>
                    <td class="tg-lboi">{{ $pengajuan->keterangan[$j++] }}</td>
                </tr>
                @endforeach
            </table>
            <br>
            <table class="tg" width="600">
                <thead>
                    <tr>
                        <th class="tg-0pky" colspan="5"
                            style="vertical-align: bottom; border-top: 0px; border-left: 0px; border-right: 0px;">
                            Verifikasi &amp; Validasi</th>
                        <th class="tg-0pky" rowspan="7" width="10"
                            style=" bottom; border-top: 0px; border-left: 0px; border-right: 0px; border-bottom: 0px;">
                        </th>
                        <th class="tg-0pky" colspan="6"
                            style="text-align: center:  bottom; border-top: 0px; border-left: 0px; border-right: 0px;">
                            Pemeriksa Kelengkapan</th>
                        <th class="tg-0pky" rowspan="7" width="10"
                            style="text-align: center; bottom; border-top: 0px; border-left: 0px; border-right: 0px; border-bottom: 0px;">
                        </th>
                        <th class="tg-0pky"
                            style="text-align: center;  bottom; border-top: 0px; border-left: 0px; border-right: 0px;">
                            Unit Sertifikasi Tenaga Kerja</th>
                    </tr>
                    <tr>
                        <td class="tg-c3ow" colspan="4" rowspan="6" style="text-align: center; width: 120;">Database
                        </td>
                        <td class="tg-0pky" rowspan="6" style="text-align: center; width: 60;">Verifikator</td>
                        <td class="tg-0pky" colspan="6" rowspan="6" style="text-align: center; width: 180;">Sub Div
                            Tenaga Kerja</td>
                        <td class="tg-0pky" rowspan="6" style="text-align: center;">Catatan:</td>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                    </tr>
                    <tr>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</body>

</html>
