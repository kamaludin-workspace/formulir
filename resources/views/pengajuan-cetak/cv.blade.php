<html>

<head>
    <title>Curriculum Vitae</title>
    <style type="text/css">
        .parent{display:block;}.title{text-align:center;font-weight:700;font-size:18px}.data{margin-right:120px}.alamat{margin-left:16px}.float-child-left{width:30%;float:left}.float-child-right{width:70%;float:left}.absolute{clear:left}table{width:100%}.tg{margin-top:-10px;border-collapse:collapse;border-spacing:0}.tg td{font-family:Arial,sans-serif;font-size:14px;padding:0 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000;}.tg th{font-family:Arial,sans-serif;font-size:14px;font-weight:400;padding:0 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#000}.tg .tg-lboi{border-color:inherit;text-align:left;vertical-align:middle}.tg .tg-9wq8{border-color:inherit;text-align:center;vertical-align:middle}.tg .tg-4yhk{;border-color:#333;text-align:center;vertical-align:middle}.tg .tg-mza1{font-style:italic;border-color:#333;text-align:center;vertical-align:middle} td .uppercase{text-transform:uppercase;}
    </style>

</head>

<body>
    <div class="content">
        <p class="title">
            CURRICULUM VITAE
        </p>
        <div class="parent">
        <table style="padding-left: -3px;">
          <tr>
            <td width="10px;">1.</td>
            <td width="150px">Nama</td>
            <td width="10px;">:</td>
            <td style="text-transform: uppercase;">{{ $pengajuan->nama_pemohon }}</td>
          </tr>
          <tr>
            <td>2.</td>
            <td>Tempat, tgl lahir</td>
            <td>:</td>
            <td style="text-transform: uppercase;">{{ $pengajuan->tempat_lahir }}, {{ $pengajuan->tgl_lahir }}</td>
          </tr>
          <tr valign="baseline">
            <td>3.</td>
            <td>Alamat</td>
            <td>:</td>
            <td style="text-transform: uppercase;">{{ $pengajuan->alamat }}</td>
          </tr>
          <tr>
            <td></td>
            <td>Provinsi</td>
            <td>:</td>
            <td style="text-transform: uppercase;">{{ $daerah[0]->provinsi }}</td>
          </tr>
          <tr>
            <td></td>
            <td>Kabupaten</td>
            <td>:</td>
            <td style="text-transform: uppercase;">{{ $daerah[0]->kabkota }}</td>
          </tr>
          <tr>
            <td></td>
            <td>Kecamatan</td>
            <td>:</td>
            <td style="text-transform: uppercase;">{{ $daerah[0]->kecamatan }}</td>
          </tr>
          <tr>
            <td></td>
            <td>Kelurahan/desa</td>
            <td>:</td>
            <td style="text-transform: uppercase;">{{ $daerah[0]->keldes }}</td>
          </tr>
          <tr>
            <td>4.</td>
            <td>No. KTP</td>
            <td>:</td>
            <td>{{ $pengajuan->nik }}</td>
          </tr>
          <tr>
            <td>5.</td>
            <td>No. NPWP</td>
            <td>:</td>
            <td>{{ $pengajuan->npwp }}</td>
          </tr>
          <tr>
            <td>6.</td>
            <td>No. Reg Asosiasi</td>
            <td>:</td>
            <td>{{ $pengajuan->no_reg_asosiasi }}</td>
          </tr>
        </table>
        </div>

            <p>7. Riwayat pendidikan</p>
            <table class="tg">
                <tr>
                    <th class="tg-mza1" width="10px;"><span style="font-style:normal">No.</span></th>
                    <th class="tg-4yhk" width="30px;">Jenjang Pendidikan</th>
                    <th class="tg-4yhk">Nama Sekolah</th>
                    <th class="tg-4yhk">Alamat</th>
                    <th class="tg-4yhk">Jurusan/Program Studi</th>
                    <th class="tg-4yhk">Tahun Lulus</th>
                    <th class="tg-4yhk">No. Ijazah</th>
                </tr>
                <tr>
                    <td class="tg-9wq8">1.</td>
                    <td class="tg-lboi" style="text-align: center;">{{ $pengajuan->jenjang_pendidikan }}</td>
                    <td class="tg-lboi" style="text-transform: uppercase;">{{ $pengajuan->nama_sekolah }}</td>
                    <td class="tg-lboi" style="text-transform: uppercase;">{{ $pengajuan->alamat_sekolah }}</td>
                    <td class="tg-lboi" style="text-transform: uppercase;">{{ $pengajuan->jurusan }}</td>
                    <td class="tg-lboi" style="text-transform: uppercase;">{{ $pengajuan->tahun_lulus }}</td>
                    <td class="tg-lboi" style="text-transform: uppercase;">{{ $pengajuan->nomor_ijazah }}</td>
                </tr>
            </table>
            <p>8. Riwayat Kursus & Pelatihan</p>
            <table class="tg">
                <tr>
                    <th class="tg-mza1"><span style="font-style:normal">No.</span></th>
                    <th class="tg-4yhk">Nama Kursus &amp; Pelatihan</th>
                    <th class="tg-4yhk">Penyelenggara</th>
                    <th class="tg-4yhk">Alamat</th>
                    <th class="tg-4yhk">No. Sertifikat</th>
                    <th class="tg-4yhk">Tahun</th>
                </tr>
                <tr>
                    <td class="tg-9wq8">1.</td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                </tr>
                <tr>
                    <td class="tg-9wq8">2.</td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                </tr>
            </table>
            <p>9. Riwayat Pengalaman Organisasi, Perusahaan, & Instansi</p>
            <table class="tg">
                <tr>
                    <th class="tg-mza1"><span style="font-style:normal">No.</span></th>
                    <th class="tg-4yhk">Nama Organisasi, Perusahaan, &amp; Instansi</th>
                    <th class="tg-4yhk">Alamat</th>
                    <th class="tg-4yhk">Jabatan</th>
                    <th class="tg-4yhk">Tgl mulai &mdash; akhir</th>
                    <th class="tg-4yhk">Ket.</th>
                </tr>
                <tr>
                    <td class="tg-9wq8">1.</td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                </tr>
            </table>
            <p>10. Pengalaman Kerja di Proyek</p>
            <table class="tg">
                <tr>
                    <th class="tg-mza1" width="10"><span style="font-style:normal">No.</span></th>
                    <th class="tg-4yhk">Nama Proyek</th>
                    <th class="tg-4yhk" width="60">Lokasi</th>
                    <th class="tg-4yhk">Nilai Kontrak</th>
                    <th class="tg-4yhk" width="40">Mulai</th>
                    <th class="tg-4yhk" width="40">Selesai</th>
                    <th class="tg-4yhk">Jabatan</th>
                </tr>
                @php($no = 1)
                @foreach ($pekerjaan as $value)
                  <tr>
                      <td class="tg-9wq8">{{ $no++ }}.</td>
                      <td class="tg-lboi">{{ $value->rp_namaProyek }}</td>
                      <td class="tg-lboi">{{ $value->rp_lokasiProp }}</td>
                      <td class="tg-lboi">Rp. {{ number_format($value->rp_nilai) }}</td>
                      <td class="tg-lboi">{{ date('d/m/Y', strtotime($value->rp_tglMulai)) }}</td>
                      <td class="tg-lboi">{{ date('d/m/Y', strtotime($value->rp_tglSelesai)) }}</td>
                      {{-- <td class="tg-lboi">{{ $value->rp_tglMulai }} &mdash; {{ $value->rp_tglSelesai }}</td> --}}
                      <td class="tg-lboi">{{ $value->sk_nama }}</td>
                  </tr>
                @endforeach

            </table>
            <p>11. Studi Kasus & Karya Tulis</p>
            <table class="tg">
                <tr>
                    <th class="tg-mza1"><span style="font-style:normal">No.</span></th>
                    <th class="tg-4yhk">Nama Studi Kasus & Karya Tulis</th>
                    <th class="tg-4yhk">Tanggal mulai</th>
                    <th class="tg-4yhk">Tanggal Selesai</th>
                    <th class="tg-4yhk">Deskripsi</th>
                </tr>
                <tr>
                    <td class="tg-9wq8">1.</td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                </tr>
                <tr>
                    <td class="tg-9wq8">2.</td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                    <td class="tg-lboi"></td>
                </tr>
            </table>
    </div>
</body>

</html>
