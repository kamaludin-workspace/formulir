@extends('layouts.app')
@section('title-page') Security
@endsection
@section('content')
<div class="section-body">
    @include('layouts.alert')
    
    <div class="card">
        <div class="card-header">
            <h4>Ubah password</h4>
        </div>
        <form action="{{ url('password') }}" method="post">
        @csrf
        @foreach ($errors->all() as $error)
            <p class="pl-4 text-danger">
                {{ $error }}
            </p>
        @endforeach 
        <div class="card-body">
            <div class="form-group row">
                <label for="current_password" class="col-sm-3 col-form-label">Password lama</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="current_password" id="current_password" placeholder="Password lama" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="new_password" class="col-sm-3 col-form-label">Password baru</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="new_password" id="new_password" placeholder="Password baru" required>
                </div>
            </div>
            <div class="form-group row">
                <label for="new_confirm_password" class="col-sm-3 col-form-label">Konfirmasi password baru</label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="new_confirm_password" id="new_confirm_password" placeholder="Konfirmasi password baru" required>
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button type="submit" class="btn btn-primary">Ubah password</button>
        </div>
        </form>
    </div>
</div>
@endsection