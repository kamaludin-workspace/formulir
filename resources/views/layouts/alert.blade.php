@if ($message = Session::get('success'))
  <div class="alert alert-primary alert-dismissible show fade">
    <div class="alert-body">
      <button class="close" data-dismiss="alert">
        <span>&times;</span>
      </button>
      {{ $message }}
    </div>
  </div>
@endif

@if ($message = Session::get('warning'))
  <div class="alert alert-warning alert-dismissible show fade">
    <div class="alert-body">
      <button class="close" data-dismiss="alert">
        <span>&times;</span>
      </button>
      {{ $message }}
    </div>
  </div>
@endif

@if ($message = Session::get('delete'))
  <div class="alert alert-danger alert-dismissible show fade">
    <div class="alert-body">
      <button class="close" data-dismiss="alert">
        <span>&times;</span>
      </button>
      {{ $message }}
    </div>
  </div>
@endif
