<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Formulir Permohonan</title>
    <link rel="icon" href="{{ asset('img/logo.png') }}">

    <!-- General CSS Files -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css">
    
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <!-- CSS -->
    <link rel="stylesheet" href="{{ asset('css/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/components.css') }}">
    <link rel="stylesheet" href="{{ asset('css/preloader.css') }}">
    <link rel="stylesheet" href="{{ asset('css/jquery.fancybox.min.css') }}">
    {{-- javascript pemilihan kode untuk staff --}}
    <script>
        function tampilkan() {
            var nama_kota = document.getElementById("form1").level.value;
            if (nama_kota == "admin" || nama_kota == "pimpinan") {
                document.getElementById("tampil").innerHTML =
                    "<option value=''>Tidak Ada Kode Yang Dipilih</option>";
            } else if (nama_kota == "staff") {
                document.getElementById("tampil").innerHTML =
                    "<option value=''>=== Silahkan Pilih Kode ===</option><option value='152'>Ataksi</option><option value='144'>Asdamkindo</option><option value='180'>Proteksi</option>";
            }
        }

        function tampilSkt() {
            var nama_jurusan = document.getElementById("form2").jurusan.value;
            if (nama_jurusan == "1") {
                document.getElementById("klasifikasi").innerHTML =
                    "<option value='AA'>AA</option>";
            } else if (nama_jurusan == "2") {
                document.getElementById("klasifikasi").innerHTML =
                    "<option value='AM'>AM</option>";
            } else if (nama_jurusan == "3") {
                document.getElementById("klasifikasi").innerHTML =
                    "<option value='AE'>AE</option>";
            } else if (nama_jurusan == "4") {
                document.getElementById("klasifikasi").innerHTML =
                    "<option value='AT'>AT</option>";
            } else if (nama_jurusan == "5") {
                document.getElementById("klasifikasi").innerHTML =
                    "<option value='AL'>AL</option>";
            } else if (nama_jurusan == "6") {
                document.getElementById("klasifikasi").innerHTML =
                    "<option value='AS'>AS</option>";
            }
        }

    @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '152')
    const tingkat = 'http://localhost:8888/ataksi/keuangan-ataksi/public/API/tingkat/';
    @elseif(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '144')
    const tingkat = 'http://localhost:8888/ataksi/keuangan-asdamkindo/public/API/tingkat/';
    @elseif(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '180')
    const tingkat = 'http://localhost:8888/ataksi/keuangan-proteksi/public/API/tingkat/';
    @endif

    </script>
    <style media="screen">
    .preloader {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      z-index: 9999;
      background-color: #fff;
      }
      .preloader .loading {
      position: absolute;
      left: 50%;
      top: 50%;
      transform: translate(-50%,-50%);
      font: 14px arial;
    }
    </style>
</head>

<body>
  <div class="preloader">
  <div class="loading">
    <img src="{{ asset('loader.gif') }}" width="80">
    <p>Memuat data</p>
  </div>
  </div>
    <div id="app">
        <div class="main-wrapper">
            <div class="navbar-bg"></div>
            <nav class="navbar navbar-expand-lg main-navbar">
                <form class="form-inline mr-auto">
                    <ul class="navbar-nav mr-3">
                        <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i
                                    class="fas fa-bars"></i></a></li>
                        <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i
                                    class="fas fa-search"></i></a></li>
                    </ul>
                </form>
                <ul class="navbar-nav navbar-right">

                    <li class="dropdown"><a href="#" data-toggle="dropdown"
                            class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                            <img alt="image" src="{{ asset('img/avatar-1.png') }}"
                                class="rounded-circle mr-1">
                            <div class="d-sm-none d-lg-inline-block">Hi, {{Auth::user()->name}}</div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="{{ url('security') }}" class="dropdown-item has-icon">
                                <i class="fas fa-cog"></i> Security
                            </a>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                            document.getElementById('logout-form').submit();"
                                class="dropdown-item has-icon text-danger">
                                <i class="fas fa-sign-out-alt"></i> Logout
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                </ul>
            </nav>
            <div class="main-sidebar">
                <aside id="sidebar-wrapper">
                    <div class="sidebar-brand">
                        <a href="/">Aplikasi Formulir</a>
                    </div>
                    <div class="sidebar-brand sidebar-brand-sm">
                        <a href="/">AP</a>
                    </div>
                    <ul class="sidebar-menu">

                        <li class="menu-header">Dashboard</li>
                        <li class="@yield('status-dashboard')"><a class="nav-link" href="{{ url('home') }}"><i class="fas fa-fire"></i>
                            <span>Dashboard</span></a>
                        </li>

                        <li class="menu-header">Starter & Reports</li>
                        @if(Auth::user()->level == 'staff')
                        <li class="@yield('status-form-pengajuan')"><a class="nav-link"
                                href="{{ url('pengajuan/form') }}"><i class="fas fa-file-contract"></i> <span>Formulir</span></a></li>
                        @endif
                        <li class="nav-item dropdown @yield('status-data-pengajuan')">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-table"></i>
                                <span>Data Pengajuan</span></a>
                            <ul class="dropdown-menu">
                                <li class="@yield('pengajuan-proses')"><a class="nav-link"
                                        href="{{ url('pengajuan/proses') }}">Proses</a></li>
                                <li class="@yield('pengajuan-selesai')"><a class="nav-link"
                                        href="{{ url('pengajuan/selesai') }}">Selesai</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown @yield('laporan')">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-fax"></i>
                                <span>Laporan</span></a>
                            <ul class="dropdown-menu">
                                <li class=" @yield('laporan-ustkm')"><a class="nav-link"
                                        href="{{ url('laporan/ustkm') }}">USTKM</a></li>
                                <li class="@yield('laporan-dpp')"><a class="nav-link " href="{{ url('laporan/dpp') }}">DPP/APROVAL</a>
                                </li>
                                <li class="@yield('laporan-periode')"><a class="nav-link  "
                                        href="{{ url('laporan/periode') }}">Periode</a></li>

                            </ul>
                        </li>
                        @if(Auth::user()->level == 'admin')
                        <li class="menu-header">Master Data</li>
                        <li class="nav-item dropdown @yield('dropdown-pengalaman')">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-rocket"></i>
                                <span>Pengalaman</span></a>
                            <ul class="dropdown-menu">
                                <li class="@yield('status-arsitektur')"><a class="nav-link"
                                        href="/arsitektur">Arsitektur</a></li>
                                <li class="@yield('status-sipil')"><a class="nav-link" href="/sipil">Sipil</a></li>
                                <li class="@yield('status-meka')"><a class="nav-link" href="/mekanikal">Mekanikal</a>
                                </li>
                                <li class="@yield('status-elka')"><a class="nav-link" href="/elektrikal">Elektrikal</a>
                                </li>
                                <li class="@yield('status-talin')"><a class="nav-link" href="/tata-lingkungan">Tata
                                        Lingkungan</a></li>
                                <li class="@yield('status-manaj')"><a class="nav-link" href="/manajemen">Manajemen</a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown @yield('dropdown-kualifikasi')">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-sitemap"></i>
                                <span>Kualifikasi</span></a>
                            <ul class="dropdown-menu">
                                <li class="@yield('status-ska')"><a class="nav-link" href="/ska">SKA</a></li>
                                <li class="@yield('status-skt')"><a class="nav-link" href="/skt">SKT</a></li>
                            </ul>
                        </li>

                        <li class="nav-item dropdown @yield('dropdown-wilayah')">
                            <a href="#" class="nav-link has-dropdown"><i class="fas fa-map-marked-alt"></i>
                                <span>Wilayah</span></a>
                            <ul class="dropdown-menu">
                                <li class="@yield('status-provinsi')"><a class="nav-link" href="{{ url('provinsi') }}">Provinsi</a></li>
                                <li class="@yield('status-kabkota')"><a class="nav-link" href="{{ url('kabkota') }}">Kabupaten/Kota</a></li>
                                <li class="@yield('status-kecamatan')"><a class="nav-link" href="{{ url('kecamatan') }}">Kecamatan</a></li>
                                <li class="@yield('status-keldes')"><a class="nav-link" href="{{ url('keldes') }}">Kelurahan/Desa</a></li>
                            </ul>
                        </li>

                        {{-- fitur manajemen user --}}
                        <li class="@yield('status-data-pengguna')"><a class="nav-link" href="{{ url('pengguna') }}"><i
                                    class="fas fa-table"></i> <span>
                                    Manajemen User</span></a></li>
                    </ul>
                    @endif

                    <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
                        <a href="{{ url('dokumentasi') }}" class="btn btn-primary btn-lg btn-block btn-icon-split">
                            <i class="fas fa-rocket"></i> Dokumentasi
                        </a>
                    </div>
                </aside>
            </div>

            <!-- Main Content -->
            <div class="main-content">
                <section class="section">
                    <div class="section-header">
                        <h1>@yield('title-page')</h1>
                    </div>
                    {{-- NEW Row --}}
                    @yield('content')
                </section>
            </div>
        </div>
    </div>

    <!-- General JS Scripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js">
    </script>
    {{-- <script src="system/js/chart.min.js"></script> --}}

    <!-- Template JS File -->
    <script src="{{ asset('js/stisla.js') }}"></script>
    <script src="{{ asset('js/scripts.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <script src="{{ asset('js/select2.full.min.js') }}"></script>
    <script src="{{ asset('js/dropdown.js') }}" charset="utf-8"></script>
    <script src="{{ asset('js/daterangepicker.js') }}" charset="utf-8"></script>
    <script src="{{ asset('js/jquery.fancybox.min.js') }}" charset="utf-8"></script>
    <script type="text/javascript">
        function numberFilter(evt) {
            let charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        var rupiah = document.getElementById('rupiah');
        rupiah.addEventListener('keyup', function (e) {
            // tambahkan 'Rp.' pada saat form di ketik
            // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
            rupiah.value = formatRupiah(this.value, 'Rp. ');
        });

        /* Fungsi formatRupiah */
        function formatRupiah(angka, prefix) {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }

    </script>

    @yield('js')
    @yield('modal-profil')

    {{-- Modal Profil --}}
    <script type="text/javascript">
        $(document).ready(function() {
            $('input[type=radio][name=jenis]').on('change', function() {
                let permohonanId = $(this).val();
                if (permohonanId == 1) {
                    window.jenis = "SKA";
                } else if (permohonanId == 2) {
                    window.jenis = "SKT";
                }
                if (permohonanId) {
                    jQuery.ajax({
                        url: '/subklasifikasi/' + window.jenis,
                        type: "GET",
                        dataType: "json",
                        success: function(data) {
                            $('select[name="klasifikasi"]').empty();
                            $('select[name="klasifikasi"]').append('<option>Pilih klasifikasi</option>');
                            $.each(data, function(key, value) {
                                $('select[name="klasifikasi"]').append('<option value="' + key + '">'+ key +'-' + value + '</option>');
                            });
                        },
                    });
                } else {
                    $('select[name="jenis"]').empty();
                }
            });
        });

        $("#checkPengalaman").on("click", function() {
            $('#emptyAlert').empty();
            let permohonanId = $('input[type=radio][name=jenis]').val();
            let klasifikasi = $('select[name="klasifikasi"]').val();
            let limit = $('select[name="jumlah"]').val();
            let tahun = $('input[type=text][name=tahun]').val();
            let daerah = $('select[name="daerah"]').val();
            if (klasifikasi) {
                jQuery.ajax({
                    url: '/pengalaman/'+klasifikasi+'/tahun/'+tahun+'/daerah/'+daerah+'/limit/'+limit,
                    type: "GET",
                    dataType: "json",
                    success: function(data) {
                        console.log(data);
                        if ($.trim(data)) {
                            $('#emptyAlert').empty();
                            $('#successBtn').remove();
                            $('#boxVerify').append('<button type="button" id="successBtn" class="btn btn-sm btn-icon btn-success"><i class="fas fa-check"></i>&nbsp;Tersedia</button>');
                            $('#listData').empty();
                            $('#submitPengalaman').prop("disabled", false);
                            $.each(data, function(key, value) {
                                $('#listData').append('<li class="list-group-item"><strong>'+ value.rp_lokasiProp + '</strong> - ' + value.rp_namaProyek + ' &mdash; ' + value.rp_tglMulai + '</li>');
                                $('#listData').append('<input type="hidden" name="rp_subKlas[]" value="' + value.rp_subKlas + '"><input type="hidden" name="rp_namaProyek[]"value="' + value.rp_namaProyek +
                                    '"><input type="hidden" name="rp_lokasiProp[]" value="' + value.rp_lokasiProp + '"><input type="hidden" name="rp_kodeLok[]" value="' + value.rp_kodeLok +
                                    '"><input type="hidden" name="rp_nilai[]" value="' + value.rp_nilai + '"><input type="hidden" name="rp_tglMulai[]" value="' + value.rp_tglMulai +
                                    '"><input type="hidden" name="rp_tglSelesai[]" value="' + value.rp_tglSelesai + '"><input type="hidden" name="rp_tahun[]" value="' + value.rp_tahun + '">');
                            });
                        } else {
                            $('#emptyAlert').empty();
                            $('#emptyAlert').append('<code>Data tidak tersedia</code>');
                            $('#successBtn').remove();
                            $('#listData').empty();
                            $('#submitPengalaman').prop("disabled", true);
                            
                        }
                    },
                });
            } else {
            }
        });
        $(document).ready(function(){
          $(".preloader").fadeOut();
        });
    </script>

</body>
</html>
