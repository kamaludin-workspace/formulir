@extends('layouts.app')
@section('title-page') Dokumentasi
@endsection

@section('content')
<div class="section-body">
    <div class="card">
        <div class="card-body">
            <ul class="nav nav-tabs" id="myTab2" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#home2" role="tab" aria-controls="home" aria-selected="true">Sign in & Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#profile2" role="tab" aria-controls="profile" aria-selected="false">Starter & Report</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="contact-tab2" data-toggle="tab" href="#contact2" role="tab" aria-controls="contact" aria-selected="false">Master Data</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pengajuan-tab2" data-toggle="tab" href="#pengajuan" role="tab" aria-controls="pengajuan" aria-selected="false">Alur Pengajuan Baru</a>
                </li>
            </ul>
            <div class="tab-content tab-bordered" id="myTab3Content">

                @include('dokumentasi.dashboard')
                @include('dokumentasi.starter')
                @include('dokumentasi.master')
                @include('dokumentasi.pengajuan')

            </div>
        </div>
    </div>

    @endsection

    @section('js')
    <script type="text/javascript" src="/fancybox/jquery.fancybox-1.3.4.pack.js"></script>
    @endsection
