<div class="tab-pane fade" id="contact2" role="tabpanel" aria-labelledby="contact-tab2">
    <div class="card-header">
        <h4>1. Pengalaman</h4>
    </div>
    <div class="row card-body">
        <div class="col-4">
            <p>Di menu pengalaman dapat melakukan aksi melihat, mengubah, dan menghapus data pengalaman, sub menu terdiri dari:</p>

            <a href="{{ asset('img/dokumentasi/menu-pengalaman.png') }}" data-fancybox="gallery">
                <img src="{{ asset('img/dokumentasi/menu-pengalaman.png') }}" class="img-fluid border">
            </a>
            <strong>Catatan:</strong>
            <div class="ml-n4">
                <ol>
                    <li>Aksi ini hanya terbatas untuk administrator</li>
                </ol>
            </div>
        </div>
        <div class="col-8">
            <kbd>Halaman pengalaman</kbd>
            <a href="{{ asset('img/dokumentasi/pengalaman.png') }}" data-fancybox="gallery">
                <img src="{{ asset('img/dokumentasi/pengalaman.png') }}" class="img-fluid border">
            </a>
            @include('dokumentasi.zoom')
        </div>
    </div>
    <div class="card-header">
        <h4>2. Kualifikasi & Wilayah</h4>
    </div>
    <div class="row card-body">
        <div class="col-auto">
            <p>Aksi melihat, mengubah, dan menghapus data pada menu <kbd>Kualifikasi</kbd> dan <kbd>Wilayah</kbd> sama dengan yang lainnya. ikut arahan pada halaman tersebut.</p>
        </div>
    </div>
</div>
