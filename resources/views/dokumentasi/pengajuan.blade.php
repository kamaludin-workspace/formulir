<div class="tab-pane fade" id="pengajuan" role="tabpanel" aria-labelledby="pengajuan-tab2">
    <div class="card-header">
        <h4>1. Pengajuan Baru</h4>
    </div>
    <div class="row card-body">
        <div class="col-4">
            <p>Akses menu dengan memilih navigasi <kbd>Pengajuan Baru</kbd>:</p>
            <strong>Catatan:</strong>
            <div class="ml-n4">
                <ol>
                    <li>Isi data dengan hati-hati</li>
                    <li>Isilah data sesuai dengan kolomnya</li>
                    <li>Tombol reset digunakan untuk mereset semua inputan anda</li>
                </ol>
            </div>
        </div>
        <div class="col-8">
            <kbd>Halaman pengajuan baru</kbd>
            <a href="{{ asset('img/dokumentasi/form-pengajuan-baru.png') }}" data-fancybox="gallery">
                <img src="{{ asset('img/dokumentasi/form-pengajuan-baru.png') }}" class="img-fluid border">
            </a>
            @include('dokumentasi.zoom')
        </div>
    </div>
    <div class="card-header">
        <h4>2. Mengidentifikasi pengalaman</h4>
    </div>
    <div class="row card-body">
        <div class="col-4">
            <p>Setelah anda selesai mengisi data dan submit pengajuan baru, maka selanjutnya anda akan diarahkan ke halaman yang menampilkan data pengalaman yang cocok dengan kualifikasi.</p>
        </div>
        <div class="col-8">
            <kbd>Halaman menambah data pengalaman</kbd>
            <a href="{{ asset('img/dokumentasi/riwayat.png') }}" data-fancybox="gallery">
                <img src="{{ asset('img/dokumentasi/riwayat.png') }}" class="img-fluid border">
            </a>
            <div class="pt-2">
                <a href="javascript:;" data-fancybox="gallery" data-options='{"caption" : "Tampilan jika pengalaman kerja kosong", "src" : "{{ asset('img/dokumentasi/pengalaman-kosong.png') }}"}'>
                    <img src="{{ asset('img/dokumentasi/pengalaman-kosong.png') }}" class="img-rounded border" width="100">
                </a>
                <a  href="javascript:;" data-fancybox="gallery" data-options='{"caption" : "Menambah data pengalaman secara manual", "src" : "{{ asset('img/dokumentasi/pengalaman-manual.png') }}"}'>
                    <img src="{{ asset('img/dokumentasi/pengalaman-manual.png') }}" class="img-rounded border" width="100">
                </a>
            </div>
            @include('dokumentasi.zoom')
        </div>
    </div>
    <div class="card-header">
        <h4>3. Aproval pengajuan</h4>
    </div>
    <div class="row card-body">
        <div class="col-4">
            <p>Setelah anda selesai mengisi semua data yang dibutuhkan, langkah selanjutnya yaitu melakukan aproval, lihat gambar disamping</p>
        </div>
        <div class="col-8">
            <kbd>Halaman aproval</kbd>
            <a  href="javascript:;" data-fancybox="gallery" data-options='{"caption" : "Pilihan menghapus pengalaman dan mengubah status aproval", "src" : "{{ asset('img/dokumentasi/hapus-pengalaman.png') }}"}'>
                <img src="{{ asset('img/dokumentasi/hapus-pengalaman.png') }}" class="img-fluid border">
            </a>
            @include('dokumentasi.zoom')
        </div>
    </div>
</div>
