<div class="tab-pane fade show active" id="home2" role="tabpanel" aria-labelledby="home-tab2">
    <div class="card-header">
        <h4>1. Sign In</h4>
    </div>
    <div class="row card-body mt-n2">
        <div class="col-4">
            <p>
                Sebelum mengakses sistem, pengguna harus melakukan proses log in. hubungi administrator sistem untuk mendapatkan hak akses.
            </p>
        </div>
        <div class="col-8">
            <kbd>Tampilan halaman sign in</kbd>
            <a href="{{ asset('img/dokumentasi/login.png') }}" data-fancybox="gallery" >
                <img src="{{ asset('img/dokumentasi/login.png') }}" class="img-fluid">
            </a>
            <small><code>Klik gambar untuk memperbesar</code></small>
        </div>
    </div>
    <div class="card-header">
        <h4>2. Dashboard</h4>
    </div>
    <div class="row card-body mt-n2">
        <div class="col-4">
            <p>
                Dashboard merupakan halaman yang tampil jika proses sign ini pengguna berhasil. tidak ada aksi dihalaman ini
            </p>
        </div>
        <div class="col-8">
            <kbd>Tampilan halaman Dashboard</kbd>
            <a href="{{ asset('img/dokumentasi/dashboard.png') }}" data-fancybox="gallery" >
                <img src="{{ asset('img/dokumentasi/dashboard.png') }}" class="img-fluid">
            </a>
            <small><code>Klik gambar untuk memperbesar</code></small>
        </div>
    </div>
</div>
