<div class="tab-pane fade" id="profile2" role="tabpanel" aria-labelledby="profile-tab2">
    <div class="card-header">
        <h4>1. Pengajuan baru</h4>
    </div>
    <div class="row card-body">
        <div class="col-4">
            <p>Pengajuan baru merupakan menu untuk mengakses formulir permohonan baru. Untuk melihat proses pengajuan baru, lihat menu tab <kbd>Alur Pengajuan Baru</kbd></p>

            <strong>Catatan:</strong>
            <div class="ml-n4">
                <ol>
                    <li>Jika anda adalah administrator, pilih kode nomor asosiasi secara mandiri.</li>
                    <li>Pastikan mengisi semua kolom sebelum menyimpan data</li>
                </ol>
            </div>
        </div>
        <div class="col-8">
            <kbd>Pengajuan baru</kbd>
            <a href="{{ asset('img/dokumentasi/pengajuan-baru.png') }}" data-fancybox="gallery">
                <img src="{{ asset('img/dokumentasi/pengajuan-baru.png') }}" class="img-fluid border">
            </a>
            @include('dokumentasi.zoom')
        </div>
    </div>
    <div class="card-header">
        <h4>2. Data pengajuan</h4>
    </div>
    <div class="row card-body">
        <div class="col-4">
            <p>Data pengajuan terdiri dari dua sub menu:</p>
            <a href="{{ asset('img/dokumentasi/menu-data-pengajuan.png') }}" data-fancybox="gallery">
                <img src="{{ asset('img/dokumentasi/menu-data-pengajuan.png') }}" class="img-fluid border">
            </a>
            <strong>Catatan:</strong>
            <div class="ml-n4">
                <ol>
                    <li>Menu proses yaitu data pengajuan yang belum di aproval</li>
                    <li>Menu selesai yaitu data pengajuan yang sudah di aproval</li>
                </ol>
            </div>
        </div>
        <div class="col-8">
            <kbd>List data pengajuan</kbd>
            <a href="{{ asset('img/dokumentasi/data-pengajuan.png') }}" data-fancybox="gallery">
                <img src="{{ asset('img/dokumentasi/data-pengajuan.png') }}" class="img-fluid border">
            </a>
            <div class="pt-2">
                <a href="javascript:;" data-fancybox="gallery" data-options='{"caption" : "Tampilan rincian data pengajuan", "src" : "{{ asset('img/dokumentasi/detail-data-pengajuan.png') }}"}'>
                    <img src="{{ asset('img/dokumentasi/detail-data-pengajuan.png') }}" class="img-rounded border" width="100">
                </a>
                <a href="javascript:;" data-fancybox="gallery" data-options='{"caption" : "Klik tombol aksi untuk menampilkan pilihan", "src" : "{{ asset('img/dokumentasi/aksi-data-pengajuan.png') }}"}'>
                    <img src="{{ asset('img/dokumentasi/aksi-data-pengajuan.png') }}" class="img-rounded border" width="100">
                </a>
                <a  href="javascript:;" data-fancybox="gallery" data-options='{"caption" : "Menambah data pengalaman secara manual", "src" : "{{ asset('img/dokumentasi/pengalaman-manual.png') }}"}'>
                    <img src="{{ asset('img/dokumentasi/pengalaman-manual.png') }}" class="img-rounded border" width="100">
                </a>
                <a  href="javascript:;" data-fancybox="gallery" data-options='{"caption" : "Pilihan menghapus pengalaman dan mengubah status aproval", "src" : "{{ asset('img/dokumentasi/hapus-pengalaman.png') }}"}'>
                    <img src="{{ asset('img/dokumentasi/hapus-pengalaman.png') }}" class="img-rounded border" width="100">
                </a>
            </div>
            @include('dokumentasi.zoom')
        </div>
    </div>
    <div class="card-header">
        <h4>3. Laporan</h4>
    </div>
    <div class="row card-body">
        <div class="col-4">
            <p>Menu laporan terdiri dari:</p>
            <a href="{{ asset('img/dokumentasi/menu-laporan.png') }}" data-fancybox="gallery">
                <img src="{{ asset('img/dokumentasi/menu-laporan.png') }}" class="img-fluid border">
            </a>
            <strong>Catatan:</strong>
            <div class="ml-n4">
                <ol>
                    <li>Laporan USTKM (Harian)</li>
                    <li>Laporan DPP/APROVAL (Harian)</li>
                    <li>Laporan Periode (Rentang tanggal dinamis)</li>
                </ol>
            </div>
        </div>
        <div class="col-8">
            <kbd>Laporan USTKM</kbd>
            <a href="{{ asset('img/dokumentasi/laporan-ustkm.png') }}" data-fancybox="gallery">
                <img src="{{ asset('img/dokumentasi/laporan-ustkm.png') }}" class="img-fluid border">
            </a>
            <div class="pt-2">
                <a href="javascript:;" data-fancybox="gallery" data-options='{"caption" : "Hasil pencarian data USKTM", "src" : "{{ asset('img/dokumentasi/hasil-ustkm.png') }}"}'>
                    <img src="{{ asset('img/dokumentasi/hasil-ustkm.png') }}" class="img-rounded border" width="100">
                </a>
                <a href="javascript:;" data-fancybox="gallery" data-options='{"caption" : "Contoh hasil pencarian data USKTM jika melebihi 10 data", "src" : "{{ asset('img/dokumentasi/hasil-ustkm2.png') }}"}'>
                    <img src="{{ asset('img/dokumentasi/hasil-ustkm2.png') }}" class="img-rounded border" width="100">
                </a>
                <a href="javascript:;" data-fancybox="gallery" data-options='{"caption" : "Laporan DPP/APROVAL", "src" : "{{ asset('img/dokumentasi/laporan-dpp.png') }}"}'>
                    <img src="{{ asset('img/dokumentasi/laporan-dpp.png') }}" class="img-rounded border" width="100">
                </a>
                <a href="javascript:;" data-fancybox="gallery" data-options='{"caption" : "Laporan Periode", "src" : "{{ asset('img/dokumentasi/laporan-periode.png') }}"}'>
                    <img src="{{ asset('img/dokumentasi/laporan-periode.png') }}" class="img-rounded border" width="100">
                </a>
            </div>
            @include('dokumentasi.zoom')
        </div>
    </div>
</div>
