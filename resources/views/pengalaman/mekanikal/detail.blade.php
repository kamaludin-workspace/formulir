@extends('layouts.app')

@section('dropdown-pengalaman') active
@endsection

@section('status-meka') active
@endsection

@section('title-page') Pengalaman Bidang Mekanikal
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>Data Detail</h4>
                <div class="card-header-form">
                </div>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>Sub kelas</th>
                            <td>: {{$mk->rp_subKlas}}</td>
                        </tr>
                        <tr>
                            <th>Nama proyek</th>
                            <td>: {{$mk->rp_namaProyek}}</td>
                        </tr>
                        <tr>
                            <th>Lokasi proyek</th>
                            <td>: {{$mk->provinsi}}</td>
                        </tr>
                        <tr>
                            <th>Kode lokasi</th>
                            <td>: {{$mk->rp_kodeLok}}</td>
                        </tr>
                        <tr>
                            <th>Kelas</th>
                            <td>: {{$mk->sk_subKlasifikasi}}</td>
                        </tr>
                        <tr>
                            <th>Nilai</th>
                            <td>: <?php $nilai = $mk->rp_nilai;
                            $hasil = number_format($nilai, 0, "," , ".");
                            ?>Rp. {{$hasil}}</td>
                        </tr>
                        <tr>
                            <th>Tanggal Mulai</th>
                            <td>: {{$result}}</td>
                        </tr>
                        <tr>
                            <th>Tanggal Selesai</th>
                            <td>: {{$result2}}</td>
                        </tr>
                        {{-- <tr>
                            <th>Tahun</th>
                            <td>: {{$mk->rp_tahun}}</td>
                        </tr> --}}
                    </table>
                </div>

            </div>
            <div class="card-body">
                <div class="buttons">
                    <a href="/mekanikal" class="btn btn-danger">Kembali</a>
                </div>
            </div>

        </div>
    </div>
</div>

@endsection
