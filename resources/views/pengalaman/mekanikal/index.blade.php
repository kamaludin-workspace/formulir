@extends('layouts.app')

@section('dropdown-pengalaman') active
@endsection

@section('status-meka') active
@endsection

@section('title-page') Pengalaman Bidang Mekanikal
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="button">
                    <a href="/mekanikal/tambah" class="btn btn-icon btn-primary"><i class="fa fa-plus"></i>
                        Tambah Data</a>
                </div>
            </div>
            <div class="card-header">
                <h4>Data pengalaman Mekanikal</h4>
                <div class="card-header-form">
                    <form action="/mekanikal/cari" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" name="cari" value="{{ old('cari') }}" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>No.</th>
                            <th>Sub kelas</th>
                            <th>Nama proyek</th>
                            <th>Lokasi proyek</th>
                            <th>Kode lokasi</th>
                            <th>Kelas</th>
                            <th>Aksi</th>
                        </tr>
                        <?php $no = $mk->firstItem(); ?>
                        @foreach ($mk as $t)
                        <tr>
                            <td>{{$no++}}</td>
                            <td>{{$t->rp_subKlas}}</td>
                            <td>{{$t->rp_namaProyek}}</td>
                            <td>{{$t->provinsi}}</td>

                            <td>
                                {{$t->rp_kodeLok}}
                            </td>
                            <td>{{$t->sk_subKlasifikasi}}</td>
                            @if (Auth::user()->level == "admin")
                            
                            <td class="datatable-ct" align="center">
                                <a href="/mekanikal/edit/{{ $t->rp_id }}" class="btn btn-primary my-3">Edit</a>

                                <form method="POST" onclick="return confirm('Yakin Ingin Menghapus Data Ini ?')" action="/mekanikal/delete/{{ $t->rp_id }}">
                                    {{csrf_field()}} {{method_field('DELETE')}}

                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                </form>

                                <a href="/mekanikal/detail/{{ $t->rp_id }}" class="btn btn-info my-3">Detail</a>
                            </td>
                                
                            @endif
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>

            <div class="card-footer text-right">
                <nav class="d-inline-block">
                    {{ $mk->links() }}
                </nav>
            </div>
        </div>
    </div>
</div>

@endsection
