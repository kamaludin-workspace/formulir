@extends('layouts.app')

@section('dropdown-pengalaman') active
@endsection

@section('status-arsitektur') active
@endsection

@section('title-page') Pengalaman Bidang Arsitektur
@endsection

@section('content')

<section class="section">

    <div class="section-body">

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Form Data Arsitektur</h4>
                    </div>
                    <form action="/arsitektur/edit/{{$ark->rp_id}}/save" method="POST">
                        @csrf {{ method_field('patch')}}
                        <input type="text" hidden value="1" name="rp_kl_id" required placeholder="Masukan Nama Proyek"
                            class="form-control">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Nama Proyek</label>
                                <input type="text" name="rp_namaProyek" value="{{$ark->rp_namaProyek}}" required
                                    placeholder="Masukan Nama Proyek" class="form-control">
                            </div>

                            <?php
                            
                            $nilai = $ark->rp_nilai;

                            $harga = number_format($nilai, 0, "," , ".");

                            ?>

                            <div class="form-group">
                                <label>Nilai</label>
                                <input type="text" id="rupiah" name="rp_nilai" value="{{$harga}}" required
                                    placeholder="Masukan Nilai" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Tgl Mulai</label>
                                <input type="date" name="rp_tglMulai" value="{{$ark->rp_tglMulai}}" required
                                    placeholder="Masukan Tanggal Mulai" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Tgl Selesai</label>
                                <input type="date" name="rp_tglSelesai" value="{{$ark->rp_tglSelesai}}" required
                                    placeholder="Masukan Tanggal Selesai" class="form-control">
                            </div>
                            {{-- <div class="form-group">
                                <label>Tahun</label>
                                <input type=text name="rp_tahun" value="{{$ark->rp_tahun}}" required
                                    placeholder="Masukan Tahun" class="form-control">
                            </div> --}}
                            {{-- <div class="form-group">
                                <label>Jabatan</label>
                                <input type=text name="rp_jabatan" value="{{$ark->rp_jabatan}}" required
                            placeholder="Masukan Jabatan"
                            class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Tgl Mulai 2</label>
                            <input type="date" name="rp_tglMulai2" value="{{$ark->rp_tglMulai2}}" required
                                placeholder="Masukan Tanggal Mulai" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Tgl Selesai 2</label>
                            <input type="date" name="rp_tglSelesai2" value="{{$ark->rp_tglSelesai2}}" required
                                placeholder="Masukan Tanggal Selesai" class="form-control">
                        </div> --}}
                        <div class="form-group">
                            <label>Sub Klas</label>
                            <select class="form-control form-control-sm select2" required name="rp_subKlas">
                                <option value="">=== Silahkan Pilih Sub Klas ===</option>
                                @foreach ($sub_klas as $t)
                                <option value="{{$t->sk_kode}}" <?php if ($t->sk_kode == $ark->rp_subKlas) {
                                        echo "selected";
                                    } ?>>{{$t->sk_kode}} - {{$t->sk_nama}}</option>
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group">
                            <label>Provinsi</label>
                            <select class="form-control form-control-sm select2" name="rp_kodeLok" id="tampil">
                                <option value="">=== Silahkan Pilih Provinsi ===</option>
                                @foreach ($provinsi as $t)
                                <option value="{{$t->provinsi}}" <?php if ($t->id_provinsi == $ark->rp_kodeLok || $t->provinsi == $ark->rp_kodeLok) {
                                        echo "selected";
                                    } ?>>{{$t->provinsi}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="buttons">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a href="/arsitektur" class="btn btn-danger">Kembali</a>
                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</section>
</div>

@endsection