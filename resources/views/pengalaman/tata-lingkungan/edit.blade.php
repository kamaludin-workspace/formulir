@extends('layouts.app')

@section('dropdown-pengalaman') active
@endsection

@section('status-talin') active
@endsection

@section('title-page') Pengalaman Bidang Tata Lingkungan
@endsection

@section('content')

<section class="section">

    <div class="section-body">

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Form Data Tata Lingkungan</h4>
                    </div>
                    <form action="/tata-lingkungan/edit/{{$talin->rp_id}}/save" method="POST">
                        @csrf {{ method_field('patch')}}
                        <input type="text" hidden value="4" name="rp_kl_id" required placeholder="Masukan Nama Proyek"
                                    class="form-control">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Nama Proyek</label>
                                <input type="text" name="rp_namaProyek" value="{{$talin->rp_namaProyek}}" required
                                    placeholder="Masukan Nama Proyek" class="form-control">
                            </div>
                            <?php
                            
                            $nilai = $talin->rp_nilai;

                            $harga = number_format($nilai, 0, "," , ".");

                            ?>
                            <div class="form-group">
                                <label>Nilai</label>
                                <input type="text" id="rupiah" name="rp_nilai" value="{{$harga}}" required placeholder="Masukan Nilai"
                                    class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Tgl Mulai</label>
                                <input type="date" name="rp_tglMulai" value="{{$talin->rp_tglMulai}}" required placeholder="Masukan Tanggal Mulai"
                                    class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Tgl Selesai</label>
                                <input type="date" name="rp_tglSelesai" value="{{$talin->rp_tglSelesai}}" required placeholder="Masukan Tanggal Selesai"
                                    class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Sub Klas</label>
                                <select class="form-control form-control-sm select2" required name="rp_subKlas">
                                    <option value="">=== Silahkan Pilih Sub Klas ===</option>
                                    @foreach ($sub_klas as $t)
                                    <option value="{{$t->sk_kode}}"<?php if ($t->sk_kode == $talin->rp_subKlas) {
                                        echo "selected";
                                    } ?>>{{$t->sk_kode}} - {{$t->sk_nama}}</option>
                                    @endforeach

                                </select>
                            </div>

                            <div class="form-group">
                                <label>Provinsi</label>
                                <select class="form-control form-control-sm select2" name="rp_kodeLok" id="tampil">
                                    <option value="">=== Silahkan Pilih Provinsi ===</option>
                                    @foreach ($provinsi as $t)
                                    <option value="{{$t->provinsi}}"<?php if ($t->id_provinsi == $talin->rp_kodeLok || $t->provinsi == $talin->rp_kodeLok) {
                                        echo "selected";
                                    } ?>>{{$t->provinsi}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="buttons">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="/tata-lingkungan" class="btn btn-danger">Kembali</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

@endsection
