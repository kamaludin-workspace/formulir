@extends('layouts.app')

@section('laporan') active
@endsection

@if (Request::is('laporan/ustkm'))
@section('laporan-ustkm') active
@endsection
@elseif(Request::is('laporan/ustkm/view'))
@section('laporan-ustkm') active
@endsection
{{-- DPP --}}
@elseif(Request::is('laporan/dpp'))
@section('laporan-dpp') active
@endsection
@elseif(Request::is('laporan/aproval'))
@section('laporan-aproval') active
@endsection
@elseif(Request::is('laporan/periode'))
@section('laporan-periode') active
@endsection
@endif

@section('title-page') Laporan
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        @include('layouts.alert')
        <div class="card">
            <div class="card-header">
                <h4>Rekap USTKM Harian</h4>
            </div>
            <form action="{{ url('laporan/ustkm/view') }}" method="post">
                @csrf
                <div class="card-body mt-n2">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="form-label">Pilih Asosiasi</label>
                                <div class="selectgroup w-100">
                                    @if(Auth::user()->level == 'staff')
                                    <label class="selectgroup-item">
                                        <input type="radio" name="no_reg_asosiasi" value="152" class="selectgroup-input"
                                            @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '152')
                                        checked @else disabled @endif required>
                                        <span class="selectgroup-button">ATAKSI</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="radio" name="no_reg_asosiasi" value="144" class="selectgroup-input"
                                            @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '144')
                                        checked @else disabled @endif required>
                                        <span class="selectgroup-button">ASDAMKINDO</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="radio" name="no_reg_asosiasi" value="180" class="selectgroup-input"
                                            @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '180')
                                        checked @else disabled @endif required>
                                        <span class="selectgroup-button">PROTEKSI</span>
                                    </label>
                                    @elseif(Auth::user()->level == 'admin')
                                    <label class="selectgroup-item">
                                        <input type="radio" name="no_reg_asosiasi" value="" class="selectgroup-input"
                                            @if($ustkm) @if($asosiasi=='Semua Asosiasi' ) checked @endif @endif
                                            required>
                                        <span class="selectgroup-button">Semua Asosiasi</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="radio" name="no_reg_asosiasi" value="152" class="selectgroup-input"
                                            @if($ustkm) @if($asosiasi=='ATAKSI' ) checked @endif @endif required>
                                        <span class="selectgroup-button">ATAKSI</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="radio" name="no_reg_asosiasi" value="144" class="selectgroup-input"
                                            @if($ustkm) @if($asosiasi=='ASDAMKINDO' ) checked @endif @endif required>
                                        <span class="selectgroup-button">ASDAMKINDO</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="radio" name="no_reg_asosiasi" value="180" class="selectgroup-input"
                                            @if($ustkm) @if($asosiasi=='PROTEKSI' ) checked @endif @endif required>
                                        <span class="selectgroup-button">PROTEKSI</span>
                                    </label>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Tanggal Aproval</label>
                                <div class="input-group mb-3">
                                    <input type="text" name="tanggal" class="form-control laporan-group datepicker"
                                        @if($ustkm) value="{{ $ustkm[0]->tgl_surat }}" @endif required>
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="submit">Cari</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            @if(!$ustkm)
            @else
            <hr class="mt-n4 mb-n2">
            <div class="card-body">
                <span class="badge badge-info">{{ $asosiasi }}</span>
                <span class="badge badge-warning">{{ tanggal_indonesia($ustkm[0]->tgl_surat) }}</span>
                <span class="badge badge-success">{{ count($ustkm) }} permohonan</span>
                @php
                $jumlahFile = ceil(count($ustkm)/10);
                @endphp

                <ul class="nav nav-tabs mt-2" id="myTab" role="tablist">
                    @for($link=0, $links=0; $link<$jumlahFile;$link++) <li class="nav-item">
                        <a class="nav-link @if($link == 0) active @endif" data-toggle="tab" href="#home{{ $link }}"
                            role="tab" aria-controls="home" aria-selected="true">Halaman {{ $link+1 }}</a>
                        </li>
                        @endfor
                </ul>

                <div class="tab-content" id="myTabContent">

                    @for($i=0, $j=0; $i<$jumlahFile;$i++) <div class="tab-pane fade @if($i == 0) show active @endif"
                        id="home{{ $i }}" role="tabpanel">
                        <div class="table-responsive">
                            <form action="{{ url('laporan/ustkm/cetak') }}" method="post">
                                @csrf
                                <table class="table table-bordered table-md">
                                    <thead>
                                        <tr>
                                            <th rowspan="2" style="vertical-align:middle">No</th>
                                            <th rowspan="2" style="vertical-align:middle">Checkout</th>
                                            <th rowspan="2" style="vertical-align:middle">Asosiasi</th>
                                            <th rowspan="2" style="vertical-align:middle">Nama</th>
                                            <th rowspan="2" style="vertical-align:middle">No. KTP</th>
                                            <th rowspan="2" style="vertical-align:middle">No. NPWP</th>
                                            <th rowspan="2" style="vertical-align:middle">Tempat Lahir</th>
                                            <th rowspan="2" style="vertical-align:middle">Tanggal Lahir</th>
                                            <th rowspan="2" style="vertical-align:middle">Provinsi</th>
                                            <th rowspan="2" style="vertical-align:middle">Kabupaten</th>
                                            <th rowspan="2" style="vertical-align:middle">Alamat</th>
                                            <th rowspan="2" style="vertical-align:middle">Jurusan</th>
                                            <th rowspan="2" style="vertical-align:middle">Pendidikan</th>
                                            <th rowspan="2" style="vertical-align:middle">Hp Pemohon</th>
                                            <th colspan="4" style="text-align:center">Baru/Tambah Sub Bid 1</th>
                                            <th colspan="4" style="text-align:center">Baru/Tambah Sub Bid 2</th>
                                            <th colspan="4" style="text-align:center">Baru/Tambah Sub Bid 3</th>
                                        </tr>
                                        <tr>
                                            <td>Klasifikasi</td>
                                            <td>Sub Klasifikasi</td>
                                            <td>Kode</td>
                                            <td style="border-right: 2px solid black;">Kualifikasi</td>
                                            <td>Klasifikasi</td>
                                            <td>Sub Klasifikasi</td>
                                            <td>Kode</td>
                                            <td style="border-right: 2px solid black;">Kualifikasi</td>
                                            <td>Klasifikasi</td>
                                            <td>Sub Klasifikasi</td>
                                            <td>Kode</td>
                                            <td>Kualifikasi</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @for ($a = 0;$a <10;$a++) @if(($j+1)<=count($ustkm)) <tr>
                                            <td>{{ $a+1 }}.</td>
                                            <td>
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" id="getId{{ $a+1 }}" name="idPengajuan[]"
                                                        class="custom-control-input" value="{{ $ustkm[$j]->id }}"
                                                        checked>
                                                    <label class="custom-control-label" for="getId{{ $a+1 }}"></label>
                                                </div>
                                            </td>
                                            <td>{{ asosiasi($ustkm[$j]->no_reg_asosiasi) }}</td>
                                            <td>{{ $ustkm[$j]->nama_pemohon }}</td>
                                            <td>{{ $ustkm[$j]->nik }}</td>
                                            <td>{{ $ustkm[$j]->npwp }}</td>
                                            <td>{{ $ustkm[$j]->tempat_lahir }}</td>
                                            <td>{{ $ustkm[$j]->tgl_lahir }}</td>
                                            <td>{{ $ustkm[$j]->provinsi }}</td>
                                            <td>{{ $ustkm[$j]->kabkota }}</td>
                                            <td>{{ $ustkm[$j]->alamat }}</td>
                                            <td>{{ $ustkm[$j]->jurusan }}</td>
                                            <td>{{ $ustkm[$j]->jenjang_pendidikan }}</td>
                                            <td>{{ $ustkm[$j]->no_hp }}</td>
                                            @php($clear = preg_replace("/[^A-Za-z0-9,]/", '',
                                            $ustkm[$j]->id_subklasifikasi))

                                            @php($tingkatan = preg_replace("/[^A-Za-z0-9,]/", '',
                                            $ustkm[$j]->id_kualifikasi))

                                            @php($expTingkatan = explode(",", $tingkatan))
                                            @php($exp = explode(",", $clear))

                                            @foreach($kualifikasi as $key)
                                            @if($key->sk_id == $exp[0])
                                            {{-- Kualifikasi 1 --}}
                                            <td>{{ $key->sk_klasifikasi }}</td>
                                            <td>{{ $key->sk_nama }}</td>
                                            <td>{{ $key->sk_kode }}</td>
                                            <td>{{ kualifikasi($expTingkatan[0], $ustkm[$j]->no_reg_asosiasi) }}
                                            </td>
                                            @endif
                                            @endforeach
                                            {{-- Kualifikasi 2 --}}
                                            @if(!isset($exp[1]))
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            @else
                                            @foreach($kualifikasi as $key)

                                            @if($key->sk_id == $exp[1])
                                            {{-- Kualifikasi 1 --}}
                                            <td>{{ $key->sk_klasifikasi }}</td>
                                            <td>{{ $key->sk_nama }}</td>
                                            <td>{{ $key->sk_kode }}</td>
                                            <td>{{ kualifikasi($expTingkatan[1], $ustkm[$j]->no_reg_asosiasi) }}
                                            </td>
                                            @endif
                                            @endforeach
                                            @endif
                                            {{-- Kualifikasi 3 --}}
                                            @if(!isset($exp[2]))
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            @else
                                            @foreach($kualifikasi as $key)
                                            @if($key->sk_id == $exp[1])
                                            {{-- Kualifikasi 1 --}}
                                            <td>{{ $key->sk_klasifikasi }}</td>
                                            <td>{{ $key->sk_nama }}</td>
                                            <td>{{ $key->sk_kode }}</td>
                                            <td>{{ kualifikasi($expTingkatan[2], $ustkm[$j]->no_reg_asosiasi) }}
                                            </td>
                                            @endif
                                            @endforeach
                                            @endif
                                            </tr>
                                            @php($j++)
                                            @endif
                                            @endfor
                                    </tbody>
                                </table>
                                <button type="submit" class="btn btn-lg btn-block btn-icon btn-primary">Cetak</button>
                            </form>
                        </div>
                </div>
                @endfor
            </div>
        </div>
        @endif
    </div>
</div>
</div>
@endsection
