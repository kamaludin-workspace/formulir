@extends('layouts.app')

@section('laporan') active
@endsection

@if (Request::is('laporan/ustkm'))
@section('laporan-ustkm') active
@endsection
@elseif(Request::is('laporan/ustkm/view'))
@section('laporan-ustkm') active
@endsection
{{-- DPP --}}
@elseif(Request::is('laporan/dpp'))
@section('laporan-dpp') active
@endsection
@elseif(Request::is('laporan/dpp/view'))
@section('laporan-ustkm') active
@endsection

@elseif(Request::is('laporan/periode'))
@section('laporan-periode') active
@endsection
@endif

@section('title-page') Laporan
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        @include('layouts.alert')
        <div class="card">
            <div class="card-header">
                <h4>Rekap DPP/APPROVAL Harian</h4>
            </div>
            <form action="{{ url('laporan/dpp/view') }}" method="post">
                @csrf
                <div class="card-body mt-n2">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="form-label">Pilih Asosiasi</label>
                                <div class="selectgroup w-100">
                                  @if(Auth::user()->level == 'staff')
                                      <label class="selectgroup-item">
                                          <input type="radio" name="no_reg_asosiasi" value="152" class="selectgroup-input" @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '152') checked @else disabled @endif required>
                                          <span class="selectgroup-button">ATAKSI</span>
                                      </label>
                                      <label class="selectgroup-item">
                                          <input type="radio" name="no_reg_asosiasi" value="144" class="selectgroup-input" @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '144') checked @else disabled @endif required>
                                          <span class="selectgroup-button">ASDAMKINDO</span>
                                      </label>
                                      <label class="selectgroup-item">
                                          <input type="radio" name="no_reg_asosiasi" value="180" class="selectgroup-input" @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '180') checked @else disabled @endif required>
                                          <span class="selectgroup-button">PROTEKSI</span>
                                      </label>
                                  @elseif(Auth::user()->level == 'admin')
                                    <label class="selectgroup-item">
                                        <input type="radio" name="no_reg_asosiasi" value="" class="selectgroup-input" required>
                                        <span class="selectgroup-button">Semua Asosiasi</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="radio" name="no_reg_asosiasi" value="152" class="selectgroup-input" required>
                                        <span class="selectgroup-button">ATAKSI</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="radio" name="no_reg_asosiasi" value="144" class="selectgroup-input" required>
                                        <span class="selectgroup-button">ASDAMKINDO</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="radio" name="no_reg_asosiasi" value="180" class="selectgroup-input" required>
                                        <span class="selectgroup-button">PROTEKSI</span>
                                    </label>
                                  @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="">Tanggal Aproval</label>
                                <div class="input-group mb-3">
                                    <input type="text" name="tanggal" class="form-control laporan-group datepicker" required>
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="submit">
                                          <i class="fas fa-print"></i>
                                          Cetak
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
      </div>
  </div>
</div>
@endsection
