@extends('layouts.app')

@section('laporan') active
@endsection

@if (Request::is('laporan/ustkm'))
@section('laporan-ustkm') active
@endsection
@elseif(Request::is('laporan/dpp'))
@section('laporan-dpp') active
@endsection
@elseif(Request::is('laporan/aproval'))
@section('laporan-aproval') active
@endsection
@elseif(Request::is('laporan/periode'))
@section('laporan-periode') active
@endsection
@endif

@section('title-page') Laporan
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        @include('layouts.alert')
        <div class="card">
            <form method="POST" action="{{ url('laporan/periode/print') }}">
                @csrf
                <div class="card-header">
                    <h4>Laporan Periode</h4>
                </div>
                <div class="card-body mt-n3">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label class="form-label">Pilih Asosiasi</label>
                                <div class="selectgroup w-100">
                                  @if(Auth::user()->level == 'staff')
                                      <label class="selectgroup-item">
                                          <input type="radio" name="no_reg_asosiasi" value="152" class="selectgroup-input" @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '152') checked @else disabled @endif required>
                                          <span class="selectgroup-button">ATAKSI</span>
                                      </label>
                                      <label class="selectgroup-item">
                                          <input type="radio" name="no_reg_asosiasi" value="144" class="selectgroup-input" @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '144') checked @else disabled @endif required>
                                          <span class="selectgroup-button">ASDAMKINDO</span>
                                      </label>
                                      <label class="selectgroup-item">
                                          <input type="radio" name="no_reg_asosiasi" value="180" class="selectgroup-input" @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '180') checked @else disabled @endif required>
                                          <span class="selectgroup-button">PROTEKSI</span>
                                      </label>
                                  @elseif(Auth::user()->level == 'admin')
                                    <label class="selectgroup-item">
                                        <input type="radio" name="no_reg_asosiasi" value="" class="selectgroup-input" required>
                                        <span class="selectgroup-button">Semua Asosiasi</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="radio" name="no_reg_asosiasi" value="152" class="selectgroup-input" required>
                                        <span class="selectgroup-button">ATAKSI</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="radio" name="no_reg_asosiasi" value="144" class="selectgroup-input" required>
                                        <span class="selectgroup-button">ASDAMKINDO</span>
                                    </label>
                                    <label class="selectgroup-item">
                                        <input type="radio" name="no_reg_asosiasi" value="180" class="selectgroup-input" required>
                                        <span class="selectgroup-button">PROTEKSI</span>
                                    </label>
                                  @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label class="form-label">Jenis Laporan</label>
                                <div class="selectgroup w-100">
                                  <label class="selectgroup-item">
                                      <input type="radio" name="laporan" value="ustkm" class="selectgroup-input" required>
                                      <span class="selectgroup-button">REKAP USTKM</span>
                                  </label>
                                  <label class="selectgroup-item">
                                      <input type="radio" name="laporan" value="approval" class="selectgroup-input" required>
                                      <span class="selectgroup-button">REKAP DPP/APPROVAL</span>
                                  </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                      <div class="form-group col-6">
                        <label>Tanggal Mulai</label>
                        <input type="text" autocomplete="off" class="form-control datepicker" name="mulai" required>
                      </div>
                      <div class="form-group col-6">
                        <label>Tanggal Selesai</label>
                        <input type="text" autocomplete="off" class="form-control datepicker" name="selesai" required>
                      </div>
                    </div>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-secondary" type="reset">Reset</button>
                    <button type="submit" class="btn btn-primary ml-2">Cetak</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

