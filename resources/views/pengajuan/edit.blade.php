@extends('layouts.app')

@section('status-data-pengajuan') active
@endsection
@if ($pengajuan->status == 'acc')
@section('pengajuan-selesai') active
@endsection
@else
@section('pengajuan-proses') active
@endsection
@endif


@section('title-page') Pengajuan
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <form action="{{ url('pengajuan/update', [$pengajuan->id]) }}" method="post">
                @csrf
                @method('patch')
                <div class="card-header">
                    <h4>Formulir permohonan SKT/SKA</h4>
                </div>
                <div class="card-body mt-n4">
                    <div class="section-title">Data Diri</div>
                    <div class="form-group">
                        <label>Nama lengkap</label>
                        <input type="text" name="nama_pemohon" class="form-control form-control-sm"
                            placeholder="Nama pemohon" autocomplete="off" value="{{ $pengajuan->nama_pemohon }}"
                            required>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Tempat lahir</label>
                                <input type="text" name="tempat_lahir" class="form-control form-control-sm"
                                    placeholder="Tempat lahir" autocomplete="off" value="{{ $pengajuan->tempat_lahir }}"
                                    required>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Tanggal lahir</label>
                                <div class="input-group">
                                    <div class="input-group-prepend"> 
                                        <div class="input-group-text" style="height: 89%; cursor: pointer;" id="pasteDate">
                                            <i class="fas fa-clipboard" id="icon"></i>
                                        </div>
                                    </div>
                                    <input type="text" id="tgl_lahir" name="tgl_lahir" maxlength="20" value="{{ $pengajuan->tgl_lahir }}" placeholder="Tanggal lahir" maxlength="20" class="form-control form-control-sm">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea class="form-control form-control-sm" rows="5" placeholder="Alamat lengkap"
                            autocomplete="off" name="alamat" placeholder="Alamat lengkap"
                            required>{{ $pengajuan->alamat }}</textarea>
                    </div>

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Provinsi</label>
                                <select class="form-control form-control-sm" name="id_provinsi" required>
                                    <option class="pilih_provinsi">Pilih Provinsi</option>
                                    @foreach ($provinsi as $key)
                                    <option value="{{ $key->id_provinsi }}" @if($pengajuan->id_provinsi ==
                                        $key->id_provinsi) selected @endif>{{ $key->provinsi }}</option>
                                    @endforeach
                                </select>
                                <label class="mt-2">Kecamatan</label>
                                <select class="form-control form-control-sm" name="id_kecamatan" required>
                                    @foreach ($kecamatan as $value)
                                    <option value="{{ $value->id_kecamatan }}" @if($pengajuan->id_kecamatan ==
                                        $value->id_kecamatan) selected @endif>{{ $value->kecamatan }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label id="labelkab">Kabupaten</label>
                                <select class="form-control form-control-sm" name="id_kabkota" required>
                                    @foreach ($kabkota as $value)
                                    <option value="{{ $value->id_kabkota }}" @if($pengajuan->id_kabkota ==
                                        $value->id_kabkota) selected @endif>{{ $value->kabkota }}</option>
                                    @endforeach
                                </select>
                                <label class="mt-2">Kelurahan</label>
                                <select class="form-control form-control-sm" name="id_keldes" required>
                                    @foreach ($keldes as $value)
                                    <option value="{{ $value->id_keldes }}" @if($pengajuan->id_keldes ==
                                        $value->id_keldes) selected @endif>{{ $value->keldes }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Nomor KTP</label>
                        <input type="text" name="nik" value="{{ $pengajuan->nik }}" class="form-control form-control-sm"
                            placeholder="Nomor KTP pemohon" autocomplete="off" onkeypress="return numberFilter(event)"
                            maxlength="16" required>
                    </div>
                    <div class="form-group">
                        <label>Nomor NPWP <small><code>optional</code></small></label>
                        <input type="text" name="npwp" value="{{ $pengajuan->npwp }}"
                            class="form-control form-control-sm" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Pilih Asosiasi</label>
                        <div class="selectgroup w-100">
                            @if(Auth::user()->level == 'staff')
                            <label class="selectgroup-item">
                                <input type="radio" name="no_reg_asosiasi" value="152" class="selectgroup-input"
                                    @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '152') checked
                                @else disabled @endif required>
                                <span class="selectgroup-button">ATAKSI</span>
                            </label>
                            <label class="selectgroup-item">
                                <input type="radio" name="no_reg_asosiasi" value="144" class="selectgroup-input"
                                    @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '144') checked
                                @else disabled @endif required>
                                <span class="selectgroup-button">ASDAMKINDO</span>
                            </label>
                            <label class="selectgroup-item">
                                <input type="radio" name="no_reg_asosiasi" value="180" class="selectgroup-input"
                                    @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '180') checked
                                @else disabled @endif required>
                                <span class="selectgroup-button">PROTEKSI</span>
                            </label>
                            @elseif(Auth::user()->level == 'admin')
                            <label class="selectgroup-item">
                                <input type="radio" name="no_reg_asosiasi" value="152" class="selectgroup-input"
                                    @if($pengajuan->no_reg_asosiasi == '152') checked @endif required>
                                <span class="selectgroup-button">ATAKSI</span>
                            </label>
                            <label class="selectgroup-item">
                                <input type="radio" name="no_reg_asosiasi" value="144" class="selectgroup-input"
                                    @if($pengajuan->no_reg_asosiasi == '144') checked @endif required>
                                <span class="selectgroup-button">ASDAMKINDO</span>
                            </label>
                            <label class="selectgroup-item">
                                <input type="radio" name="no_reg_asosiasi" value="180" class="selectgroup-input"
                                    @if($pengajuan->no_reg_asosiasi == '180') checked @endif required>
                                <span class="selectgroup-button">PROTEKSI</span>
                            </label>
                            @endif

                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Nomor Telepon <small><code>optional</code></small></label>
                                <input type="text" name="no_hp" value="{{ $pengajuan->no_hp }}"
                                    class="form-control form-control-sm" autocomplete="off">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Alamat email <small><code>optional</code></small></label>
                                <input type="text" name="email" value="{{ $pengajuan->email }}"
                                    class="form-control form-control-sm" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="section-title">Data Pendidikan</div>
                    <div class="row">
                        <div class="col-auto">
                            <div class="form-group">
                                <label>Jenjang pendidikan</label>
                                <select class="form-control" name="jenjang_pendidikan" required>
                                    <option>Pilih pendidikan</option>
                                    <option value="SMP" @if($pengajuan->jenjang_pendidikan == 'SMP') selected @endif>SMP/Sederajat</option>
                                    <option value="SMA" @if($pengajuan->jenjang_pendidikan == 'SMA') selected @endif>SMA/Sederajat</option>
                                    <option value="D1" @if($pengajuan->jenjang_pendidikan == 'D1') selected @endif>D1</option>
                                    <option value="D2" @if($pengajuan->jenjang_pendidikan == 'D2') selected @endif>D2</option>
                                    <option value="D3" @if($pengajuan->jenjang_pendidikan == 'D3') selected @endif>D3</option>
                                    <option value="D4" @if($pengajuan->jenjang_pendidikan == 'D4') selected @endif>D4</option>
                                    <option value="S1" @if($pengajuan->jenjang_pendidikan == 'S1') selected @endif>S1/Sarjana</option>
                                    <option value="S2" @if($pengajuan->jenjang_pendidikan == 'S2') selected @endif>S2/Magister</option>
                                    <option value="S3" @if($pengajuan->jenjang_pendidikan == 'S3') selected @endif>S3/Doktor</option>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Jurusan</label>
                                <input type="text" name="jurusan" value="{{ $pengajuan->jurusan }}" class="form-control"
                                    placeholder="Jurusan/bidang" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-9">
                            <div class="form-group">
                                <label>Nama sekolah</label>
                                <input type="text" name="nama_sekolah" value="{{ $pengajuan->nama_sekolah }}"
                                    class="form-control form-control-sm" placeholder="Nama sekolah/lembaga"
                                    autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label>Tahun lulus</label>
                                <input type="text" name="tahun_lulus" value="{{ $pengajuan->tahun_lulus }}"
                                    class="form-control form-control-sm" placeholder="Tahun lulus"
                                    onkeypress="return numberFilter(event)" maxlength="4" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <input type="text" name="alamat_sekolah" value="{{ $pengajuan->alamat_sekolah }}"
                            class="form-control form-control-sm" placeholder="Alamat sekolah" autocomplete="off"
                            required>
                    </div>
                    <div class="form-group">
                        <label>Nomor ijazah</label>
                        <input type="text" name="nomor_ijazah" value="{{ $pengajuan->nomor_ijazah }}"
                            class="form-control form-control-sm" placeholder="Nomor ijazah" autocomplete="off" required>
                    </div>
                    <div class="section-title">Data Kualifikasi</div>
                    <div class="row">
                        <div class="form-group col-6">
                            <label class="d-block">Jenis permohonan</label>
                            <div class="form-check custom-control custom-radio form-check-inline">
                                <input type="radio" id="tipe_permohonan_1" name="tipe_permohonan" value="1" class="custom-control-input" required {{ ($pengajuan->tipe_permohonan == 'ska') ? 'checked' : '' }}>
                                <label class="custom-control-label" for="tipe_permohonan_1">SKA (Sertifikat tenaga ahli) </label>
                            </div>
                            <div class="form-check custom-control custom-radio form-check-inline">
                                <input type="radio" id="tipe_permohonan_2" name="tipe_permohonan" value="2"
                                    class="custom-control-input" required
                                    {{ ($pengajuan->tipe_permohonan == 'skt') ? 'checked' : '' }}>
                                <label class="custom-control-label" for="tipe_permohonan_2">SKT (Sertifikat tenaga terampil) </label>
                            </div>
                        </div>
                        <div class="form-group col-6">
                            <label>Klasifikasi</label>
                            <select class="form-control form-control-sm" name="id_klasifikasi">
                                <option value="pilih_klasifikasi">Pilih klasifikasi</option>
                                @foreach ($klasifikasi as $data)
                                <option value="{{ $data->kl_id }}" {{ ($pengajuan->id_klasifikasi == $data->kl_id) ? 'selected' : '' }}>{{ $data->kl_nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                @php($jumlahKualifikasi = count($pengajuan->id_subklasifikasi))
                
                @php($countKs = 0)
                @for ($i=0; $i < $jumlahKualifikasi; $i++)
                    {{-- Get count array --}}
                    @php($n = $countKs++)
                    <div class="row" @if($n == 1 ) id="secondElement" @endif @if($n == 2 ) id="thirdElement" @endif>
                        <div class="form-group col-4">
                            <label>Sub klasifikasi</label><br>
                            <select class="form-control form-control-sm select2" name="id_subklasifikasi[]" id="sub1">
                                @foreach ($subklasifikasi as $value)
                                <option value="{{ $value->sk_id }}" @if($pengajuan->id_subklasifikasi[$n] ==
                                    $value->sk_id) selected @endif>{{ $value->sk_kode }}/{{ $value->sk_nama }}
                                </option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group col-5">
                            <label>Kualifikasi</label>
                            <select class="form-control form-control-sm " name="id_kualifikasi[]">
                                @foreach ($kualifikasi as $data)
                                <option value="{{ $data->id }}" @if($pengajuan->id_kualifikasi[$n] == $data->id) selected @endif>{{ $data->nama }}/DPP-{{ number_format($data->potongan1) }}/USTKM-{{ number_format($data->potongan2) }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-2">
                            <label>Keterangan</label>
                            <input id="ket_1" type="text" name="keterangan[]"
                                value="{{ $pengajuan->keterangan[$n] }}" class="form-control keterangan"
                                value="Baru">
                        </div>
                        <div class="col-auto text-right">
                            <label>Aksi</label><br>
                            @if($n == 0)
                            <button type="button" class="btn btn-icon btn-success text-white" id="addElement" @if($jumlahKualifikasi>= 3) disabled @endif ><li class="fa fa-plus"></li></button>
                            @endif
                            @if($n == 1)
                            <button type="button" class="btn btn-icon btn-danger text-white" id="removeSecond"><li class="fa fa-trash"></li></button>
                            @endif
                            @if($n == 2)
                            <button type="button" class="btn btn-icon btn-danger text-white" id="removeThird"><li class="fa fa-trash"></li></button>
                            @endif
                        </div>
                    </div>
                @endfor
                    @if($jumlahKualifikasi == 1)
                    <div class="row" id="secondElement"></div>
                    <div class="row" id="thirdElement"></div>
                    @endif
                    @if($jumlahKualifikasi == 2)
                    <div class="row" id="thirdElement"></div>
                    @endif
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-secondary" type="reset">Reset</button>
                <button type="submit" class="btn btn-primary ml-2">Submit</button>
            </div>
            </div>
            
        </form>
</div>
</div>
</div>
@endsection