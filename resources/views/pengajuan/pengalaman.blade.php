@extends('layouts.app')

@section('status-data-pengajuan') active
@endsection

@if ($users[0]->status == 'acc')
  @section('pengajuan-selesai') active
  @endsection
@else
  @section('pengajuan-proses') active
  @endsection
@endif

@section('title-page') Pengajuan
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>Kelola pengalaman pekerjaan</h4>
            </div>
            <form method="post" action="{{ url('pengajuan/pengalaman/store') }}">
            @csrf
            <div class="card-body mt-n4">

                <div class="section-title">Data Diri</div>
                <div class="row">
                    <div class="col-8">
                        @foreach ($users as $users)
                        <input type="hidden" name="id_pengajuan" value="{{ $users->id }}">
                        <address class="mt-n1" style="line-height: 180%;">
                            <strong>Data Persona:</strong><br>
                            Nama : {{ $users->nama_pemohon }}<br>
                            Tempat, tgl lahir : {{ $users->tempat_lahir }}, {{ $users->tgl_lahir }}<br>
                            Alamat : {{ $users->alamat }}<br>
                            Daerah : {{ $users->provinsi }}, {{ $users->kabkota }}, {{ $users->kecamatan }}, {{ $users->keldes }} <br>
                            Pendidikan : {{ $users->jenjang_pendidikan }} - {{ $users->jurusan }}, {{ $users->nama_sekolah }} ({{ $users->tahun_lulus }})
                        </address>
                        @endforeach
                    </div>
                    <div class="col-4 text-md-right"></div>
                </div>
                <div class="section-title">Permohonan Kualifikasi</div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-md">
                        <tr>
                            <th data-width="40">#</th>
                            <th>Kode</th>
                            <th class="text-center">Klasifikasi</th>
                            <th class="text-left">Sub klasifikasi</th>
                            <th class="text-left">Tingkat</th>
                        </tr>
                        @php($i=1)
                        @php($k=0)
                        @foreach ($result as $key)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>{{ $key->sk_klasifikasi }}</td>
                            <td class="text-center">
                                @switch($key->sk_kl_id)
                                    @case(1)
                                    <span class="badge badge-primary">Arsitektur</span>
                                    @break
                                    @case(2)
                                    <span class="badge badge-secondary">Mekanikal</span>
                                    @break
                                    @case(3)
                                    <span class="badge badge-success">Elektrikal</span>
                                    @break
                                    @case(4)
                                    <span class="badge badge-danger">Tata Lingkungan</span>
                                    @break
                                    @case(5)
                                    <span class="badge badge-warning">Manajemen</span>
                                    @break
                                    @case(6)
                                    <span class="badge badge-info">Sipil</span>
                                    @break
                                    @default
                                    <span class="badge badge-outline-danger">Kesalahan</span>
                                @endswitch
                            </td>
                            <td class="text-left">{{ $key->sk_kode }}-{{ $key->sk_nama }}</td>
                            <td class="text-left">{{ Kualifikasi($pengajuan->id_kualifikasi[$k++], $pengajuan->no_reg_asosiasi) }}</td>
                        </tr>
                        @endforeach

                    </table>
                </div>
                <div class="section-title ">Data Pengalaman
                    <button type="reset" value="Reset" onClick="window.location.reload()" class="btn btn-icon btn-sm icon-left btn-info float-right mb-2"><i class="fas fa-sync-alt"></i> Randomize</button>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-md" id="pengalaman">
                        <tr>
                            <th data-width="40">#</th>
                            <th>Pengalaman</th>
                            <th class="text-center">Lokasi</th>
                            <th class="text-center">Nilai Kontrak</th>
                            <th class="text-right">Tgl mulai</th>
                            <th class="text-right">Tgl selesai</th>
                            <th class="text-right">Jabatan</th>
                        </tr>
                        @php($no = 1)
                        {{-- Row Pengalaman 1 --}}
                        @if(!empty($pengalaman1))
                            @php($isi1 = count($pengalaman1))
                            @php($isi2 = 0)
                            @php($isi3 = 0)
                        @foreach ($pengalaman1 as $data)
                            <tr id="dataRow{{ $data->rp_id }}">
                            <input type="hidden" name="rp_subKlas[]" value="{{ $data->rp_subklas }}">
                            <input type="hidden" name="rp_namaProyek[]" value="{{ $data->rp_namaproyek }}">
                            <input type="hidden" name="rp_lokasiProp[]" value="{{ $data->rp_lokasiProp }}">
                            <input type="hidden" name="rp_kodeLok[]" value="{{ $data->rp_kodelok }}">
                            <input type="hidden" name="rp_nilai[]" value="{{ $data->rp_nilai }}">
                            <input type="hidden" name="rp_tglMulai[]" value="{{ $data->rp_tglMulai }}">
                            <input type="hidden" name="rp_tglSelesai[]" value="{{ $data->rp_tglSelesai }}">
                            <input type="hidden" name="rp_tahun[]" value="{{ $data->tahun }}">
                                <td>{{ $no++ }}.1</td>
                                <td>{{ $data->rp_namaproyek }}</td>
                                <td class="text-center">{{ $data->provinsi }}</td>
                                <td class="text-center">Rp. {{ number_format($data->rp_nilai) }}</td>
                                <td class="text-right">{{ $data->rp_tglMulai }}</td>
                                <td class="text-right">{{ $data->rp_tglSelesai }}</td>
                                <td class="text-right">{{ $data->sk_nama }}</td>
                                <td class="text-right">
                                    <button type="button" id="{{ $data->rp_id }}" class="btn btn-icon btn-sm btn-danger getId"><i class="fas fa-trash"></i></button>
                                </td>
                            </tr>
                        @endforeach
                        @endif

                        {{-- Row pengalaman 2 --}}
                        @if(!empty($pengalaman2))
                            @php($isi2 = count($pengalaman2))
                            @php($isi3 = 0)
                            @foreach ($pengalaman2 as $data)
                                <tr id="dataRow{{ $data->rp_id }}">
                                <input type="hidden" name="rp_subKlas[]" value="{{ $data->rp_subklas }}">
                                <input type="hidden" name="rp_namaProyek[]" value="{{ $data->rp_namaproyek }}">
                                <input type="hidden" name="rp_lokasiProp[]" value="{{ $data->rp_lokasiProp }}">
                                <input type="hidden" name="rp_kodeLok[]" value="{{ $data->rp_kodelok }}">
                                <input type="hidden" name="rp_nilai[]" value="{{ $data->rp_nilai }}">
                                <input type="hidden" name="rp_tglMulai[]" value="{{ $data->rp_tglMulai }}">
                                <input type="hidden" name="rp_tglSelesai[]" value="{{ $data->rp_tglSelesai }}">
                                <input type="hidden" name="rp_tahun[]" value="{{ $data->tahun }}">
                                    <td>{{ $no++ }}.1</td>
                                    <td>{{ $data->rp_namaproyek }}</td>
                                    <td class="text-center">{{ $data->provinsi }}</td>
                                    <td class="text-center">Rp. {{ number_format($data->rp_nilai) }}</td>
                                    <td class="text-right">{{ $data->rp_tglMulai }}</td>
                                    <td class="text-right">{{ $data->rp_tglSelesai }}</td>
                                    <td class="text-right">{{ $data->sk_nama }}</td>
                                    <td class="text-right">
                                        <button type="button" id="{{ $data->rp_id }}" class="btn btn-icon btn-sm btn-danger getId"><i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif

                        {{-- Row pengalaman 3 --}}
                        @if(!empty($pengalaman3))
                            @php($isi3 = count($pengalaman3))
                            @foreach ($pengalaman3 as $data)
                                <tr id="dataRow{{ $data->rp_id }}">
                                <input type="hidden" name="rp_subKlas[]" value="{{ $data->rp_subklas }}">
                                <input type="hidden" name="rp_namaProyek[]" value="{{ $data->rp_namaproyek }}">
                                <input type="hidden" name="rp_lokasiProp[]" value="{{ $data->rp_lokasiProp }}">
                                <input type="hidden" name="rp_kodeLok[]" value="{{ $data->rp_kodelok }}">
                                <input type="hidden" name="rp_nilai[]" value="{{ $data->rp_nilai }}">
                                <input type="hidden" name="rp_tglMulai[]" value="{{ $data->rp_tglMulai }}">
                                <input type="hidden" name="rp_tglSelesai[]" value="{{ $data->rp_tglSelesai }}">
                                <input type="hidden" name="rp_tahun[]" value="{{ $data->tahun }}">
                                    <td>{{ $no++ }}.1</td>
                                    <td>{{ $data->rp_namaproyek }}</td>
                                    <td class="text-center">{{ $data->provinsi }}</td>
                                    <td class="text-center">Rp. {{ number_format($data->rp_nilai) }}</td>
                                    <td class="text-right">{{ $data->rp_tglMulai }}</td>
                                    <td class="text-right">{{ $data->rp_tglSelesai }}</td>
                                    <td class="text-right">{{ $data->sk_nama }}</td>
                                    <td class="text-right">
                                        <button type="button" id="{{ $data->rp_id }}" class="btn btn-icon btn-sm btn-danger getId"><i class="fas fa-trash"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
            @if($isi1 > 0 || $isi2 > 0 || $isi3 > 0)
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary ml-2">Simpan pengalaman</a>
            </div>
            @else
            <p class="text-center">
                <kbd class="bg-danger">Pengalaman yang cocok tidak ditemukan</kbd>
            </p>
            <p class="text-center">
                <a href="{{ url('pengajuan/detail', [$users->id]) }}"><kbd>Klik disini</kbd></a> untuk melanjutkan penambahan data secara manual
            </p>
            @endif
            </form>
        </div>
    </div>
</div>
@endsection
