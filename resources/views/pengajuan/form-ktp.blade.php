@extends('layouts.app')

@section('status-form-pengajuan') active
@endsection

@section('title-page') Pengajuan
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>Formulir permohonan SKT/SKA</h4>
                <div class="card-header-action">
                    <div class="btn-group">
                        <a href="{{ url('pengajuan/form') }}" class="btn btn-primary">Pengajuan Baru</a>
                        <a href="javascript:void(0)" class="btn">Nomor KTP</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <form action="{{ url('pengajuan/checknik') }}" method="post">
                    <div class="form-group">
                        <label>Nomor KTP</label>
                        <div class="input-group mb-3">
                            @csrf
                            <input type="search" name="nik" class="form-control" placeholder="Nomor KTP" autocomplete="off" value="{{ old('nik') }}" onkeypress="return numberFilter(event)" maxlength="16">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">Cari</button>
                            </div>
                        </div>
                    </div>
                </form>
                @if(!empty($pengajuan))
                    <hr>
                    <div class="section-title">Data Diri</div>
                    <address class="mt-n1" style="line-height: 180%;">
                        <strong>Persona:</strong><br>
                        Nama : {{ $pengajuan->nama_pemohon }}<br>
                        NIK : {{ $pengajuan->nik }}<br>
                        Tempat, tgl lahir : {{ $pengajuan->tempat_lahir }}, {{ $pengajuan->tgl_lahir }}
                    </address>
                    <form action="{{ url('pengajuan/store') }}" method="post">
                        @csrf
                        <input type="hidden" name="nik" value="{{ $pengajuan->nik }}">
                        <input type="hidden" name="nama_pemohon" value="{{ $pengajuan->nama_pemohon }}">
                        <input type="hidden" name="tempat_lahir" value="{{ $pengajuan->tempat_lahir }}">
                        <input type="hidden" name="tgl_lahir" value="{{ $pengajuan->tgl_lahir }}">
                        <input type="hidden" name="alamat" value="{{ $pengajuan->alamat }}">
                        <input type="hidden" name="id_provinsi" value="{{ $pengajuan->id_provinsi }}">
                        <input type="hidden" name="id_kabkota" value="{{ $pengajuan->id_kabkota }}">
                        <input type="hidden" name="id_kecamatan" value="{{ $pengajuan->id_kecamatan }}">
                        <input type="hidden" name="id_keldes" value="{{ $pengajuan->id_keldes }}">
                        <input type="hidden" name="npwp" value="{{ $pengajuan->npwp }}">
                        <input type="hidden" name="no_hp" value="{{ $pengajuan->no_hp }}">
                        <input type="hidden" name="email" value="{{ $pengajuan->email }}">
                        <input type="hidden" name="jenjang_pendidikan" value="{{ $pengajuan->jenjang_pendidikan }}">
                        <input type="hidden" name="jurusan" value="{{ $pengajuan->jurusan }}">
                        <input type="hidden" name="nama_sekolah" value="{{ $pengajuan->nama_sekolah }}">
                        <input type="hidden" name="alamat_sekolah" value="{{ $pengajuan->alamat_sekolah }}">
                        <input type="hidden" name="tahun_lulus" value="{{ $pengajuan->tahun_lulus }}">
                        <input type="hidden" name="nomor_ijazah" value="{{ $pengajuan->nomor_ijazah }}">
                        <div class="form-group">
                            <label class="form-label">Asosiasi</label>
                            <div class="selectgroup w-100">
                                @if(Auth::user()->level == 'staff')
                                <label class="selectgroup-item">
                                    <input type="radio" name="no_reg_asosiasi" value="152" class="selectgroup-input" @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '152') checked
                                    @else disabled @endif required>
                                    <span class="selectgroup-button">ATAKSI</span>
                                </label>
                                <label class="selectgroup-item">
                                    <input type="radio" name="no_reg_asosiasi" value="144" class="selectgroup-input" @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '144') checked
                                    @else disabled @endif required>
                                    <span class="selectgroup-button">ASDAMKINDO</span>
                                </label>
                                <label class="selectgroup-item">
                                    <input type="radio" name="no_reg_asosiasi" value="180" class="selectgroup-input" @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '180') checked
                                    @else disabled @endif required>
                                    <span class="selectgroup-button">PROTEKSI</span>
                                </label>
                                @elseif(Auth::user()->level == 'admin')
                                <label class="selectgroup-item">
                                    <input type="radio" name="no_reg_asosiasi" value="152" class="selectgroup-input" required>
                                    <span class="selectgroup-button">ATAKSI</span>
                                </label>
                                <label class="selectgroup-item">
                                    <input type="radio" name="no_reg_asosiasi" value="144" class="selectgroup-input" required>
                                    <span class="selectgroup-button">ASDAMKINDO</span>
                                </label>
                                <label class="selectgroup-item">
                                    <input type="radio" name="no_reg_asosiasi" value="180" class="selectgroup-input" required>
                                    <span class="selectgroup-button">PROTEKSI</span>
                                </label>
                                @endif
                            </div>
                        </div>
                        <div class="section-title">Data Kualifikasi</div>
                        <div class="row">
                            <div class="form-group col-6">
                                <label class="d-block">Jenis permohonan</label>
                                <div class="form-check custom-control custom-radio form-check-inline">
                                    <input type="radio" id="tipe_permohonan_1" name="tipe_permohonan" value="1" class="custom-control-input" required>
                                    <label class="custom-control-label" for="tipe_permohonan_1">SKA (Sertifikat tenaga ahli)</label>
                                </div>
                                <div class="form-check custom-control custom-radio form-check-inline">
                                    <input type="radio" id="tipe_permohonan_2" name="tipe_permohonan" value="2" class="custom-control-input" required>
                                    <label class="custom-control-label" for="tipe_permohonan_2">SKT (Sertifikat tenaga terampil)</label>
                                </div>
                            </div>
                            <div class="form-group col-6">
                                <label>Klasifikasi</label>
                                <select class="form-control form-control-sm" name="id_klasifikasi" disabled>
                                    <option value="pilih_klasifikasi">Pilih klasifikasi</option>
                                    @foreach ($klasifikasi as $data)
                                    <option value="{{ $data->kl_id }}">{{ $data->kl_nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-4">
                                <label for="">Sub klasifikasi 1</label>
                                <select class="form-control select2" name="id_subklasifikasi[]" required>
                                    <option value="">Subkualifikasi</option>
                                </select>
                            </div>
                            <div class="form-group col-4">
                                <label>Kualifikasi 1</label>
                                <select class="form-control" name="id_kualifikasi[]" required>
                                    <option value="">Kualifikasi</option>
                                </select>
                            </div>
                            <div class="form-group col-3">
                                <label>Keterangan 1</label>
                                <input type="text" name="keterangan[]" class="form-control keterangan" value="Baru">
                            </div>
                            <div class="col-auto text-right">
                                <label>Aksi</label><br>
                                <button type="button" class="btn btn-icon btn-success text-white" id="addElement">
                                    <li class="fa fa-plus"></li>
                                </button>
                            </div>
                        </div>
                        <div class="row" id="secondElement"></div>
                        <div class="row" id="thirdElement"></div>
                        <div class="card-footer text-right">
                            <button class="btn btn-secondary" type="reset">Reset</button>
                            <button type="submit" class="btn btn-primary ml-2">Submit</button>
                        </div>
                    </form>
                @else
                    @if(!empty($nik))
                    <hr>
                    <div class="text-center mt-5">
                        <h3 class="text-warning"><u>{{ $nik }}</u> Tidak ditemukan</h3>
                    </div>
                    @endif
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
