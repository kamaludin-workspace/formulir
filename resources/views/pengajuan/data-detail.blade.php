@extends('layouts.app')

@section('status-data-pengajuan') active
@endsection
@if ($users[0]->status == 'acc')
@section('pengajuan-selesai') active
@endsection
@else
@section('pengajuan-proses') active
@endsection
@endif

@section('title-page') Pengajuan
@endsection

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">
<div class="row">
    <div class="col-12">
        @include('layouts.alert')
        <div class="card">
            <div class="card-header">
                <h4>Data permohonan <span class="text-uppercase">{{ $users[0]->tipe_permohonan }}</span>
                    <br>
                    <small>
                        {{ asosiasi($users[0]->no_reg_asosiasi) }}
                    </small>
                </h4>
                <div class="card-header-action">
                    <div class="dropdown">
                        <a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle">Aksi</a>
                        <div class="dropdown-menu dropdown-menu-right">
                            @if(Auth::user()->level == 'staff')
                            <a href="{{ url('pengajuan/edit', [$users[0]->id]) }}" class="dropdown-item has-icon"><i
                                    class="far fa-edit"></i> Edit</a>
                            @endif
                            <a href="#" class="dropdown-item has-icon text-danger"
                                data-confirm="Hapus?|Aksi ini tidak bisa diundur, dan data tidak bisa ditarik kembali"
                                data-confirm-yes="window.location = '{{ url('pengajuan/delete', [$users[0]->id]) }}';"><i
                                    class="far fa-trash-alt"></i> Delete</a>

                        </div>
                    </div>
                </div>
            </div>
            <div class="card-body mt-n4">
                <div class="row">
                    <div class="col-8">
                        <div class="section-title">Data diri </div>
                        @foreach ($users as $users)
                        @php($status = $users->status)
                        @php($id = $users->id)
                        @php($status = $users->status)
                        @php($tahun = $users->tahun_lulus)
                        <address class="mt-n1" style="line-height: 180%;">
                            <strong>Persona:</strong><br>
                            Nama : {{ $users->nama_pemohon }}<br>
                            NIK : {{ $users->nik }}<br>
                            Tempat, tgl lahir : {{ $users->tempat_lahir }}, {{ $users->tgl_lahir }}<br>
                            Alamat : {{ $users->alamat }}<br>
                            Daereah : {{ $users->provinsi }}, {{ $users->kabkota }}, {{ $users->kecamatan }},
                            {{ $users->keldes }}
                        </address>
                        <address style="line-height: 180%;">
                            <strong>Data lain:</strong><br>
                            NPWP : {{ $users->npwp }}<br>
                            Nomor reg. asosiasi : {{ $users->no_reg_asosiasi }}<br>
                            Nomor Telepon : {{ $users->no_hp }}<br>
                            Alamat email : {{ $users->email }}
                        </address>
                        <address>
                            <strong>Pendidikan:</strong><br>
                            Pendidikan : {{ $users->jenjang_pendidikan }} <br>
                            Jurusan : {{ $users->jurusan }} <br>
                            Institusi : {{ $users->nama_sekolah }} - {{ $users->tahun_lulus }} <br>
                            Alamat : {{ $users->alamat_sekolah }}
                        </address>
                        @endforeach
                    </div>
                    <div class="col-4">
                        <div class="section-title">Berkas</div>
                        @if($pengajuan->tgl_surat)
                        <ul class="list-unstyled list-unstyled-border">
                            <li class="media">
                                <div class="media-body">
                                    <a href="{{ url('pengajuan/cetak/formulir', [$id]) }}" target="_blank"
                                        class="btn btn-icon btn-sm btn-info float-right"><i
                                            class="fas fa-download"></i></a>
                                    <h6 class="media-title">Formulir permohonan SKA/SKT</h6>
                                    <div class="text-small text-muted">Printed <div class="bullet"></div> <span
                                            class="text-primary">12/12/2020</span></div>
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-body">
                                    <a href="{{ url('pengajuan/cetak/suratpernyataan', [$id]) }}" target="_blank"
                                        class="btn btn-icon btn-sm btn-info float-right"><i
                                            class="fas fa-download"></i></a>
                                    <h6 class="media-title">Surat pernyataan kebenaran data</h6>
                                    <div class="text-small text-muted">Printed <div class="bullet"></div> <span
                                            class="text-primary">12/12/2020</span></div>
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-body">
                                    <a href="{{ url('pengajuan/cetak/suratpermohonan', [$id]) }}" target="_blank"
                                        class="btn btn-icon btn-sm btn-info float-right"><i
                                            class="fas fa-download"></i></a>
                                    <h6 class="media-title">Surat permohonan SKA/SKT</h6>
                                    <div class="text-small text-muted">Printed <div class="bullet"></div> <span
                                            class="text-primary">12/12/2020</span></div>
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-body">
                                    <a href="{{ url('pengajuan/cetak/cv', [$id]) }}" target="_blank"
                                        class="btn btn-icon btn-sm btn-info float-right"><i
                                            class="fas fa-download"></i></a>
                                    <h6 class="media-title">Curriculum vitae</h6>
                                    <div class="text-small text-muted">Printed <div class="bullet"></div> <span
                                            class="text-primary">12/12/2020</span></div>
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-body">
                                    <a href="{{ url('pengajuan/cetak/dokumen', [$id]) }}" target="_blank"
                                        class="btn btn-icon btn-sm btn-info float-right"><i
                                            class="fas fa-download"></i></a>
                                    <h6 class="media-title">Hasil pemeriksaan dokumen</h6>
                                    <div class="text-small text-muted">Printed <div class="bullet"></div> <span
                                            class="text-primary">12/12/2020</span></div>
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-body">
                                    <a href="{{ url('pengajuan/cetak/asosiasi', [$id]) }}" target="_blank"
                                        class="btn btn-icon btn-sm btn-info float-right"><i
                                            class="fas fa-download"></i></a>
                                    <h6 class="media-title">Data asosiasi profesi</h6>
                                    <div class="text-small text-muted">Printed <div class="bullet"></div> <span
                                            class="text-primary">12/12/2020</span></div>
                                </div>
                            </li>
                        </ul>
                        @else
                        <p>Silahkan lengkapi formulir <a href="#dataSurat">Data Surat</a> dibagian paling bawah halaman
                        </p>
                        @endif
                    </div>
                </div>
                <div class="section-title">Permohonan kualifikasi</div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-md">
                        <tr>
                            <th data-width="40">#</th>
                            <th>Kode</th>
                            <th class="text-center">Klasifikasi</th>
                            <th class="text-center">Sub Klasifikasi</th>
                            <th class="text-center">Kode Kualifikasi</th>
                            <th class="text-center">Tingkat</th>
                            <th class="text-center">Ket.</th>
                        </tr>
                        @php($i=1)
                        @php($j=0)
                        @php($k=0)
                        @foreach ($kualifikasi as $key)
                        <tr>
                            <td>{{ $i++ }}.</td>
                            <td>{{ $key->sk_klasifikasi }}</td>
                            <td class="text-center">
                                @switch($key->sk_kl_id)
                                @case(1)
                                <span class="badge badge-primary">Arsitektur</span>
                                @break
                                @case(2)
                                <span class="badge badge-secondary">Mekanikal</span>
                                @break
                                @case(3)
                                <span class="badge badge-success">Elektrikal</span>
                                @break
                                @case(4)
                                <span class="badge badge-danger">Tata Lingkungan</span>
                                @break
                                @case(5)
                                <span class="badge badge-warning">Manajemen</span>
                                @break
                                @case(6)
                                <span class="badge badge-info">Sipil</span>
                                @break
                                @default
                                <span class="badge badge-outline-danger">Kesalahan</span>
                                @endswitch
                            </td>
                            <td class="text-center">{{ $key->sk_nama }}</td>
                            <td class="text-right">{{ $key->sk_kode }}</td>
                            <td class="text-right">
                                {{ Kualifikasi($pengajuan->id_kualifikasi[$k++], $pengajuan->no_reg_asosiasi) }}
                            </td>
                            <td class="text-right">{{ $pengajuan->keterangan[$j++] }}</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                <div class="section-title ">Data pengalaman
                    <a href="#" class="btn btn-sm btn-icon btn-primary float-right" data-toggle="modal"
                        data-target="#pengalaman"><i class="fas fa-plus"></i></a>
                </div>
                <button style="margin-bottom: 10px" class="btn btn-warning delete_all" data-toggle="tooltip"
                    Title="Hapus Semua Data" data-url="{{ url('pengajuan/deleteAll') }}"><i
                        class="fas fa-trash"></i></button>
                <div class="table-responsive">
                    <table class="table table-striped table-hover table-md">
                        <tr>
                            <th width="50px"><input type="checkbox" id="master"></th>
                            <th data-width="40">No</th>
                            <th>Pengalaman</th>
                            <th class="text-center">Lokasi</th>
                            <th class="text-center">Nilai Kontrak</th>
                            <th class="text-right">Tgl mulai</th>
                            <th class="text-right">Tgl selesai</th>
                            <th class="text-right">Jabatan</th>
                        </tr>
                        @php($no = 1)
                        @if(!empty($pekerjaan))
                        @foreach ($pekerjaan as $data)
                        <tr>
                            <td><input type="checkbox" class="sub_chk" data-id="{{$data->id}}"></td>
                            <td>{{ $no++ }}.</td>
                            <td>{{ $data->rp_namaProyek }}</td>
                            <td class="text-center">{{ $data->rp_lokasiProp }}</td>
                            <td class="text-center">Rp. {{ number_format($data->rp_nilai) }}</td>
                            <td class="text-right">{{ $data->rp_tglMulai }}</td>
                            <td class="text-right">{{ $data->rp_tglSelesai }}</td>
                            <td class="text-right">{{ $data->sk_nama }}</td>
                            <td class="text-right">
                                <form action="{{ url('pengajuan/pengalaman/destroy/single', $data->id) }}"
                                    method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-icon btn-sm btn-warning getId" onclick="return confirm('Anda yakin ingin menghapus data ini?')"><i class="fas fa-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </table>
                </div>
                @if($pengajuan->status == 'menunggu')
                <div class="section-title" id="dataSurat">Data surat</div>
                @if(Auth::user()->level == 'admin')
                <address class="mt-n1" style="line-height: 180%;">
                    Nomor Surat : {{ $users->nomor_surat }} <br>
                    Tgl Surat : {{ date('Y-m-d', strtotime($users->tgl_surat)) }}<br>
                    Pemeriksa : {{ $users->pemeriksa }}<br>
                    Status: @if($status=='menunggu' ) Proses @else Selesai @endif
                </address>
                @endif
                @if(Auth::user()->level == 'staff')
                <form action="{{ url('pengajuan/updateStatus', [$id]) }}" method="post">
                    @csrf @method('PATCH')
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="">Nomor Surat</label>
                                <input type="text" name="nomor_surat" value="{{ $users->nomor_surat }}" class="form-control">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="">Tanggal Surat</label>
                                <input type="date" name="tgl_surat" value="@if(!$users->tgl_surat){{ date('Y-m-d') }}@else{{ date('Y-m-d', strtotime($users->tgl_surat)) }}@endif"class="form-control" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="">Pemeriksa Dokumen</label>
                                <input type="text" name="pemeriksa" placeholder="Nama pemeriksa dokumen" value="{{ $users->pemeriksa }}" class="form-control" required>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="">Approval -<code>Aksi ini tidak dapat diundur</code> </label>
                                <select class="form-control form-control-sm" name="status">
                                    <option>Pilih...</option>
                                    <option value="menunggu" @if($status=='menunggu' ) selected @endif>Proses</option>
                                    <option value="acc" @if($status=='acc' ) selected @endif>Selesai</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    @endif
            </div>
            @endif
            @if(Auth::user()->level == 'staff')
                @if($pengajuan->status == 'menunggu')
                <div class="card-footer text-right">
                    <button class="btn btn-secondary" type="reset">Reset</button>
                    <button class="btn btn-primary mr-1" type="submit">Submit</button>
                </div>
                @endif
            @endif
            </form>
        </div>
    </div>
</div>
@endsection

@section('modal-profil')
<div class="modal fade" id="pengalaman" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah pengalaman</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="post" action="{{ url('pengajuan/pengalaman/storemanual') }}">
                @csrf
                <input type="hidden" name="id_pengajuan" value="{{ $id }}">
                <div class="modal-body">
                    <div class="form-group">
                        <label class="d-block">Jenis permohonan</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="tipe_permohonan_1" name="jenis" value="1">
                            <label class="form-check-label" for="inlineradio1">SKA</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="tipe_permohonan_2" name="jenis" value="2">
                            <label class="form-check-label" for="inlineradio2">SKT</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <select class="form-control select2" style="width:100%" name="klasifikasi">
                            <option>Subklasifikasi...</option>
                        </select>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                            <select class="form-control" name="jumlah">
                                <option>Jumlah data...</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                        <div class="form-group col-6">
                            <input type="text" name="tahun" class="form-control" value="{{ $tahun }}" placeholder="Tahun minimal">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <select class="custom-select select2" style="width:83.5%" name="daerah">
                                <option selected>Daerah...</option>
                                @foreach ($daerah as $item)
                                <option value="{{ $item->id_provinsi }}">{{ $item->provinsi }}</option>
                                @endforeach
                            </select>
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button" id="checkPengalaman" >Periksa</button>
                            </div>
                        </div>
                    </div>
                    <div id="emptyAlert" class="text-center"></div>
                    <div class="form-group text-center" id="boxVerify"></div>
                    <div class="form-group">
                        <ul class="list-group" id="listData"></ul>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="submit" id="submitPengalaman" class="btn btn-primary" disabled>Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#master').on('click', function (e) {
            if ($(this).is(':checked', true)) {
                $(".sub_chk").prop('checked', true);
            } else {
                $(".sub_chk").prop('checked', false);
            }
        });
        $('.delete_all').on('click', function (e) {
            var allVals = [];
            $(".sub_chk:checked").each(function () {
                allVals.push($(this).attr('data-id'));
            });
            if (allVals.length <= 0) {
                alert("Belum ada data yang dipilih");
            } else {
                var check = confirm("Hapus data yang dipilih?");
                if (check == true) {
                    var join_selected_values = allVals.join(",");
                    $.ajax({
                        url: $(this).data('url'),
                        type: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: 'ids=' + join_selected_values,
                        success: function (data) {
                            if (data['success']) {
                                $(".sub_chk:checked").each(function () {
                                    $(this).parents("tr").remove();
                                });
                                alert(data['success']);
                            } else if (data['error']) {
                                alert(data['error']);
                            } else {
                                alert('Whoops Something went wrong!!');
                            }
                        },
                        error: function (data) {
                            alert(data.responseText);
                        }
                    });
                    $.each(allVals, function (index, value) {
                        $('table tr').filter("[data-row-id='" + value + "']").remove();
                    });
                }
            }
        });

        $('[data-toggle=confirmation]').confirmation({
            rootSelector: '[data-toggle=confirmation]',
            onConfirm: function (event, element) {
                element.trigger('confirm');
            }
        });

        $(document).on('confirm', function (e) {
            var ele = e.target;
            e.preventDefault();
            $.ajax({
                url: ele.href,
                type: 'DELETE',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    if (data['success']) {
                        $("#" + data['tr']).slideUp("slow");
                        alert(data['success']);
                    } else if (data['error']) {
                        alert(data['error']);
                    } else {
                        alert('Whoops Something went wrong!!');
                    }
                },
                error: function (data) {
                    alert(data.responseText);
                }
            });
            return false;
        });

    });
</script>
@endsection