@extends('layouts.app')

@section('status-data-pengajuan') active
@endsection

@if (Request::is('pengajuan/selesai'))
  @php($text='selesai')
  @section('pengajuan-selesai') active
  @endsection
@else
  @php($text='proses')
  @section('pengajuan-proses') active
  @endsection
@endif

@section('title-page') Pengajuan
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        @include('layouts.alert')
        <div class="card">
            <div class="card-header">
                <h4>Data pengajuan {{ $text }}</h4>
                <div class="card-header-form">
                    <form action="/pengajuan/cari" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" name="cari" value="{{ old('cari') }}" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th width="10">#</th>
                            <th>Pemohon</th>
                            <th>Sertifikat</th>
                            @if (Request::is('pengajuan/selesai'))
                            <th>Tanggal aproval</th>
                            @else
                            <th>Tanggal input</th>
                            @endif
                            <th>Status</th>
                            <th>Aksi</th>
                        </tr>
                        @if(!empty($pengajuan))
                        @php($no = 1)
                        @foreach ($pengajuan as $data)
                        <tr>
                            <td>{{ $no++ }}.</td>
                            <td><span class="text-uppercase">{{ $data->nama_pemohon }}</span>
                                <br><small>{{ $data->nik }}</small></td>
                            <td><span class="text-uppercase">
                                {{ $data->tipe_permohonan }} - {{ asosiasi($data->no_reg_asosiasi) }}
                            </td>
                            @if (Request::is('pengajuan/selesai'))
                            <td>
                                {{ tanggal_indonesia($data->tgl_surat) }}
                            </td>
                            @else
                            <td>
                              {{ tanggal_indonesia($data->created_at) }}
                            </td>
                            @endif
                            <td>
                                <div class="badge badge-{{ $data->status == 'acc' ? 'success' : 'warning' }}">{{ $data->status == 'acc' ? 'Completed' : 'Waiting' }}</div>
                            </td>
                            <td>
                                @if (Request::is('pengajuan/proses'))
                                <a href="{{ url('pengajuan/pengalaman', [$data->id]) }}" class="btn btn-sm btn-icon btn-primary" data-toggle="tooltip" data-placement="top" title="Tambah pengalaman kerja"><i class="far fa-edit"></i></a>
                                @endif
                                <a href="{{ url('pengajuan/detail', [$data->id]) }}" class="btn  btn-sm btn-secondary">Detail</a>
                            </td>
                        </tr>
                        @endforeach
                        @endif
                    </table>
                </div>
            </div>
            <div class="card-footer text-right">
                <nav class="d-inline-block">
                    @if(!empty($pengajuan))
                        {{ $pengajuan->links() }}
                    @endif
                </nav>
            </div>
        </div>
    </div>
</div>

@endsection
