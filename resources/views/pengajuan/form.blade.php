@extends('layouts.app')
@section('status-form-pengajuan') active
@endsection
@section('title-page') Pengajuan
@endsection
@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <form action="{{ url('pengajuan/store') }}" method="post">
                @csrf
                <div class="card-header">
                    <h4>Formulir permohonan SKT/SKA</h4>
                    <div class="card-header-action">
                        <div class="btn-group">
                            <a href="javascript:void(0)" class="btn">Pengajuan Baru</a>
                            <a href="{{ url('pengajuan/form-ktp') }}" class="btn btn-primary">Nomor KTP</a>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-n4">
                    <div class="section-title">Data Diri</div>
                    <div class="form-group">
                        <label>Nomor KTP</label>
                        <input type="text" name="nik" class="form-control form-control-sm" placeholder="Nomor KTP pemohon" autocomplete="off" onkeypress="return numberFilter(event)" maxlength="16" autofocus required>
                    </div>
                    <div class="form-group">
                        <label>Nama lengkap</label>
                        <input type="text" name="nama_pemohon" class="form-control form-control-sm" placeholder="Nama pemohon" autocomplete="off" required>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Tempat lahir</label>
                                <input type="text" name="tempat_lahir" class="form-control form-control-sm" placeholder="Tempat lahir" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col">
                            <label>Tanggal lahir</label>
                            <div class="input-group">
                                <div class="input-group-prepend"> 
                                    <div class="input-group-text" style="height: 89%; cursor: pointer;" id="pasteDate">
                                        <i class="fas fa-clipboard" id="icon"></i>
                                    </div>
                                </div>
                                <input type="text" id="tgl_lahir" name="tgl_lahir" maxlength="20" placeholder="Tanggal lahir" maxlength="20" class="form-control form-control-sm">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea class="form-control form-control-sm" rows="5" placeholder="Alamat lengkap" autocomplete="off" name="alamat" placeholder="Alamat lengkap" required></textarea>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Provinsi</label>
                                <select class="form-control form-control-sm select2" name="id_provinsi" required>
                                    <option class="pilih_provinsi">Pilih Provinsi</option>
                                    @foreach ($provinsi as $key)
                                    <option value="{{ $key->id_provinsi }}">{{ $key->provinsi }}</option>
                                    @endforeach
                                </select>
                                <label class="mt-2">Kecamatan</label>
                                <select class="form-control form-control-sm select2" name="id_kecamatan" required>
                                    <option>Pilih kecamatan</option>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label id="labelkab">Kabupaten</label>
                                <select class="form-control form-control-sm select2" name="id_kabkota" required>
                                    <option class="pilih">Pilih kabupaten</option>
                                </select>
                                <label class="mt-2">Kelurahan</label>
                                <select class="form-control form-control-sm select2" name="id_keldes" required>
                                    <option>Pilih kelurahan/desa</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Nomor NPWP <small><code>optional</code></small></label>
                        <input type="text" name="npwp" class="form-control form-control-sm" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label class="form-label">Pilih Asosiasi</label>
                        <div class="selectgroup w-100">
                            @if(Auth::user()->level == 'staff')
                            <label class="selectgroup-item">
                                <input type="radio" name="no_reg_asosiasi" value="152" class="selectgroup-input" @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '152') checked
                                @else disabled @endif required>
                                <span class="selectgroup-button">ATAKSI</span>
                            </label>
                            <label class="selectgroup-item">
                                <input type="radio" name="no_reg_asosiasi" value="144" class="selectgroup-input" @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '144') checked
                                @else disabled @endif required>
                                <span class="selectgroup-button">ASDAMKINDO</span>
                            </label>
                            <label class="selectgroup-item">
                                <input type="radio" name="no_reg_asosiasi" value="180" class="selectgroup-input" @if(Auth::user()->level == 'staff' && Auth::user()->kode_staff == '180') checked
                                @else disabled @endif required>
                                <span class="selectgroup-button">PROTEKSI</span>
                            </label>
                            @elseif(Auth::user()->level == 'admin')
                            <label class="selectgroup-item">
                                <input type="radio" name="no_reg_asosiasi" value="152" class="selectgroup-input" required>
                                <span class="selectgroup-button">ATAKSI</span>
                            </label>
                            <label class="selectgroup-item">
                                <input type="radio" name="no_reg_asosiasi" value="144" class="selectgroup-input" required>
                                <span class="selectgroup-button">ASDAMKINDO</span>
                            </label>
                            <label class="selectgroup-item">
                                <input type="radio" name="no_reg_asosiasi" value="180" class="selectgroup-input" required>
                                <span class="selectgroup-button">PROTEKSI</span>
                            </label>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label>Nomor Telepon <small><code>optional</code></small></label>
                                <input type="text" name="no_hp" class="form-control form-control-sm" autocomplete="off">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Alamat email <small><code>optional</code></small></label>
                                <input type="text" name="email" class="form-control form-control-sm" autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="section-title">Data Pendidikan</div>
                    <div class="row">
                        <div class="col-auto">
                            <div class="form-group">
                                <label>Jenjang pendidikan</label>
                                <select class="form-control" name="jenjang_pendidikan" required>
                                    <option value="SMP">SMP/Sederajat</option>
                                    <option value="SMA">SMA/Sederajat</option>
                                    <option value="D1">D1</option>
                                    <option value="D2">D2</option>
                                    <option value="D3">D3</option>
                                    <option value="D4">D4</option>
                                    <option value="S1">S1/Sarjana</option>
                                    <option value="S2">S2/Magister</option>
                                    <option value="S3">S3/Doktor</option>
                                </select>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label>Jurusan</label>
                                <input type="text" name="jurusan" class="form-control" placeholder="Jurusan/bidang" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-9">
                            <div class="form-group">
                                <label>Nama sekolah</label>
                                <input type="text" name="nama_sekolah" class="form-control form-control-sm" placeholder="Nama sekolah/lembaga" autocomplete="off" required>
                            </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group">
                                <label>Tahun lulus</label>
                                <input type="text" name="tahun_lulus" class="form-control form-control-sm" placeholder="Tahun lulus" onkeypress="return numberFilter(event)" maxlength="4" autocomplete="off" required>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <input type="text" name="alamat_sekolah" class="form-control form-control-sm" placeholder="Alamat sekolah/lembaga" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label>Nomor ijazah</label>
                        <input type="text" name="nomor_ijazah" class="form-control form-control-sm" placeholder="Nomor ijazah" autocomplete="off" required>
                    </div>
                    <div class="section-title">Data Kualifikasi</div>
                    <div class="row">
                        <div class="form-group col-6">
                            <label class="d-block">Jenis permohonan</label>
                            <div class="form-check custom-control custom-radio form-check-inline">
                                <input type="radio" id="tipe_permohonan_1" name="tipe_permohonan" value="1" class="custom-control-input" required>
                                <label class="custom-control-label" for="tipe_permohonan_1">SKA (Sertifikat tenaga ahli)</label>
                            </div>
                            <div class="form-check custom-control custom-radio form-check-inline">
                                <input type="radio" id="tipe_permohonan_2" name="tipe_permohonan" value="2" class="custom-control-input" required>
                                <label class="custom-control-label" for="tipe_permohonan_2">SKT (Sertifikat tenaga terampil)</label>
                            </div>
                        </div>
                        <div class="form-group col-6">
                            <label>Klasifikasi</label>
                            <select class="form-control form-control-sm" name="id_klasifikasi" disabled>
                                <option value="pilih_klasifikasi">Pilih klasifikasi</option>
                                @foreach ($klasifikasi as $data)
                                <option value="{{ $data->kl_id }}">{{ $data->kl_nama }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-4">
                            <label for="">Sub klasifikasi 1</label>
                            <select class="form-control select2" name="id_subklasifikasi[]" required>
                                <option value="">Subkualifikasi</option>
                            </select>
                        </div>
                        <div class="form-group col-5">
                            <label>Kualifikasi 1</label>
                            <select class="form-control" name="id_kualifikasi[]" required>
                                <option value="">Kualifikasi</option>
                            </select>
                        </div>
                        <div class="form-group col-2">
                            <label>Keterangan 1</label>
                            <input type="text" name="keterangan[]" class="form-control keterangan" value="Baru">
                        </div>
                        <div class="col-auto text-right">
                            <label>Aksi</label><br>
                            <button type="button" class="btn btn-icon btn-success text-white" id="addElement">
                                <li class="fa fa-plus"></li>
                            </button>
                        </div>
                    </div>
                    <div class="row" id="secondElement"></div>
                    <div class="row" id="thirdElement"></div>
                </div>
                <div class="card-footer text-right">
                    <button class="btn btn-secondary" type="reset">Reset</button>
                    <button type="submit" class="btn btn-primary ml-2">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection