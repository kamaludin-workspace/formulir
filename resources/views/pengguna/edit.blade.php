@extends('layouts.app')

@section('status-data-pengguna') active
@endsection

@section('title-page') Form Manajemen User
@endsection

@section('content')

<section class="section">

    <div class="section-body">

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Form Data user</h4>
                    </div>
                    <form action="/pengguna/edit/{{$user->id}}/save" id="form1" method="POST">
                        @csrf {{ method_field('patch')}}
                        <div class="card-body">
                            <div class="form-group">
                                <label>Nama Lengkap</label>
                                <input type="text" name="name" value="{{ $user->name }}" required
                                    placeholder="Masukan Nama Lengkap" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Password <small>(Default)</small></label>
                                <input type="text" name="password" value="12345678" readonly
                                    placeholder="Masukan Nama Lengkap" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Level</label>
                                <select class="form-control" id="level" required name="level" onchange="tampilkan()">
                                    <option value="">=== Silahkan Pilih Level ===</option>
                                    <option value="admin" <?php if ($user->level == "admin") {
                                        echo "selected";
                                    } ?>>Admin</option>
                                    <option value="pimpinan" <?php if ($user->level == "pimpinan") {
                                        echo "selected";
                                    } ?>>pimpinan</option>
                                    <option value="staff" <?php if ($user->level == "staff") {
                                        echo "selected";
                                    } ?>>staff</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Kode Staff</label>
                                <select class="form-control" name="kode_staff" id="tampil">
                                    <option value="">=== Kode Staff ===</option>
                                    @if ($user->kode_staff == "152")
                                    <option value="152" <?php if ($user->kode_staff == "152") {
                                        echo "selected";
                                    } ?>>Ataksi</option>
                                    @elseif($user->kode_staff == "144")
                                    <option value="144" <?php if ($user->kode_staff == "144") {
                                        echo "selected";
                                    } ?>>Asdamkindo</option>
                                    @elseif($user->kode_staff == "180")
                                    <option value="180" <?php if ($user->kode_staff == "180") {
                                        echo "selected";
                                    } ?>>Proteksi</option>
                                    @endif
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Email</label>
                                <input type="email" value="{{$user->email}}" name="email" required
                                    placeholder="Masukan Email" class="form-control">
                            </div>
                            <div class="buttons">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="/pengguna" class="btn btn-danger">Kembali</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

@endsection
