@extends('layouts.app')

@section('status-data-pengguna') active
@endsection

@section('title-page') Manajemen User
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>Data User</h4>
                <div class="card-header-action">
                  <div class="button">
                    <a href="/pengguna/tambah" class="btn btn-icon btn-primary"><i class="fa fa-plus"></i>
                        Tambah Pengguna</a>
                </div>
                </div>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <tr>
                            <th>#</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Tipe akun</th>
                            <th>Aksi</th>
                        </tr>
                        @php($no = 1)
                        @foreach ($user as $t)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{$t->name}}</td>
                            <td>{{$t->email}}</td>
                            @if ($t->level == 'admin')
                            <td>Administrator</td>
                            @elseif($t->level == 'pimpinan')
                            <td>Pimpinan</td>
                            @elseif($t->level == 'staff')
                            <td>Staff {{ asosiasi($t->kode_staff) }}</td>
                            @endif

                            @if (Auth::user()->level == "admin")

                            <td class="datatable-ct" align="center">

                                <form method="POST" action="/pengguna/delete/{{ $t->id }}">
                                    {{csrf_field()}} {{method_field('DELETE')}}

                                    <a href="/pengguna/edit/{{ $t->id }}" class="btn btn-primary" data-toggle="tooltip"
                                        data-placement="top" title="Kelola User"><i class="far fa-edit"></i></a>

                                    <button type="submit" onclick="return confirm('Yakin Ingin Menghapus Data Ini ?')"
                                        class="btn btn-danger" data-toggle="tooltip" data-placement="top"
                                        title="Hapus User"><i class="fas fa-trash"></i></button>
                                </form>
                            </td>

                            @endif



                        </tr>
                        @endforeach

                    </table>

                </div>
            </div>
            <div class="card-footer text-right">
                <nav class="d-inline-block">
                    {{ $user->links() }}
                </nav>
            </div>
        </div>
    </div>
</div>

@endsection
