@extends('layouts.app')

@section('dropdown-kualifikasi') active
@endsection

@section('status-skt') active
@endsection

@section('title-page') Kualifikasi SKT
@endsection

@section('content')

<section class="section">

    <div class="section-body">

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Form Data SKT</h4>
                    </div>
                    <form action="/skt/tambah/save" id="form2" method="POST">
                        @csrf
                        <div class="card-body">
                            <input hidden type="text" value="SKT" name="sk_jenis" required
                                placeholder="Masukan Nama SubKlasifikasi." class="form-control">
                            <div class="form-group">
                                <label>Jurusan</label>
                                <select class="form-control" required name="sk_kl_id" id="jurusan"
                                    onchange="tampilSkt()">
                                    <option value="">=== Silahkan Pilih Jurusan ===</option>
                                    @foreach ($klasifikasi as $t)
                                    <option value="{{$t->kl_id}}">{{$t->kl_nama}}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div hidden class="form-group">
                                <label>Klasifikasi</label>
                                <select class="form-control" name="sk_klasifikasi" id="klasifikasi">
                                    <option value="">=== Kode Klasifikasi ===</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Masukan Nama</label>
                                <input type="text" name="sk_nama" required placeholder="Masukan Nama SubKlasifikasi."
                                    class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Masukan Kode</label>
                                <input type="text" name="sk_kode" required placeholder="Masukan Kode SubKlasifikasi."
                                    class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Sub Klasifikasi</label>
                                <select class="form-control" required name="sk_subKlasifikasi">
                                    <option value="">=== Silahkan Pilih Sub Klaifikasi ===</option>
                                    <option value="Kelas I">Kelas I</option>
                                    <option value="Kelas II">Kelas II</option>
                                    <option value="Kelas III">Kelas III</option>
                                </select>
                            </div>
                            <div class="buttons">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="/skt" class="btn btn-danger">Kembali</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

@endsection
