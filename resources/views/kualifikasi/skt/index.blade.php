@extends('layouts.app')

@section('dropdown-kualifikasi') active
@endsection

@section('status-skt') active
@endsection

@section('title-page') Kualifikasi SKT
@endsection

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">

                <div class="button">
                    <a href="/skt/tambah" class="btn btn-icon btn-primary"><i class="fa fa-plus"></i>
                        Tambah Data</a>
                </div>
            </div>
            <div class="card-header">
                <h4>Data Kualifikasi SKT</h4>
                <div class="card-header-form">
                    <form action="/skt/cari" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" name="cari" value="{{ old('cari') }}" placeholder="Search">
                            <div class="input-group-btn">
                                <button class="btn btn-primary"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-striped">

                        <tr>
                            
                            <th>No</th>
                            <th>Jurusan</th>
                            <th>Klasifikasi</th>
                            <th>Nama</th>
                            <th>Kode</th>
                            <th>Sub Klasifikasi</th>
                            <th>Jenis</th>
                            
                        </tr>
                        <?php $no = $skt->firstItem() ; ?>
                        @foreach ($skt as $t)
                        
                        <tr>
                            
                            <td>{{$no++}}</td>
                            <td>{{$t->kl_nama}}</td>
                            <td>{{$t->sk_klasifikasi}}</td>
                            <td>{{$t->sk_nama}}</td>
                            <td>{{$t->sk_kode}}</td>
                            <td>{{$t->sk_subKlasifikasi}}</td>
                            
                            
                            @if (Auth::user()->level == "admin")
                            
                            <td class="datatable-ct" align="center">
                                <a href="/skt/edit/{{ $t->sk_id }}" class="btn btn-info my-3">Edit</a>
                                <form method="POST" onclick="return confirm('Yakin Ingin Menghapus Data Ini ?')" action="/skt/delete/{{ $t->sk_id }}">
                                    {{csrf_field()}} {{method_field('DELETE')}}

                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                </form>
                            </td>
                                
                            @endif
                            
                                
                            
                        </tr>
                        @endforeach

                    </table>

                </div>
            </div>
            <div class="card-footer text-right">
                <nav class="d-inline-block">
                    {{ $skt->links() }}
                </nav>
            </div>
        </div>
    </div>
</div>

@endsection
