@extends('layouts.app')

@section('dropdown-kualifikasi') active
@endsection

@section('status-skt') active
@endsection

@section('title-page') Kualifikasi SKT
@endsection

@section('content')

<section class="section">

    <div class="section-body">

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Form Data SKT</h4>
                    </div>
                    <form action="/skt/edit/{{$skt->sk_id}}/save" method="POST" id="form2">
                        @csrf {{ method_field('patch')}}
                        <input hidden type="text" value="SKT" name="sk_jenis" required
                            placeholder="Masukan Nama SubKlasifikasi." class="form-control">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Jurusan</label>
                                <select class="form-control" required name="sk_kl_id" id="jurusan"
                                    onchange="tampilSkt()">
                                    <option value="">=== Silahkan Pilih Jurusan ===</option>
                                    @foreach ($klasifikasi as $t)
                                    <option value="{{$t->kl_id}}" <?php if ($t->kl_id == $skt->sk_kl_id) {
                                            echo "selected";
                                        } ?>>{{$t->kl_nama}}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div hidden class="form-group">
                                <label>Klasifikasi</label>
                                <select class="form-control" name="sk_klasifikasi" id="klasifikasi">
                                    <option value="">=== Kode Klasifikasi ===</option>

                                    @if ($skt->sk_klasifikasi == "AA")
                                    <option value="AA" <?php if ($skt->sk_klasifikasi == "AA") {
                                        echo "selected";
                                    } ?>>AA</option>
                                    @elseif($skt->sk_klasifikasi == "AM")
                                    <option value="AM" <?php if ($skt->sk_klasifikasi == "AM") {
                                        echo "selected";
                                    } ?>>AM</option>
                                    @elseif($skt->sk_klasifikasi == "AE")
                                    <option value="AE" <?php if ($skt->sk_klasifikasi == "AE") {
                                        echo "selected";
                                    } ?>>AE</option>
                                    @elseif($skt->sk_klasifikasi == "AT")
                                    <option value="AT" <?php if ($skt->sk_klasifikasi == "AT") {
                                        echo "selected";
                                    } ?>>AT</option>
                                    @elseif($skt->sk_klasifikasi == "AL")
                                    <option value="AL" <?php if ($skt->sk_klasifikasi == "AL") {
                                        echo "selected";
                                    } ?>>AL</option>
                                    @elseif($skt->sk_klasifikasi == "AS")
                                    <option value="AS" <?php if ($skt->sk_klasifikasi == "AS") {
                                        echo "selected";
                                    } ?>>AS</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Masukan Nama</label>
                                <input type="text" value="{{$skt->sk_nama}}" name="sk_nama" required
                                    placeholder="Masukan Nama SubKlasifikasi." class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Masukan Kode</label>
                                <input type="text" name="sk_kode" value="{{$skt->sk_kode}}" required
                                    placeholder="Masukan Kode SubKlasifikasi." class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Sub Klasifikasi</label>
                                <select class="form-control" required name="sk_subKlasifikasi">
                                    <option value="">=== Silahkan Pilih Sub Klaifikasi ===</option>
                                    <option value="Kelas I" <?php if ($skt->sk_subKlasifikasi == "Kelas I") {
                                        echo "selected";
                                    } ?>>Kelas I</option>
                                    <option value="Kelas II" <?php if ($skt->sk_subKlasifikasi == "Kelas II") {
                                        echo "selected";
                                    } ?>>Kelas II</option>
                                    <option value="Kelas III" <?php if ($skt->sk_subKlasifikasi == "Kelas III") {
                                        echo "selected";
                                    } ?>>Kelas III</option>
                                </select>
                            </div>
                            <div class="buttons">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="/skt" class="btn btn-danger">Kembali</a>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

@endsection
