@extends('layouts.app')

@section('dropdown-kualifikasi') active
@endsection

@section('status-ska') active
@endsection

@section('title-page') Kualifikasi SKA
@endsection

@section('content')

<section class="section">

    <div class="section-body">

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Form Data SKA</h4>
                    </div>
                    <form action="/ska/edit/{{$ska->sk_id}}/save" method="POST">
                        @csrf {{ method_field('patch')}}
                        <input hidden type="text" value="SKA" name="sk_jenis" required
                            placeholder="Masukan Nama SubKlasifikasi." class="form-control">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Jurusan</label>
                                <select class="form-control" required name="sk_kl_id" id="jurusan"
                                    onchange="tampilska()">
                                    <option value="">=== Silahkan Pilih Jurusan ===</option>
                                    @foreach ($klasifikasi as $t)
                                    <option value="{{$t->kl_id}}" <?php if ($t->kl_id == $ska->sk_kl_id) {
                                            echo "selected";
                                        } ?>>{{$t->kl_nama}}</option>
                                    @endforeach

                                </select>
                            </div>
                            <div hidden class="form-group">
                                <label>Klasifikasi</label>
                                <select class="form-control" name="sk_klasifikasi" id="klasifikasi">
                                    <option value="">=== Kode Klasifikasi ===</option>

                                    @if ($ska->sk_klasifikasi == "AA")
                                    <option value="AA" <?php if ($ska->sk_klasifikasi == "AA") {
                                        echo "selected";
                                    } ?>>AA</option>
                                    @elseif($ska->sk_klasifikasi == "AM")
                                    <option value="AM" <?php if ($ska->sk_klasifikasi == "AM") {
                                        echo "selected";
                                    } ?>>AM</option>
                                    @elseif($ska->sk_klasifikasi == "AE")
                                    <option value="AE" <?php if ($ska->sk_klasifikasi == "AE") {
                                        echo "selected";
                                    } ?>>AE</option>
                                    @elseif($ska->sk_klasifikasi == "AT")
                                    <option value="AT" <?php if ($ska->sk_klasifikasi == "AT") {
                                        echo "selected";
                                    } ?>>AT</option>
                                    @elseif($ska->sk_klasifikasi == "AL")
                                    <option value="AL" <?php if ($ska->sk_klasifikasi == "AL") {
                                        echo "selected";
                                    } ?>>AL</option>
                                    @elseif($ska->sk_klasifikasi == "AS")
                                    <option value="AS" <?php if ($ska->sk_klasifikasi == "AS") {
                                        echo "selected";
                                    } ?>>AS</option>
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Masukan Nama</label>
                                <input type="text" value="{{$ska->sk_nama}}" name="sk_nama" required
                                    placeholder="Masukan Nama SubKlasifikasi." class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Masukan Kode</label>
                                <input type="text" value="{{$ska->sk_kode}}" name="sk_kode" required
                                    placeholder="Masukan Kode SubKlasifikasi." class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Sub Klasifikasi</label>
                                <select class="form-control" required name="sk_subKlasifikasi">
                                    <option value="">=== Silahkan Pilih Sub Klaifikasi ===</option>
                                    <option value="Utama" <?php if ($ska->sk_subKlasifikasi == "Utama") {
                                        echo "selected";
                                    } ?>>Utama</option>
                                    <option value="Madya" <?php if ($ska->sk_subKlasifikasi == "Madya") {
                                        echo "selected";
                                    } ?>>Madya</option>
                                    <option value="Muda" <?php if ($ska->sk_subKlasifikasi == "Muda") {
                                        echo "selected";
                                    } ?>>Muda</option>
                                </select>
                            </div>
                            <div class="buttons">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="/ska" class="btn btn-danger">Kembali</a>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

@endsection
