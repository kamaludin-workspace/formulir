@extends('layouts.app')

@section('dropdown-kualifikasi') active
@endsection

@section('status-ska') active
@endsection

@section('title-page') Kualifikasi SKA
@endsection

@section('content')

<section class="section">

    <div class="section-body">

        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Form Data SKA</h4>
                    </div>
                    <form action="/ska/tambah/save" id="form2" method="POST">
                        @csrf
                        <input hidden type="text" value="SKA" name="sk_jenis" required
                                placeholder="Masukan Nama SubKlasifikasi." class="form-control">
                        <div class="card-body">
                            <div class="form-group">
                                <label>Jurusan</label>
                                <select class="form-control" required name="sk_kl_id" id="jurusan" onchange="tampilSkt()">
                                    <option value="">=== Silahkan Pilih Jurusan ===</option>
                                    @foreach ($klasifikasi as $t)
                                        <option value="{{$t->kl_id}}">{{$t->kl_nama}}</option>
                                    @endforeach
                                    
                                </select>
                            </div>
                            <div hidden class="form-group">
                                <label>Klasifikasi</label>
                                <select class="form-control" name="sk_klasifikasi" id="klasifikasi">
                                    <option value="">=== Kode Klasifikasi ===</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Masukan Nama</label>
                                <input type="text" name="sk_nama" required placeholder="Masukan Nama SubKlasifikasi."
                                    class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Masukan Kode</label>
                                <input type="text" name="sk_kode" required placeholder="Masukan Kode SubKlasifikasi."
                                    class="form-control">
                            </div>
                            {{-- <div class="form-group">
                                <label>Kode</label>
                                <select class="form-control" required name="sk_kode">
                                    <option value="">=== Silahkan Pilih Kode ===</option>
                                    <option value="TA003">TA003</option>
                                    <option value="TA004">TA004</option>
                                    <option value="TA005">TA005</option>
                                    <option value="TA006">TA006</option>
                                    <option value="TA007">TA007</option>
                                    <option value="TA008">TA008</option>
                                    <option value="TA009">TA009</option>
                                    <option value="TA010">TA010</option>
                                    <option value="TA011">TA011</option>
                                    <option value="TA012">TA012</option>
                                    <option value="TA013">TA013</option>
                                    <option value="TA014">TA014</option>
                                    <option value="TA015">TA015</option>
                                    <option value="TA016">TA016</option>
                                    <option value="TA017">TA017</option>
                                    <option value="TA018">TA018</option>
                                    <option value="TA019">TA019</option>
                                    <option value="TA020">TA020</option>
                                    <option value="TA021">TA021</option>
                                    <option value="TA022">TA022</option>
                                    <option value="TA023">TA023</option>
                                    <option value="TA024">TA024</option>
                                    <option value="TA025">TA025</option>
                                    <option value="TA026">TA026</option>
                                    <option value="TA027">TA027</option>
                                    <option value="TA028">TA028</option>
                                    <option value="TA029">TA029</option>
                                    <option value="TA030">TA030</option>
                                    <option value="TA031">TA031</option>
                                    <option value="TA032">TA032</option>
                                    <option value="TA033">TA033</option>
                                    <option value="TA034">TA034</option>
                                </select>
                            </div> --}}
                            <div class="form-group">
                                <label>Sub Klasifikasi</label>
                                <select class="form-control" required name="sk_subKlasifikasi">
                                    <option value="">=== Silahkan Pilih Sub Klaifikasi ===</option>
                                    <option value="Utama">Utama</option>
                                    <option value="Madya">Madya</option>
                                    <option value="Muda">Muda</option>
                                </select>
                            </div>
                            <div class="buttons">
                                <button type="submit" class="btn btn-primary">Save</button>
                                <a href="/ska" class="btn btn-danger">Kembali</a>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
</div>

@endsection
