<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pekerjaan extends Model
{
    protected $fillable = ['id_pengajuan', 'rp_subKlas', 'rp_namaProyek', 'rp_lokasiProp', 'rp_kodeLok', 'rp_nilai', 'rp_tglMulai', 'rp_tglSelesai', 'rp_tahun'];
}
