<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Klasifikasi extends Model
{
    //
    protected $table = 'klasifikasi'; 
    protected $primaryKey = 'kl_id'; 

    protected $fillable = ['kl_nama']; 
    public $timestamps = false;
}
