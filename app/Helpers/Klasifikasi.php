<?php

use App\Klasifikasi;

function Klasifikasi($id)
{
    $klasifikasi =  Klasifikasi::findOrFail($id);
    return $klasifikasi->kl_nama;
}
