<?php

function asosiasi($kode)
{
	if($kode == '152') {
		$nama_asosiasi = 'ATAKSI';
	} elseif($kode == '144') {
		$nama_asosiasi = 'ASDAMKINDO';
	} elseif($kode == '180') {
		$nama_asosiasi = 'PROTEKSI';
	} else {
		$nama_asosiasi = 'Kode Asosiasi tidak dikenal';
	}

	return $nama_asosiasi;
}
