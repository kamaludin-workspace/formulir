<?php
// get tingkat by id
function kualifikasi($kode, $asosiasi)
{
	switch ($asosiasi) {
        case '152': # ATAKSI
            $url = 'http://localhost:8888/ataksi/curl-post/ataksi/tingkatId.php?id='.$kode;
            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $output = curl_exec($ch); 
            curl_close($ch);
            return $output;
        break;

        case '144': # ASDAMKINDO
            $url = 'http://localhost:8888/ataksi/curl-post/asdamkindo/tingkatId.php?id='.$kode;
            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $output = curl_exec($ch); 
            curl_close($ch);
            return $output;
        break;

        case '180': # PROTEKSI
            $url = 'http://localhost:8888/ataksi/curl-post/proteksi/tingkatId.php?id='.$kode;
            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $output = curl_exec($ch); 
            curl_close($ch);
            return $output;
        break;

        default:
            return 'Asosiasi tidak dikenali';
            break;
    }
}

// Get kualifikasi by jenis and asosiasi...
function AllKualifikasi($tipe, $asosiasi)
{
    $tipePermohonan = '';
    if ($tipe == 'ska') {
        $tipePermohonan = 1;
    } else {
        $tipePermohonan = 2;
    }

    switch ($asosiasi) {
        case '152': # ATAKSI
            $url = 'http://localhost:8888/ataksi/keuangan-asdamkindo/public/API/tingkat/'.$tipePermohonan;
            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $output = curl_exec($ch); 
            curl_close($ch);
            return $output;
        break;

        case '144': # ASDAMKINDO
            $url = 'http://localhost:8888/ataksi/keuangan-asdamkindo/public/API/tingkat/'.$tipePermohonan;
            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $output = curl_exec($ch); 
            curl_close($ch);
            return $output;
        break;

        case '180': # PROTEKSI
            $url = 'http://localhost:8888/ataksi/keuangan-asdamkindo/public/API/tingkat/'.$tipePermohonan;
            $ch = curl_init(); 
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
            $output = curl_exec($ch); 
            curl_close($ch);
            return $output;
        break;

        default:
            return 'Asosiasi tidak dikenali';
            break;
    }
}
