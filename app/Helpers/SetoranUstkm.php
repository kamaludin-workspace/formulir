<?php
// get harga ustkm
function hargaUstkm($tingkat, $asosiasi)
{
  switch ($asosiasi) {
    case '152': # ATAKSI
        $url = 'http://localhost:8888/ataksi/curl-post/ataksi/ustkm.php?id='.$tingkat;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return 'Rp. '.number_format($output);
    break;
    case '144': # ASDAMKINDO
        $url = 'http://localhost:8888/ataksi/curl-post/asdamkindo/ustkm.php?id='.$tingkat;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return 'Rp. '.number_format($output);
    break;
    case '180': # PROTEKSI
        $url = 'http://localhost:8888/ataksi/curl-post/proteksi/ustkm.php?id='.$tingkat;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return 'Rp. '.number_format($output);
    break;

    default:
        return 'Asosiasi tidak dikenali';
    break;
  }
}
