<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubKlasifikasi extends Model
{
    protected $table = 'sub_klasifikasi';
    protected $primaryKey = 'sk_id';

    protected $fillable = ['sk_kl_id','sk_klasifikasi','sk_nama','sk_kode','sk_subKlasifikasi','sk_jenis'];
    public $timestamps = false;
}
