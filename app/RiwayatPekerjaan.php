<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RiwayatPekerjaan extends Model
{
    //
    protected $table = 'riwayat_pekerjaan'; 
    protected $primaryKey = 'rp_id'; 

    protected $fillable = ['rp_kl_id','rp_subKlas','rp_namaProyek','rp_kodeLok','rp_nilai','rp_tglMulai','rp_tglSelesai','rp_tahun']; 
    public $timestamps = false;
}
