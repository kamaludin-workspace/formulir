<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengajuan extends Model
{
    protected $fillable = [
      'tipe_permohonan',
      'nama_pemohon', 'tempat_lahir', 'tgl_lahir', 'alamat',
      'id_provinsi', 'id_kabkota', 'id_kecamatan', 'id_keldes',
      'wilayah_kalimat', 'nik', 'npwp',
      'no_hp', 'email',
      'no_reg_asosiasi',
      'jenis_permohonan', 'cv',
      'jenjang_pendidikan', 'jurusan', 'nama_sekolah', 'alamat_sekolah',
      'tahun_lulus', 'nomor_ijazah',
      'id_klasifikasi', 'id_subklasifikasi', 'id_kualifikasi', 'keterangan', 'status',
      'kode_asosiasi', 'pemeriksa', 'nomor_surat', 'tgl_surat'
    ];

    protected $casts = [
       'id_subklasifikasi' => 'array',
       'id_kualifikasi' => 'array',
       'keterangan' => 'array'
   ];
}
