<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisSertifikat extends Model
{
    //
    protected $table = 'jenis_sertifikats'; 
    protected $primaryKey = 'js_id'; 

    protected $fillable = ['nama']; 
}
