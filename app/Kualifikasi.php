<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kualifikasi extends Model
{
    //
    protected $table = 'kualifikasis'; 
    protected $primaryKey = 'kul_id'; 

    protected $fillable = ['kul_js_id','kul_nama']; 
}
