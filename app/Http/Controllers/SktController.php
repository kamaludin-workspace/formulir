<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kualifikasi;
use App\SubKlasifikasi;
use App\Klasifikasi;

class SktController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
        $skt = SubKlasifikasi::join('klasifikasi','kl_id','sk_kl_id')->where('sk_jenis','=','SKT')->paginate(10);

        return view('kualifikasi.skt.index', compact('skt'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $klasifikasi = Klasifikasi::all();
        return view('kualifikasi.skt.create', compact('klasifikasi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $skt = new SubKlasifikasi;
        $skt->sk_kl_id = $request['sk_kl_id'];
        $skt->sk_klasifikasi = $request['sk_klasifikasi'];
        $skt->sk_nama = $request['sk_nama'];
        $skt->sk_kode = $request['sk_kode'];
        $skt->sk_subKlasifikasi = $request['sk_subKlasifikasi'];
        $skt->sk_jenis = $request['sk_jenis'];
        $skt->save();
        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Klasifikasi  $klasifikasi
     * @return \Illuminate\Http\Response
     */
    public function show(Klasifikasi $klasifikasi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Klasifikasi  $klasifikasi
     * @return \Illuminate\Http\Response
     */
    public function edit( $sk_id)
    {
        //
        $skt = SubKlasifikasi::find($sk_id);
        $klasifikasi = Klasifikasi::all();

        return view('kualifikasi.skt.edit', compact('skt','klasifikasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Klasifikasi  $klasifikasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $sk_id)
    {
        //
        $skt = SubKlasifikasi::find($sk_id);
        $skt->sk_kl_id = $request['sk_kl_id'];
        $skt->sk_klasifikasi = $request['sk_klasifikasi'];
        $skt->sk_nama = $request['sk_nama'];
        $skt->sk_kode = $request['sk_kode'];
        $skt->sk_subKlasifikasi = $request['sk_subKlasifikasi'];
        $skt->sk_jenis = $request['sk_jenis'];
        $skt->save();
        return redirect('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Klasifikasi  $klasifikasi
     * @return \Illuminate\Http\Response
     */
    public function destroy( $sk_id)
    {
        //
        $skt = SubKlasifikasi::find($sk_id);
        $skt->delete();
        return redirect('/home');
    }

    public function cari(Request $request)
	{
		// menangkap data pencarian
        $cari = $request->cari;

    	// mengambil data dari table pegawai sesuai pencarian data
        $skt = SubKlasifikasi::join('klasifikasi','kl_id','sk_kl_id')->where('sk_jenis','=','SKT')->where('sk_klasifikasi','like',"%".$cari."%")->orWhere('sk_nama','like',"%".$cari."%")->orWhere('sk_kode','like',"%".$cari."%")->orWhere('sk_subKlasifikasi','like',"%".$cari."%")->orWhere('sk_jenis','like',"%".$cari."%")->paginate(10);
        $skt->appends($request->only('cari'));
    	// mengirim data pegawai ke view index
		return view('kualifikasi.skt.index',compact('skt'));

	}
}
