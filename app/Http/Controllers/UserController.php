<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rules\MatchOldPassword;
use App\User;
use Hash;
use Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = User::where('users.id','!=', Auth::user()->id)->paginate(10);
        return view('pengguna.index', compact('user'));
    }

    public function create()
    {
        $user = User::all();
        return view('pengguna.create', compact('user'));
    }

    public function store(Request $request)
    {
        $user = new User;
        $user->level = $request['level'];
        $user->kode_staff = $request['kode_staff'];
        $user->name = $request['name'];
        $user->password = Hash::make($request['password']);
        $user->email = $request['email'];
        $user->save();

        return redirect('/pengguna')->with('sukses','Data Berhasil Di Tambahkan');
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('pengguna.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->level = $request['level'];
        $user->kode_staff = $request['kode_staff'];
        $user->name = $request['name'];
        $user->password = Hash::make($request['password']);
        $user->email = $request['email'];
        $user->save();

        return redirect('/pengguna')->with('update','Data Barhasil Di Update');
    }

    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('/pengguna')->with('delete','Data Berhasil Di Hapus');
    }

    public function ChangePassword(Request $request)
    {
        $request->validate([
            'current_password' => ['required', new MatchOldPassword],
            'new_password' => ['required'],
            'new_confirm_password' => ['same:new_password'],
        ]);

        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_password)]);

        return redirect('security')->with('success','Password berhasil diubah');
    }
}
