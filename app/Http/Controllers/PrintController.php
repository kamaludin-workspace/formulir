<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\{Pengajuan, SubKlasifikasi};
use PDF;

class PrintController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function formulir($id)
    {
      $pengajuan = Pengajuan::findOrFail($id);
      $daerah = DB::table('pengajuans')
        ->join('provinsi', 'pengajuans.id_provinsi', '=', 'provinsi.id_provinsi')
        ->join('kabkota', 'pengajuans.id_kabkota', '=', 'kabkota.id_kabkota')
        ->join('kecamatan', 'pengajuans.id_kecamatan', '=', 'kecamatan.id_kecamatan')
        ->join('keldes', 'pengajuans.id_keldes', '=', 'keldes.id_keldes')
        ->select('provinsi.provinsi', 'kabkota.kabkota', 'kecamatan.kecamatan', 'keldes.keldes')
        ->where('id', '=', $id)
        ->get();
      $tahunExplode = explode("-", $pengajuan->tgl_surat);
      switch ($tahunExplode[1]) {
        case '01':
          $bulan = 'JANUARI';
          $tgl_surat = $tahunExplode[2].' '.$bulan.' '.$tahunExplode[0];
          break;
        case '02':
          $bulan = 'FEBRUARI';
          $tgl_surat = $tahunExplode[2].' '.$bulan.' '.$tahunExplode[0];
          break;
        case '03':
          $bulan = 'MARET';
          $tgl_surat = $tahunExplode[2].' '.$bulan.' '.$tahunExplode[0];
          break;
        case '04':
          $bulan = 'APRIL';
          $tgl_surat = $tahunExplode[2].' '.$bulan.' '.$tahunExplode[0];
          break;
        case '05':
          $bulan = 'MEI';
          $tgl_surat = $tahunExplode[2].' '.$bulan.' '.$tahunExplode[0];
          break;
        case '06':
          $bulan = 'JUNI';
          $tgl_surat = $tahunExplode[2].' '.$bulan.' '.$tahunExplode[0];
          break;
        case '07':
          $bulan = 'JULI';
          $tgl_surat = $tahunExplode[2].' '.$bulan.' '.$tahunExplode[0];
          break;
        case '08':
          $bulan = 'AGUSTUS';
          $tgl_surat = $tahunExplode[2].' '.$bulan.' '.$tahunExplode[0];
          break;
        case '09':
          $bulan = 'SEPTEMBER';
          $tgl_surat = $tahunExplode[2].' '.$bulan.' '.$tahunExplode[0];
          break;
        case '10':
          $bulan = 'OKTOBER';
          $tgl_surat = $tahunExplode[2].' '.$bulan.' '.$tahunExplode[0];
          break;
        case '11':
          $bulan = 'NOVEMBER';
          $tgl_surat = $tahunExplode[2].' '.$bulan.' '.$tahunExplode[0];
          break;
        case '12':
          $bulan = 'DESEMBER';
          $tgl_surat = $tahunExplode[2].' '.$bulan.' '.$tahunExplode[0];
          break;
        default:
          return 'Error date, contact your administrator now!';
          break;
      }
    	$formulir = PDF::loadview('pengajuan-cetak.formulir', ['pengajuan' => $pengajuan, 'daerah' => $daerah, 'tgl_surat' => $tgl_surat]);

      
    	return $formulir->stream();
    }

    public function pernyataan($id)
    {
      $pengajuan = Pengajuan::findOrFail($id);
      $daerah = DB::table('pengajuans')
        ->join('provinsi', 'pengajuans.id_provinsi', '=', 'provinsi.id_provinsi')
        ->select('provinsi.provinsi')
        ->where('id', '=', $id)
        ->get();
      $kualifikasi = SubKlasifikasi::whereIn('sk_id', $pengajuan->id_subklasifikasi)->get();
      
    	$pernyataan = PDF::loadview('pengajuan-cetak.surat-pernyataan', ['pengajuan' => $pengajuan, 'kualifikasi' => $kualifikasi, 'daerah' => $daerah]);
    	return $pernyataan->stream();
    }

    public function permohonan($id)
    {
      $pengajuan = Pengajuan::findOrFail($id);
      $kualifikasi = SubKlasifikasi::whereIn('sk_id', $pengajuan->id_subklasifikasi)->get();
      
    	$permohonan = PDF::loadview('pengajuan-cetak.surat-permohonan', ['pengajuan' => $pengajuan, 'kualifikasi' => $kualifikasi]);
    	return $permohonan->stream();
    }

    public function cv($id)
    {
      $pengajuan = Pengajuan::findOrFail($id);
      $daerah = DB::table('pengajuans')
        ->join('provinsi', 'pengajuans.id_provinsi', '=', 'provinsi.id_provinsi')
        ->join('kabkota', 'pengajuans.id_kabkota', '=', 'kabkota.id_kabkota')
        ->join('kecamatan', 'pengajuans.id_kecamatan', '=', 'kecamatan.id_kecamatan')
        ->join('keldes', 'pengajuans.id_keldes', '=', 'keldes.id_keldes')
        ->select('provinsi.provinsi', 'kabkota.kabkota', 'kecamatan.kecamatan', 'keldes.keldes')
        ->where('id', '=', $id)
        ->get();
      $kualifikasi = SubKlasifikasi::whereIn('sk_id', $pengajuan->id_subklasifikasi)->get();
      $pekerjaan = DB::table('pekerjaans')
      ->join('provinsi', 'pekerjaans.rp_kodeLok', '=', 'provinsi.id_provinsi')
      ->join('sub_klasifikasi', 'pekerjaans.rp_subKlas', '=', 'sub_klasifikasi.sk_kode')
      ->where('pekerjaans.id_pengajuan', '=', $id)
      ->distinct()->get();
      $pekerjaan = $pekerjaan->unique('rp_namaProyek');
    	$cv = PDF::loadview('pengajuan-cetak.cv', ['pengajuan' => $pengajuan, 'daerah' => $daerah, 'kualifikasi' => $kualifikasi, 'pekerjaan' => $pekerjaan]);
    	return $cv->stream();
    }

    public function dokumen($id)
    {
      $pengajuan = Pengajuan::findOrFail($id);
      $daerah = DB::table('pengajuans')
        ->join('provinsi', 'pengajuans.id_provinsi', '=', 'provinsi.id_provinsi')
        ->join('kabkota', 'pengajuans.id_kabkota', '=', 'kabkota.id_kabkota')
        ->join('kecamatan', 'pengajuans.id_kecamatan', '=', 'kecamatan.id_kecamatan')
        ->join('keldes', 'pengajuans.id_keldes', '=', 'keldes.id_keldes')
        ->select('provinsi.provinsi', 'kabkota.kabkota', 'kecamatan.kecamatan', 'keldes.keldes')
        ->where('id', '=', $id)
        ->get();
      $kualifikasi = SubKlasifikasi::whereIn('sk_id', $pengajuan->id_subklasifikasi)->get();
      $pekerjaan = DB::table('pekerjaans')
      ->join('provinsi', 'pekerjaans.rp_kodeLok', '=', 'provinsi.id_provinsi')
      ->join('sub_klasifikasi', 'pekerjaans.rp_subKlas', '=', 'sub_klasifikasi.sk_kode')
      ->where('pekerjaans.id_pengajuan', '=', $id)
      ->distinct()->get();
      $pekerjaan = $pekerjaan->unique('rp_namaProyek');
		  $dokumen = PDF::loadview('pengajuan-cetak.dokumen', ['pengajuan' => $pengajuan, 'daerah' => $daerah, 'kualifikasi' => $kualifikasi, 'pekerjaan' => $pekerjaan]);
    	return $dokumen->stream();
    }

    public function asosiasi($id)
    {
      $pengajuan = Pengajuan::findOrFail($id);
      $daerah = DB::table('pengajuans')
        ->join('provinsi', 'pengajuans.id_provinsi', '=', 'provinsi.id_provinsi')
        ->join('kabkota', 'pengajuans.id_kabkota', '=', 'kabkota.id_kabkota')
        ->join('kecamatan', 'pengajuans.id_kecamatan', '=', 'kecamatan.id_kecamatan')
        ->join('keldes', 'pengajuans.id_keldes', '=', 'keldes.id_keldes')
        ->select('provinsi.provinsi', 'kabkota.kabkota', 'kecamatan.kecamatan', 'keldes.keldes')
        ->where('id', '=', $id)
        ->get();
      $kualifikasi = SubKlasifikasi::whereIn('sk_id', $pengajuan->id_subklasifikasi)->get();
      $pekerjaan = DB::table('pekerjaans')
      ->join('provinsi', 'pekerjaans.rp_kodeLok', '=', 'provinsi.id_provinsi')
      ->join('sub_klasifikasi', 'pekerjaans.rp_subKlas', '=', 'sub_klasifikasi.sk_kode')
      ->where('pekerjaans.id_pengajuan', '=', $id)
      ->distinct()->get();
      $pekerjaan = $pekerjaan->unique('rp_namaProyek');

    	$asosiasi = PDF::loadview('pengajuan-cetak.asosiasi', ['pengajuan' => $pengajuan, 'daerah' => $daerah, 'kualifikasi' => $kualifikasi, 'pekerjaan' => $pekerjaan]);
    	$asosiasi->setPaper('A4', 'landscape');
    	return $asosiasi->stream();
    }
}
