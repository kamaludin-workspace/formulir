<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RiwayatPekerjaan;
use App\SubKlasifikasi;
use App\provinsi;

class SipilController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $sipil = RiwayatPekerjaan::join('klasifikasi','kl_id','rp_kl_id')->join('provinsi','id_provinsi','rp_kodeLok')->join('sub_klasifikasi','sk_kode','rp_subKlas')->where('rp_kl_id', 6)->paginate(10);
        return view('pengalaman.sipil.index', compact('sipil'));
    }

    public function create()
    {
        //
        $sub_klas = SubKlasifikasi::where('sk_klasifikasi','AS')->get();
        $provinsi = provinsi::all();
        return view('pengalaman.sipil.create', compact('sub_klas','provinsi'));
    }

    public function edit($rp_id)
    {
        //
        $sub_klas = SubKlasifikasi::where('sk_klasifikasi','AS')->get();
        $sipil = RiwayatPekerjaan::find($rp_id);
        $provinsi = provinsi::all();

        return view('pengalaman.sipil.edit', compact('sipil','provinsi','sub_klas'));
    }

    public function detail($rp_id)
    {
        $sipil = RiwayatPekerjaan::join('klasifikasi','kl_id','rp_kl_id')->join('provinsi','id_provinsi','rp_kodeLok')->join('sub_klasifikasi','sk_kode','rp_subKlas')->where('rp_kl_id', 6)->find($rp_id);

        //menampilkan tanggal format Indonesia
        $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September","Oktober", "November", "Desember");
        $tahun = substr($sipil->rp_tglMulai, 0, 4);
        $tahun2 = substr($sipil->rp_tglSelesai, 0, 4);
        $bulan = substr($sipil->rp_tglMulai, 5, 2);
        $bulanselesai = substr($sipil->rp_tglSelesai, 5, 2);
        $tgl   = substr($sipil->rp_tglMulai, 8, 2);
        $tgl2   = substr($sipil->rp_tglSelesai, 8, 2);
        $result = $tgl . " " . $BulanIndo[(int)$bulan-1]. " ". $tahun;
        $result2 = $tgl2 . " " . $BulanIndo[(int)$bulanselesai-1]. " ". $tahun2;

        return view('pengalaman.sipil.detail', compact('sipil','result','result2'));
    }

    public function cari(Request $request)
	{
		// menangkap data pencarian
        $cari = $request->cari;

    	// mengambil data dari table pegawai sesuai pencarian data
        $sipil = RiwayatPekerjaan::join('klasifikasi','kl_id','rp_kl_id')->join('provinsi','id_provinsi','rp_kodeLok')->join('sub_klasifikasi','sk_kode','rp_subKlas')->where('rp_kl_id', 6)->where('rp_subKlas','like',"%".$cari."%")->orWhere('rp_namaProyek','like',"%".$cari."%")->orWhere('rp_lokasiProp','like',"%".$cari."%")->orWhere('rp_kodeLok','like',"%".$cari."%")->orWhere('sk_subKlasifikasi','like',"%".$cari."%")->orWhere('provinsi','like',"%".$cari."%")->paginate(10);
        $sipil->appends($request->only('cari'));

    	// mengirim data pegawai ke view index
		return view('pengalaman.elektrikal.index', compact('cari','sipil'));

	}
}
