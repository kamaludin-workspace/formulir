<?php

namespace App\Http\Controllers;

use App\Pekerjaan;
use Illuminate\Http\Request;

class PekerjaanController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function create(Request $request)
    {
      for($i = 0; $i < count($request->rp_subKlas) ; $i++) {
          $id_pengajuan = $request->id_pengajuan;
          $rp_subKlas = $request->rp_subKlas[$i];
          $rp_namaProyek = $request->rp_namaProyek[$i];
          $rp_lokasiProp = $request->rp_lokasiProp[$i];
          $rp_kodeLok = $request->rp_kodeLok[$i];
          $rp_nilai = $request->rp_nilai[$i];
          $rp_tglMulai = $request->rp_tglMulai[$i];
          $rp_tglSelesai = $request->rp_tglSelesai[$i];
          $rp_tahun = $request->rp_tahun[$i];

          $pekerjaan = Pekerjaan::create([
              'id_pengajuan' => $id_pengajuan,
              'rp_subKlas' => $rp_subKlas,
              'rp_namaProyek' => $rp_namaProyek,
              'rp_lokasiProp' => $rp_lokasiProp,
              'rp_kodeLok' => $rp_kodeLok,
              'rp_nilai' => $rp_nilai,
              'rp_tglMulai' => $rp_tglMulai,
              'rp_tglSelesai' => $rp_tglSelesai,
              'rp_tahun' => $rp_tahun
          ]);
      }

      if($pekerjaan->wasRecentlyCreated){
          return redirect('pengajuan/detail/'.$request->id_pengajuan)->with('success', 'Pengalaman berhasil ditambah');
      } else {
          return redirect('pengajuan/detail/'.$request->id_pengajuan)->with('delete', 'Data sudah ada, silakan ulang kembali dengan data yang berbeda');
      }
    }
    public function createmanual(Request $request)
    {
      for($i = 0; $i < count($request->rp_subKlas) ; $i++) {
          $id_pengajuan = $request->id_pengajuan;
          $rp_subKlas = $request->rp_subKlas[$i];
          $rp_namaProyek = $request->rp_namaProyek[$i];
          $rp_lokasiProp = $request->rp_lokasiProp[$i];
          $rp_kodeLok = $request->rp_kodeLok[$i];
          $rp_nilai = $request->rp_nilai[$i];
          $rp_tglMulai = $request->rp_tglMulai[$i];
          $rp_tglSelesai = $request->rp_tglSelesai[$i];
          $rp_tahun = $request->rp_tahun[$i];

          $pekerjaan = Pekerjaan::create([
              'id_pengajuan' => $id_pengajuan,
              'rp_subKlas' => $rp_subKlas,
              'rp_namaProyek' => $rp_namaProyek,
              'rp_lokasiProp' => $rp_lokasiProp,
              'rp_kodeLok' => $rp_kodeLok,
              'rp_nilai' => $rp_nilai,
              'rp_tglMulai' => $rp_tglMulai,
              'rp_tglSelesai' => $rp_tglSelesai,
              'rp_tahun' => $rp_tahun
          ]);
      }

      if($pekerjaan->wasRecentlyCreated){
          return redirect('pengajuan/detail/'.$request->id_pengajuan)->with('success', 'Pengalaman berhasil ditambah');
      } else {
          return redirect('pengajuan/detail/'.$request->id_pengajuan)->with('delete', 'Data sudah ada, silakan ulang kembali dengan data yang berbeda');
      }
    }

    public function singleDestroy($id)
    {
      $pekerjaan = Pekerjaan::findOrFail($id);
      $pekerjaan->delete();
      return redirect('pengajuan/detail/'.$pekerjaan->id_pengajuan)->with('delete', 'Data berhasil dihapus');
    }


}
