<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RiwayatPekerjaan;
use App\SubKlasifikasi;
use App\provinsi;

class ManajemenController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $men = RiwayatPekerjaan::join('klasifikasi','kl_id','rp_kl_id')->join('provinsi','id_provinsi','rp_kodeLok')->join('sub_klasifikasi','sk_kode','rp_subKlas')->where('rp_kl_id', 5)->paginate(10);

        return view('pengalaman.manajemen.index', compact('men'));
    }
    public function create()
    {
        //
        $sub_klas = SubKlasifikasi::where('sk_klasifikasi','AL')->get();
        $provinsi = provinsi::all();
        return view('pengalaman.manajemen.create', compact('sub_klas','provinsi'));
    }

    public function edit($rp_id)
    {
        //
        $sub_klas = SubKlasifikasi::where('sk_klasifikasi','AL')->get();
        $men = RiwayatPekerjaan::find($rp_id);
        $provinsi = provinsi::all();

        return view('pengalaman.manajemen.edit', compact('men','provinsi','sub_klas'));
    }

    public function detail($rp_id)
    {
        $men = RiwayatPekerjaan::join('klasifikasi','kl_id','rp_kl_id')->join('provinsi','id_provinsi','rp_kodeLok')->join('sub_klasifikasi','sk_kode','rp_subKlas')->where('rp_kl_id', 5)->find($rp_id);

        //menampilkan tanggal format Indonesia
        $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September","Oktober", "November", "Desember");
        $tahun = substr($men->rp_tglMulai, 0, 4);
        $tahun2 = substr($men->rp_tglSelesai, 0, 4);
        $bulan = substr($men->rp_tglMulai, 5, 2);
        $bulanselesai = substr($men->rp_tglSelesai, 5, 2);
        $tgl   = substr($men->rp_tglMulai, 8, 2);
        $tgl2   = substr($men->rp_tglSelesai, 8, 2);
        $result = $tgl . " " . $BulanIndo[(int)$bulan-1]. " ". $tahun;
        $result2 = $tgl2 . " " . $BulanIndo[(int)$bulanselesai-1]. " ". $tahun2;


        return view('pengalaman.manajemen.detail', compact('men','result','result2'));
    }

    public function cari(Request $request)
	{
		// menangkap data pencarian
        $cari = $request->cari;

    	//mengambil data dari table pegawai sesuai pencarian data
        $men = RiwayatPekerjaan::join('klasifikasi','kl_id','rp_kl_id')->join('provinsi','id_provinsi','rp_kodeLok')->join('sub_klasifikasi','sk_kode','rp_subKlas')->where('rp_kl_id', 5)->where('rp_subKlas','like',"%".$cari."%")->orWhere('rp_namaProyek','like',"%".$cari."%")->orWhere('rp_lokasiProp','like',"%".$cari."%")->orWhere('rp_kodeLok','like',"%".$cari."%")->orWhere('sk_subKlasifikasi','like',"%".$cari."%")->orWhere('provinsi','like',"%".$cari."%")->paginate(10);
        $men->appends($request->only('cari'));

    	// mengirim data pegawai ke view index
		return view('pengalaman.manajemen.index', compact('cari','men'));

	}
}
