<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SubKlasifikasi;

class SubKlasifikasiController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getsubcualification($id, $jenis)
    {
      $subkualifikasi = SubKlasifikasi::where([
        ['sk_kl_id', '=', $id],
        ['sk_jenis', '=', $jenis]
        ])->get();
      $subkualifikasi = $subkualifikasi->unique('sk_kode');
      return $subkualifikasi;
    }

    public function getsubcualificationmanual($jenis)
    {
      $subkualifikasi = SubKlasifikasi::where('sk_jenis', $jenis)->orderByRaw('sk_nama ASC')->pluck('sk_nama', 'sk_kode');
      return json_encode($subkualifikasi);
    }
}
