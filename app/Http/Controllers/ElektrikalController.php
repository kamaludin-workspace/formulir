<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RiwayatPekerjaan;
use App\SubKlasifikasi;
use App\provinsi;

class ElektrikalController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $elka = RiwayatPekerjaan::join('klasifikasi','kl_id','rp_kl_id')->join('provinsi','id_provinsi','rp_kodeLok')->join('sub_klasifikasi','sk_kode','rp_subKlas')->where('rp_kl_id', 3)->paginate(10);
        return view('pengalaman.elektrikal.index', compact('elka'));
    }

    public function create()
    {
        //
        $sub_klas = SubKlasifikasi::where('sk_klasifikasi','AE')->get();
        $provinsi = provinsi::all();
        return view('pengalaman.elektrikal.create', compact('sub_klas','provinsi'));
    }

    public function edit($rp_id)
    {
        //
        $sub_klas = SubKlasifikasi::where('sk_klasifikasi','AE')->get();
        $elka = RiwayatPekerjaan::find($rp_id);
        $provinsi = provinsi::all();

        return view('pengalaman.elektrikal.edit', compact('elka','provinsi','sub_klas'));
    }

    public function detail($rp_id)
    {
        $elka = RiwayatPekerjaan::join('klasifikasi','kl_id','rp_kl_id')->join('provinsi','id_provinsi','rp_kodeLok')->join('sub_klasifikasi','sk_kode','rp_subKlas')->where('rp_kl_id', 3)->find($rp_id);

        //menampilkan tanggal format Indonesia
        $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September","Oktober", "November", "Desember");
        $tahun = substr($elka->rp_tglMulai, 0, 4);
        $tahun2 = substr($elka->rp_tglSelesai, 0, 4);
        $bulan = substr($elka->rp_tglMulai, 5, 2);
        $bulanselesai = substr($elka->rp_tglSelesai, 5, 2);
        $tgl   = substr($elka->rp_tglMulai, 8, 2);
        $tgl2   = substr($elka->rp_tglSelesai, 8, 2);
        $result = $tgl . " " . $BulanIndo[(int)$bulan-1]. " ". $tahun;
        $result2 = $tgl2 . " " . $BulanIndo[(int)$bulanselesai-1]. " ". $tahun2;

        return view('pengalaman.elektrikal.detail', compact('elka','result','result2'));
    }

    public function cari(Request $request)
	{
		// menangkap data pencarian
        $cari = $request->cari;

    	// mengambil data dari table pegawai sesuai pencarian data
        $elka = RiwayatPekerjaan::join('klasifikasi','kl_id','rp_kl_id')->join('provinsi','id_provinsi','rp_kodeLok')->join('sub_klasifikasi','sk_kode','rp_subKlas')->where('rp_kl_id', 3)->where('rp_subKlas','like',"%".$cari."%")->orWhere('rp_namaProyek','like',"%".$cari."%")->orWhere('rp_lokasiProp','like',"%".$cari."%")->orWhere('rp_kodeLok','like',"%".$cari."%")->orWhere('sk_subKlasifikasi','like',"%".$cari."%")->orWhere('provinsi','like',"%".$cari."%")->paginate(10);
        $elka->appends($request->only('cari'));

    	// mengirim data pegawai ke view index
		return view('pengalaman.elektrikal.index', compact('cari','elka'));

	}
}
