<?php

namespace App\Http\Controllers;

use App\{Pengajuan, Klasifikasi, SubKlasifikasi, Kualifikasi, Provinsi, Pekerjaan, Kabkota, Kecamatan, Keldes, RiwayatPekerjaan};
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Auth;

class PengajuanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->level == 'staff') {
            $pengajuan = DB::table('pengajuans')
          ->where('no_reg_asosiasi', '=', Auth::user()->kode_staff)
          ->orderByRaw('created_at DESC')->paginate(10);
        } else {
            $pengajuan = DB::table('pengajuans')->orderByRaw('created_at DESC')->paginate(10);
        }
        return view('pengajuan.data', ['pengajuan' => $pengajuan]);
    }

    public function proses()
    {
        if (Auth::user()->level == 'staff') {
            $pengajuan = DB::table('pengajuans')
          ->where([
            ['no_reg_asosiasi', '=', Auth::user()->kode_staff],
            ['status', '=', 'menunggu']
          ])
          ->orderByRaw('created_at DESC')->paginate(10);
        } else {
            $pengajuan = DB::table('pengajuans')
          ->where('status', '=', 'menunggu')
          ->orderByRaw('created_at DESC')->paginate(10);
        }

        return view('pengajuan.data', ['pengajuan' => $pengajuan]);
    }

    public function selesai()
    {
        if (Auth::user()->level == 'staff') {
            $pengajuan = DB::table('pengajuans')
          ->where([
            ['no_reg_asosiasi', '=', Auth::user()->kode_staff],
            ['status', '=', 'acc']
          ])
          ->orderByRaw('created_at DESC')->paginate(10);
        } else {
            $pengajuan = DB::table('pengajuans')
          ->where('status', '=', 'acc')
          ->orderByRaw('created_at DESC')->paginate(10);
        }

        return view('pengajuan.data', ['pengajuan' => $pengajuan]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Wilayah
        $province = Provinsi::all();
        $klasifikasi = Klasifikasi::all();
        return view('pengajuan.form', ['klasifikasi' => $klasifikasi, 'provinsi' => $province]);
    }

    public function createktp()
    {
        // Wilayah
        $province = Provinsi::all();
        $klasifikasi = Klasifikasi::all();
        $nik = "";
        return view('pengajuan.form-ktp', ['klasifikasi' => $klasifikasi, 'provinsi' => $province, 'nik' => $nik]);
    }

    public function checkNik(Request $request)
    {
      $klasifikasi = Klasifikasi::all();
      $pengajuan = Pengajuan::where('nik', $request->nik)->first();
      $nik = $request->nik;
      return view('pengajuan.form-ktp', ['klasifikasi' => $klasifikasi, 'pengajuan' => $pengajuan, 'nik' => $nik]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_subklasifikasi = json_encode($request->id_subklasifikasi);
        $id_kualifikasi    = json_encode($request->id_kualifikasi);
        $keterangan        = json_encode($request->keterangan);
        $tipePengajuan     = $request->tipe_permohonan == '1' ? 'ska' : 'skt';

        $pengajuan = DB::table('pengajuans')->insertGetId([
            'tipe_permohonan'    => $tipePengajuan,
            'nama_pemohon'       => $request->nama_pemohon,
            'tempat_lahir'       => $request->tempat_lahir,
            'tgl_lahir'          => $request->tgl_lahir,
            'alamat'             => $request->alamat,
            'id_provinsi'        => $request->id_provinsi,
            'id_kabkota'         => $request->id_kabkota,
            'id_kecamatan'       => $request->id_kecamatan,
            'id_keldes'          => $request->id_keldes,
            'nik'                => $request->nik,
            'npwp'               => $request->npwp,
            'no_hp'              => $request->no_hp,
            'email'              => $request->email,
            'no_reg_asosiasi'    => $request->no_reg_asosiasi,
            'jenjang_pendidikan' => $request->jenjang_pendidikan,
            'jurusan'            => $request->jurusan,
            'nama_sekolah'       => $request->nama_sekolah,
            'alamat_sekolah'     => $request->alamat_sekolah,
            'tahun_lulus'        => $request->tahun_lulus,
            'nomor_ijazah'       => $request->nomor_ijazah,
            'id_klasifikasi'     => $request->id_klasifikasi,
            'id_subklasifikasi'  => $id_subklasifikasi,
            'id_kualifikasi'     => $id_kualifikasi,
            'keterangan'         => $keterangan,
            'status'             => 'menunggu',
            'created_at'         => now()->toDateTimeString(),
            'updated_at'         => now()->toDateTimeString()
        ]);

        $pemohon = Pengajuan::find($pengajuan);

        return redirect('pengajuan/pengalaman/'.$pemohon->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pengajuan  $pengajuan
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $selectData = 'DISTINCT LEFT(riwayat_pekerjaan.rp_tglMulai, 4) as tahun, riwayat_pekerjaan.rp_id, riwayat_pekerjaan.rp_subklas, riwayat_pekerjaan.rp_lokasiProp, riwayat_pekerjaan.rp_kodelok, riwayat_pekerjaan.rp_nilai, riwayat_pekerjaan.rp_namaproyek, provinsi.provinsi, riwayat_pekerjaan.rp_tglMulai, riwayat_pekerjaan.rp_tglSelesai, sub_klasifikasi.sk_nama';

        $pengajuan = Pengajuan::findOrFail($id);
        $users = DB::table('pengajuans')
            ->join('provinsi', 'pengajuans.id_provinsi', '=', 'provinsi.id_provinsi')
            ->join('kabkota', 'pengajuans.id_kabkota', '=', 'kabkota.id_kabkota')
            ->join('kecamatan', 'pengajuans.id_kecamatan', '=', 'kecamatan.id_kecamatan')
            ->join('keldes', 'pengajuans.id_keldes', '=', 'keldes.id_keldes')
            ->select('pengajuans.*', 'provinsi.provinsi', 'kabkota.kabkota', 'kecamatan.kecamatan', 'keldes.keldes')
            ->where('id', '=', $id)
            ->get();
        $provinsi = Provinsi::where('id_provinsi', $pengajuan->id_provinsi)->first();
        $kualifikasi = Kualifikasi::all();
        $result = SubKlasifikasi::whereIn('sk_id', $pengajuan->id_subklasifikasi)->get();

        switch (count($result)) {
          case '1':
          $subklasifikasi = SubKlasifikasi::where('sk_id', $pengajuan->id_subklasifikasi)->first();
          $pengalaman1 = DB::table('riwayat_pekerjaan')
                ->join('provinsi', 'riwayat_pekerjaan.rp_kodeLok', '=', 'provinsi.id_provinsi')
                ->join('sub_klasifikasi', 'riwayat_pekerjaan.rp_subKlas', '=', 'sub_klasifikasi.sk_kode')
                ->whereRaw('LEFT(riwayat_pekerjaan.rp_tglMulai, 4) > '.$pengajuan->tahun_lulus.'')
                ->where([
                  ['provinsi.provinsi', '=', $provinsi->provinsi],
                  ['riwayat_pekerjaan.rp_subKlas', '=', $subklasifikasi->sk_kode]
                ])
                ->selectRAW($selectData)
                ->inRandomOrder()
                ->limit(5)
                ->get();

          if ($pengalaman1->isEmpty()) {
              $pengalaman1 = DB::table('riwayat_pekerjaan')
                ->join('provinsi', 'riwayat_pekerjaan.rp_kodeLok', '=', 'provinsi.id_provinsi')
                ->join('sub_klasifikasi', 'riwayat_pekerjaan.rp_subKlas', '=', 'sub_klasifikasi.sk_kode')
                ->whereRaw('LEFT(riwayat_pekerjaan.rp_tglMulai, 4) > '.$pengajuan->tahun_lulus.'')
                ->where('riwayat_pekerjaan.rp_subKlas', '=', $subklasifikasi->sk_kode)  
                ->selectRAW($selectData)
                ->inRandomOrder()
                ->limit(5)
                ->get();
          }
          $pengalaman1 = $pengalaman1->unique('tahun', 'rp_namaproyek');
          return view('pengajuan.pengalaman', ['users' => $users, 'pengajuan' => $pengajuan, 'pengalaman1' => $pengalaman1, 'result' => $result, 'kualifikasi' => $kualifikasi ]);
            break;
          case '2':
          $subklasifikasi1 = SubKlasifikasi::where('sk_id', $pengajuan->id_subklasifikasi[0])->first();
          $subklasifikasi2 = SubKlasifikasi::where('sk_id', $pengajuan->id_subklasifikasi[1])->first();

          $pengalaman1 = DB::table('riwayat_pekerjaan')
              ->join('provinsi', 'riwayat_pekerjaan.rp_kodeLok', '=', 'provinsi.id_provinsi')
              ->join('sub_klasifikasi', 'riwayat_pekerjaan.rp_subKlas', '=', 'sub_klasifikasi.sk_kode')
              ->whereRaw('LEFT(riwayat_pekerjaan.rp_tglMulai, 4) > '.$pengajuan->tahun_lulus.'')
              ->where([
                ['provinsi.provinsi', '=', $provinsi->provinsi],
                ['riwayat_pekerjaan.rp_subKlas', '=', $subklasifikasi1->sk_kode]
              ])
              ->selectRAW($selectData)
              ->inRandomOrder()
              ->limit(5)
              ->get();

          if ($pengalaman1->isEmpty()) {
              $pengalaman1 = DB::table('riwayat_pekerjaan')
                ->join('provinsi', 'riwayat_pekerjaan.rp_kodeLok', '=', 'provinsi.id_provinsi')
                ->join('sub_klasifikasi', 'riwayat_pekerjaan.rp_subKlas', '=', 'sub_klasifikasi.sk_kode')
                ->whereRaw('LEFT(riwayat_pekerjaan.rp_tglMulai, 4) > '.$pengajuan->tahun_lulus.'')
                ->where('riwayat_pekerjaan.rp_subKlas', '=', $subklasifikasi1->sk_kode)
                ->selectRAW($selectData)
                ->inRandomOrder()
                ->limit(5)
                ->get();
          }

          $pengalaman2 = DB::table('riwayat_pekerjaan')
              ->join('provinsi', 'riwayat_pekerjaan.rp_kodeLok', '=', 'provinsi.id_provinsi')
              ->join('sub_klasifikasi', 'riwayat_pekerjaan.rp_subKlas', '=', 'sub_klasifikasi.sk_kode')
              ->whereRaw('LEFT(riwayat_pekerjaan.rp_tglMulai, 4) > '.$pengajuan->tahun_lulus.'')
              ->where([
                ['provinsi.provinsi', '=', $provinsi->provinsi],
                ['riwayat_pekerjaan.rp_subKlas', '=', $subklasifikasi2->sk_kode]
              ])
              ->selectRAW($selectData)
              ->inRandomOrder()
              ->limit(5)
              ->get();


          if ($pengalaman2->isEmpty()) {
              $pengalaman2 = DB::table('riwayat_pekerjaan')
                ->join('provinsi', 'riwayat_pekerjaan.rp_kodeLok', '=', 'provinsi.id_provinsi')
                ->join('sub_klasifikasi', 'riwayat_pekerjaan.rp_subKlas', '=', 'sub_klasifikasi.sk_kode')
                ->whereRaw('LEFT(riwayat_pekerjaan.rp_tglMulai, 4) > '.$pengajuan->tahun_lulus.'')
                ->where('riwayat_pekerjaan.rp_subKlas', '=', $subklasifikasi2->sk_kode)
                ->selectRAW($selectData)
                ->inRandomOrder()
                ->limit(5)
                ->get();
          }
          $pengalaman1 = $pengalaman1->unique('tahun', 'rp_namaproyek');
          $pengalaman2 = $pengalaman2->unique('tahun', 'rp_namaproyek');
          return view('pengajuan.pengalaman', ['users' => $users, 'pengajuan' => $pengajuan, 'pengalaman1' => $pengalaman1, 'pengalaman2' => $pengalaman2, 'result' => $result]);
            break;
          case '3':
          $subklasifikasi1 = SubKlasifikasi::where('sk_id', $pengajuan->id_subklasifikasi[0])->first();
          $subklasifikasi2 = SubKlasifikasi::where('sk_id', $pengajuan->id_subklasifikasi[1])->first();
          $subklasifikasi3 = SubKlasifikasi::where('sk_id', $pengajuan->id_subklasifikasi[2])->first();
          // return $subklasifikasi2;

          $pengalaman1 = DB::table('riwayat_pekerjaan')
              ->join('provinsi', 'riwayat_pekerjaan.rp_kodeLok', '=', 'provinsi.id_provinsi')
              ->join('sub_klasifikasi', 'riwayat_pekerjaan.rp_subKlas', '=', 'sub_klasifikasi.sk_kode')
              ->whereRaw('LEFT(riwayat_pekerjaan.rp_tglMulai, 4) > '.$pengajuan->tahun_lulus.'')
              ->where([
                ['provinsi.provinsi', '=', $provinsi->provinsi],
                ['riwayat_pekerjaan.rp_subKlas', '=', $subklasifikasi1->sk_kode]
              ])
              ->selectRAW($selectData)
              ->inRandomOrder()
              ->limit(5)
              ->get();

          $pengalaman2 = DB::table('riwayat_pekerjaan')
              ->join('provinsi', 'riwayat_pekerjaan.rp_kodeLok', '=', 'provinsi.id_provinsi')
              ->join('sub_klasifikasi', 'riwayat_pekerjaan.rp_subKlas', '=', 'sub_klasifikasi.sk_kode')
              ->whereRaw('LEFT(riwayat_pekerjaan.rp_tglMulai, 4) > '.$pengajuan->tahun_lulus.'')
              ->where([
                ['provinsi.provinsi', '=', $provinsi->provinsi],
                ['riwayat_pekerjaan.rp_subKlas', '=', $subklasifikasi2->sk_kode]
              ])
              ->selectRAW($selectData)
              ->inRandomOrder()
              ->limit(5)
              ->get();

          $pengalaman3 = DB::table('riwayat_pekerjaan')
              ->join('provinsi', 'riwayat_pekerjaan.rp_kodeLok', '=', 'provinsi.id_provinsi')
              ->join('sub_klasifikasi', 'riwayat_pekerjaan.rp_subKlas', '=', 'sub_klasifikasi.sk_kode')
              ->whereRaw('LEFT(riwayat_pekerjaan.rp_tglMulai, 4) > '.$pengajuan->tahun_lulus.'')
              ->where([
                ['provinsi.provinsi', '=', $provinsi->provinsi],
                ['riwayat_pekerjaan.rp_subKlas', '=', $subklasifikasi3->sk_kode]
              ])
              ->selectRAW($selectData)
              ->inRandomOrder()
              ->limit(5)
              ->get();

            if ($pengalaman1->isEmpty()) {
              $pengalaman1 = DB::table('riwayat_pekerjaan')
                ->join('provinsi', 'riwayat_pekerjaan.rp_kodeLok', '=', 'provinsi.id_provinsi')
                ->join('sub_klasifikasi', 'riwayat_pekerjaan.rp_subKlas', '=', 'sub_klasifikasi.sk_kode')
                ->whereRaw('LEFT(riwayat_pekerjaan.rp_tglMulai, 4) > '.$pengajuan->tahun_lulus.'')
                ->where('riwayat_pekerjaan.rp_subKlas', '=', $subklasifikasi1->sk_kode)
                ->selectRAW($selectData)
                ->inRandomOrder()
                ->limit(5)
                ->get();
          }
          if ($pengalaman2->isEmpty()) {
              $pengalaman1 = DB::table('riwayat_pekerjaan')
                ->join('provinsi', 'riwayat_pekerjaan.rp_kodeLok', '=', 'provinsi.id_provinsi')
                ->join('sub_klasifikasi', 'riwayat_pekerjaan.rp_subKlas', '=', 'sub_klasifikasi.sk_kode')
                ->whereRaw('LEFT(riwayat_pekerjaan.rp_tglMulai, 4) > '.$pengajuan->tahun_lulus.'')
                ->where('riwayat_pekerjaan.rp_subKlas', '=', $subklasifikasi2->sk_kode)
                ->selectRAW($selectData)
                ->inRandomOrder()
                ->limit(5)
                ->get();
          }
          if ($pengalaman3->isEmpty()) {
              // Query jika pengalaman tidak memiliki daerah
              $pengalaman1 = DB::table('riwayat_pekerjaan')
                ->join('provinsi', 'riwayat_pekerjaan.rp_kodeLok', '=', 'provinsi.id_provinsi')
                ->join('sub_klasifikasi', 'riwayat_pekerjaan.rp_subKlas', '=', 'sub_klasifikasi.sk_kode')
                ->whereRaw('LEFT(riwayat_pekerjaan.rp_tglMulai, 4) > '.$pengajuan->tahun_lulus.'')
                ->where('riwayat_pekerjaan.rp_subKlas', '=', $subklasifikasi3->sk_kode)
                ->selectRAW($selectData)
                ->inRandomOrder()
                ->limit(5)
                ->get();
          }
          $pengalaman1 = $pengalaman1->unique('tahun', 'rp_namaproyek');
          $pengalaman2 = $pengalaman2->unique('tahun', 'rp_namaproyek');
          $pengalaman3 = $pengalaman3->unique('tahun', 'rp_namaproyek');
          return view('pengajuan.pengalaman', ['users' => $users, 'pengajuan' => $pengajuan, 'pengalaman1' => $pengalaman1, 'pengalaman2' => $pengalaman2, 'pengalaman3' => $pengalaman3, 'result' => $result]);
            break;
          default:
            return 'Kesalahan';
            break;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pengajuan  $pengajuan
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $pengajuan = Pengajuan::findOrFail($id);
        $users = DB::table('pengajuans')
          ->join('provinsi', 'pengajuans.id_provinsi', '=', 'provinsi.id_provinsi')
          ->join('kabkota', 'pengajuans.id_kabkota', '=', 'kabkota.id_kabkota')
          ->join('kecamatan', 'pengajuans.id_kecamatan', '=', 'kecamatan.id_kecamatan')
          ->join('keldes', 'pengajuans.id_keldes', '=', 'keldes.id_keldes')
          ->select('pengajuans.*', 'provinsi.provinsi', 'kabkota.kabkota', 'kecamatan.kecamatan', 'keldes.keldes')
          ->where('id', '=', $id)
          ->get();

        $kualifikasi = SubKlasifikasi::whereIn('sk_id', $pengajuan->id_subklasifikasi)->get();

        $pekerjaan = DB::table('pekerjaans')
        ->join('provinsi', 'pekerjaans.rp_kodeLok', '=', 'provinsi.id_provinsi')
        ->join('sub_klasifikasi', 'pekerjaans.rp_subKlas', '=', 'sub_klasifikasi.sk_kode')
        ->where('pekerjaans.id_pengajuan', '=', $id)
        ->distinct()
        ->get();

        // return $pekerjaan;
        $pekerjaan = $pekerjaan->unique('rp_namaProyek', 'rp_tglMulai');
        $daerah = Provinsi::all();
        return view('pengajuan.data-detail', ['pengajuan' => $pengajuan, 'users' => $users, 'kualifikasi' => $kualifikasi, 'pekerjaan' => $pekerjaan, 'daerah' => $daerah]);
    }

    public function edit($id)
    {
      $pengajuan = Pengajuan::findOrFail($id);
      $province = Provinsi::all();

      $kabupaten = Kabkota::where('id_kabkota_provinsi', $pengajuan->id_provinsi)->get();
      $kecamatan = Kecamatan::where('id_kabkota_kecamatan', '=', $pengajuan->id_kabkota)->get();
      $kelurahan = Keldes::where('id_kecamatan_keldes', '=', $pengajuan->id_kecamatan)->get();

      $klasifikasi = Klasifikasi::all();
      $subklasifikasi = SubKlasifikasi::where([
        ['sk_jenis', '=', $pengajuan->tipe_permohonan],
        ['sk_kl_id', '=', $pengajuan->id_klasifikasi]
        ])->get();
      if($pengajuan->tipe_permohonan == 'ska') {
        $idJenis = 1;
      } elseif ($pengajuan->tipe_permohonan == 'skt') {
        $idJenis = 2;
      }
      $kualifikasi = json_decode(AllKualifikasi($pengajuan->tipe_permohonan, $pengajuan->no_reg_asosiasi));
      return view('pengajuan.edit', [
        'provinsi' => $province,
        'klasifikasi' => $klasifikasi,
        'pengajuan' => $pengajuan,
        'klasifikasi' => $klasifikasi,
        'subklasifikasi' => $subklasifikasi,
        'kualifikasi' => $kualifikasi,
        'kabkota' => $kabupaten,
        'kecamatan' => $kecamatan,
        'keldes' => $kelurahan
      ]);
    }

    public function update(Request $request, $id)
    {

      $cv = json_encode($request->cv);

      $tipePengajuan = $request->tipe_permohonan == '1' ? 'ska' : 'skt';
      $pengajuan = Pengajuan::findOrFail($id);
      $pengajuan->tipe_permohonan = $tipePengajuan;
      $pengajuan->nama_pemohon = $request->nama_pemohon;
      $pengajuan->tempat_lahir = $request->tempat_lahir;
      $pengajuan->tgl_lahir = $request->tgl_lahir;
      $pengajuan->alamat = $request->alamat;
      $pengajuan->id_provinsi = $request->id_provinsi;
      $pengajuan->id_kabkota = $request->id_kabkota;
      $pengajuan->id_kecamatan = $request->id_kecamatan;
      $pengajuan->id_keldes = $request->id_keldes;
      $pengajuan->nik = $request->nik;
      $pengajuan->npwp = $request->npwp;
      $pengajuan->no_hp = $request->no_hp;
      $pengajuan->email = $request->email;
      $pengajuan->no_reg_asosiasi = $request->no_reg_asosiasi;
      $pengajuan->jenis_permohonan = $request->jenis_permohonan;
      $pengajuan->cv = $cv;
      $pengajuan->jenjang_pendidikan = $request->jenjang_pendidikan;
      $pengajuan->jurusan = $request->jurusan;
      $pengajuan->nama_sekolah = $request->nama_sekolah;
      $pengajuan->alamat_sekolah = $request->alamat_sekolah;
      $pengajuan->tahun_lulus = $request->tahun_lulus;
      $pengajuan->nomor_ijazah = $request->nomor_ijazah;
      $pengajuan->id_klasifikasi = $request->id_klasifikasi;
      $pengajuan->id_subklasifikasi = $request->id_subklasifikasi;
      $pengajuan->id_kualifikasi = $request->id_kualifikasi;
      $pengajuan->keterangan = $request->keterangan;
      $pengajuan->updated_at = now()->toDateTimeString();

      $pengajuan->save();
      return redirect('pengajuan/detail/'.$id)->with('success', 'Status berhasil diubah');
    }

    public function UpdateStatus(Request $request, $id)
    {
        $pengajuan = Pengajuan::findOrFail($id);
        $nama = $pengajuan->nama_pemohon;
        $nik = $pengajuan->nik;
        $email = $pengajuan->email;
        $jenis = $pengajuan->tipe_permohonan;
        $klasifikasi = Klasifikasi($pengajuan->id_klasifikasi);
        $tanggal = $pengajuan->tgl_surat;

        if($request->status == 'acc') {
          $kualifikasi = SubKlasifikasi::whereIn('sk_id', $pengajuan->id_subklasifikasi)->get();
          $k = 0;
          foreach($kualifikasi as $kualifikasi) {
            $data = array(
              'nama' => $nama,
              'nik' => $nik,
              'email' => $email,
              'password' => '',
              'jenis' => $jenis,
              'idTingkat' => $pengajuan->id_kualifikasi[$k++],
              'klasifikasi' => $klasifikasi,
              'kualifikasi' => $kualifikasi->sk_kode.'/'.$kualifikasi->sk_nama,
              'tanggalTransaksi' => $request->tgl_surat
            );

            // insert organisasi
            if($pengajuan->no_reg_asosiasi == '152') {
              $url = 'http://localhost:8888/ataksi/curl-post/ataksi/ataksi.php';
            } elseif($pengajuan->no_reg_asosiasi == '144') {
              $url = 'http://localhost:8888/ataksi/curl-post/asdamkindo/asdamkindo.php';
            } elseif($pengajuan->no_reg_asosiasi == '180') {
              $url = 'http://localhost:8888/ataksi/curl-post/proteksi/proteksi.php';
            } else {
              $url = 'Undefined';
            }
            $payload = json_encode(array("transaksi" => $data));
            $response = CURLDATA($url, $payload);
          }

          if($response == 'success')
          {
              $pengajuan->status = $request->status;
              $pengajuan->nomor_surat = $request->nomor_surat;
              $pengajuan->pemeriksa = $request->pemeriksa;
              $pengajuan->tgl_surat = $request->tgl_surat;
              $pengajuan->save();
              return redirect('pengajuan/detail/'.$pengajuan->id)->with('success', 'Berhasil mengaproval dan mengirim data permohonan');
          } else {
              return redirect('pengajuan/detail/'.$pengajuan->id)->with('delete', 'Gagal sinkronisasi data antar server, Hubungi pengembang segera');
          }

        } elseif ($request->status == 'menunggu') {

            $pengajuan->status = $request->status;
            $pengajuan->nomor_surat = $request->nomor_surat;
            $pengajuan->pemeriksa = $request->pemeriksa;
            $pengajuan->tgl_surat = $request->tgl_surat;
            $pengajuan->save();
            return redirect('pengajuan/detail/'.$pengajuan->id)->with('success', 'Data berhasil diubah');
        }
    }

    public function cari(Request $request)
	{
    $cari = $request->cari;

    $pengajuan = Pengajuan::where('tipe_permohonan','like',"%".$cari."%")->orWhere('nama_pemohon','like',"%".$cari."%")->orWhere('tempat_lahir','like',"%".$cari."%")->orWhere('tgl_lahir','like',"%".$cari."%")->orWhere('alamat','like',"%".$cari."%")->orWhere('provinsi','like',"%".$cari."%")->orWhere('kabkota','like',"%".$cari."%")->orWhere('kecamatan','like',"%".$cari."%")->orWhere('keldes','like',"%".$cari."%")->orWhere('wilayah_kalimat','like',"%".$cari."%")->orWhere('nik','like',"%".$cari."%")->orWhere('npwp','like',"%".$cari."%")->orWhere('email','like',"%".$cari."%")->orWhere('no_reg_asosiasi','like',"%".$cari."%")->orWhere('jenis_permohonan','like',"%".$cari."%")->orWhere('jenjang_pendidikan	','like',"%".$cari."%")->orWhere('jurusan','like',"%".$cari."%")->orWhere('nama_sekolah','like',"%".$cari."%")->orWhere('alamat_sekolah','like',"%".$cari."%")->orWhere('kl_nama','like',"%".$cari."%")->orWhere('kul_nama','like',"%".$cari."%")->orWhere('status','like',"%".$cari."%")->paginate(10);
    $pengajuan->appends($request->only('cari'));

		return view('pengajuan.data',compact('pengajuan'));

  }
  
  public function destroy($id)
  {
      $pengajuan = Pengajuan::findOrFail($id);
      $pekerjaan = Pekerjaan::where('id_pengajuan', $id);
      $pengajuan->delete();
      $pekerjaan->delete();
      return redirect('pengajuan/proses/')->with('delete', 'Data pengajuan berhasil dihapus');
  }

  public function deleteAll(Request $request)
  {
      $ids = $request->ids;
      DB::table("pekerjaans")->whereIn('id',explode(",",$ids))->delete();
      return response()->json(['success'=>"Products Deleted successfully."]);
  }
}
