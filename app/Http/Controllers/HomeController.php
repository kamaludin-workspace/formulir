<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{Pengajuan, RiwayatPekerjaan, Klasifikasi, SubKlasifikasi};

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pengajuan = Pengajuan::count();
        $pengalaman = RiwayatPekerjaan::count();
        $klasifikasi = Klasifikasi::count();
        $subklasifikasi = SubKlasifikasi::count();

        return view('welcome', [
            'pengajuan' => $pengajuan,
            'pengalaman' => $pengalaman,
            'klasifikasi' => $klasifikasi,
            'subklasifikasi' => $subklasifikasi
        ]);
    }
}
