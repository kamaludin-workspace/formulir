<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RiwayatPekerjaan;
use App\SubKlasifikasi;
use App\provinsi;

class ArsitekturController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $ark = RiwayatPekerjaan::join('klasifikasi','kl_id','rp_kl_id')->join('provinsi','id_provinsi','rp_kodeLok')->join('sub_klasifikasi','sk_kode','rp_subKlas')->where('rp_kl_id', 1)->paginate(10);
        $cari = null;
        return view('pengalaman.arsitektur.index', compact('ark','cari'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $sub_klas = SubKlasifikasi::where('sk_klasifikasi','AA')->get();
        $provinsi = provinsi::all();
        return view('pengalaman.arsitektur.create', compact('sub_klas','provinsi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $lok = $request['rp_kodeLok'];
        $kodelokasi = Provinsi::where('provinsi', $lok)->get();
        $kode = $kodelokasi[0]->id_provinsi;

        $ark = new RiwayatPekerjaan;
        $ark->rp_kl_id = $request['rp_kl_id'];
        $ark->rp_subKlas = $request['rp_subKlas'];
        $ark->rp_namaProyek = $request['rp_namaProyek'];
        $ark->rp_lokasiProp = $request['rp_kodeLok'];
        $ark->rp_kodeLok = $kode;
        $result = preg_replace("/[^0-9]/", "", $request['rp_nilai']);
        $ark->rp_nilai = $result;
        $ark->rp_tglMulai = $request['rp_tglMulai'];
        $ark->rp_tglSelesai = $request['rp_tglSelesai'];
        $ark->rp_tahun = $request['rp_tahun'];
        
        $ark->save();
        return redirect('/home');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\pekerjaan  $pekerjaan
     * @return \Illuminate\Http\Response
     */
    public function show(pekerjaan $pekerjaan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\pekerjaan  $pekerjaan
     * @return \Illuminate\Http\Response
     */
    public function edit($rp_id)
    {
        //
        $sub_klas = SubKlasifikasi::where('sk_klasifikasi','AA')->get();
        $ark = RiwayatPekerjaan::find($rp_id);
        $provinsi = provinsi::all();

        return view('pengalaman.arsitektur.edit', compact('ark','provinsi','sub_klas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\pekerjaan  $pekerjaan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $rp_id)
    {
        //
        $lok = $request['rp_kodeLok'];
        $kodelokasi = Provinsi::where('provinsi', $lok)->get();
        $kode = $kodelokasi[0]->id_provinsi;

        $ark = RiwayatPekerjaan::find($rp_id);
        $ark->rp_kl_id = $request['rp_kl_id'];
        $ark->rp_subKlas = $request['rp_subKlas'];
        $ark->rp_namaProyek = $request['rp_namaProyek'];
        $ark->rp_lokasiProp = $request['rp_kodeLok'];
        $ark->rp_kodeLok = $kode;
        $result = preg_replace("/[^0-9]/", "", $request['rp_nilai']);
        $ark->rp_nilai = $result;
        $ark->rp_tglMulai = $request['rp_tglMulai'];
        $ark->rp_tglSelesai = $request['rp_tglSelesai'];
        // $ark->rp_tahun = $request['rp_tahun'];
        $ark->save();

        return redirect('/home');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\pekerjaan  $pekerjaan
     * @return \Illuminate\Http\Response
     */
    public function destroy( $rp_id)
    {
        //
        $ark = RiwayatPekerjaan::find($rp_id);
        $ark->delete();

        return redirect('/home');
    }

    public function detail($rp_id)
    {
        $ark = RiwayatPekerjaan::join('klasifikasi','kl_id','rp_kl_id')->join('provinsi','id_provinsi','rp_kodeLok')->join('sub_klasifikasi','sk_kode','rp_subKlas')->where('rp_kl_id', 1)->find($rp_id);

        //menampilkan tanggal format Indonesia
        $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September","Oktober", "November", "Desember");
        $tahun = substr($ark->rp_tglMulai, 0, 4);
        $tahun2 = substr($ark->rp_tglSelesai, 0, 4);
        $bulan = substr($ark->rp_tglMulai, 5, 2);
        $bulanselesai = substr($ark->rp_tglSelesai, 5, 2);
        $tgl   = substr($ark->rp_tglMulai, 8, 2);
        $tgl2   = substr($ark->rp_tglSelesai, 8, 2);
        $result = $tgl . " " . $BulanIndo[(int)$bulan-1]. " ". $tahun;
        $result2 = $tgl2 . " " . $BulanIndo[(int)$bulanselesai-1]. " ". $tahun2;

        return view('pengalaman.arsitektur.detail', compact('ark','result','result2'));
    }

    public function cari(Request $request)
	{
		// menangkap data pencarian
        $cari = $request->cari;

    	// mengambil data dari table pegawai sesuai pencarian data
        $ark = RiwayatPekerjaan::join('klasifikasi','kl_id','rp_kl_id')->join('provinsi','id_provinsi','rp_kodeLok')->join('sub_klasifikasi','sk_kode','rp_subKlas')->where('rp_kl_id', 1)->where('rp_subKlas','like',"%".$cari."%")->orWhere('rp_namaProyek','like',"%".$cari."%")->orWhere('rp_lokasiProp','like',"%".$cari."%")->orWhere('rp_kodeLok','like',"%".$cari."%")->orWhere('sk_subKlasifikasi','like',"%".$cari."%")->orWhere('provinsi','like',"%".$cari."%")->paginate(10);
        $ark->appends($request->only('cari'));

    	// mengirim data pegawai ke view index
		return view('pengalaman.arsitektur.index', compact('cari','ark'));

	}
}
