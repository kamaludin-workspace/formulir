<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kualifikasi;
use App\SubKlasifikasi;
use App\Klasifikasi;
use App\JenisSertifikat;

class SkaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
        $ska = SubKlasifikasi::join('klasifikasi','kl_id','sk_kl_id')->where('sk_jenis','=','SKA')->paginate(10);

        return view('kualifikasi.ska.index', compact('ska'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $klasifikasi = Klasifikasi::all();
        return view('kualifikasi.ska.create', compact('klasifikasi'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $ska = new kualifikasi;
        $ska->kul_js_id = 1;
        $ska->kul_nama = $request['kul_nama'];
        $ska->save();
        return redirect('/ska');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Klasifikasi  $klasifikasi
     * @return \Illuminate\Http\Response
     */
    public function show(Klasifikasi $klasifikasi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Klasifikasi  $klasifikasi
     * @return \Illuminate\Http\Response
     */
    public function edit( $sk_id)
    {
        //
        $ska = SubKlasifikasi::find($sk_id);
        $klasifikasi = Klasifikasi::all();
        return view('kualifikasi.ska.edit', compact('ska','klasifikasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Klasifikasi  $klasifikasi
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $kul_id)
    {
        //
        $ska = kualifikasi::find($kul_id);
        $ska->kul_js_id = 1;
        $ska->kul_nama = $request['kul_nama'];
        $ska->save();
        return redirect('/ska');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Klasifikasi  $klasifikasi
     * @return \Illuminate\Http\Response
     */
    public function destroy( $kul_id)
    {
        //
        $ska = kualifikasi::find($kul_id);
        $ska->delete();
        return redirect('/ska');
    }

    public function cari(Request $request)
	{
		// menangkap data pencarian
        $cari = $request->cari;

    	// mengambil data dari table pegawai sesuai pencarian data
        $ska = SubKlasifikasi::join('klasifikasi','kl_id','sk_kl_id')->where('sk_jenis','=','SKA')->where('sk_klasifikasi','like',"%".$cari."%")->orWhere('sk_nama','like',"%".$cari."%")->orWhere('sk_kode','like',"%".$cari."%")->orWhere('sk_subKlasifikasi','like',"%".$cari."%")->orWhere('sk_jenis','like',"%".$cari."%")->paginate(10);
        $ska->appends($request->only('cari'));
    	// mengirim data pegawai ke view index
		return view('kualifikasi.ska.index',compact('ska'));

	}
}
