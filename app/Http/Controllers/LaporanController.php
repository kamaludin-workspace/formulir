<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengajuan;
use App\SubKlasifikasi;
use DB;
use PDF;
use Excel;
use Auth;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class LaporanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ustkm()
    {
        $ustkm = 0;
        return view('laporan.ustkm', ['ustkm' => $ustkm]);
    }

    public function storeUstkm(Request $request)
    {
        if ($request->no_reg_asosiasi == 0)
        {
            $ustkm = DB::table('pengajuans')
            ->join('provinsi', 'pengajuans.id_provinsi', '=', 'provinsi.id_provinsi')
            ->join('kabkota', 'pengajuans.id_kabkota', '=', 'kabkota.id_kabkota')
            ->join('kecamatan', 'pengajuans.id_kecamatan', '=', 'kecamatan.id_kecamatan')
            ->join('keldes', 'pengajuans.id_keldes', '=', 'keldes.id_keldes')
            ->select(
                'pengajuans.*',
                'provinsi.provinsi',
                'kabkota.kabkota',
                'kecamatan.kecamatan',
                'keldes.keldes'
            )
            ->where([
              ['status', '=', 'acc'],
              ['tgl_surat', '=', $request->tanggal]
            ])
            ->orderByRaw('created_at DESC')
            ->get();
            $asosiasi = 'Semua Asosiasi';
            if ($ustkm->isEmpty()) {
                return redirect('laporan/ustkm')->with('warning', 'Data tidak ditemukan');
            }

        } else {
            $ustkm = DB::table('pengajuans')
            ->join('provinsi', 'pengajuans.id_provinsi', '=', 'provinsi.id_provinsi')
            ->join('kabkota', 'pengajuans.id_kabkota', '=', 'kabkota.id_kabkota')
            ->join('kecamatan', 'pengajuans.id_kecamatan', '=', 'kecamatan.id_kecamatan')
            ->join('keldes', 'pengajuans.id_keldes', '=', 'keldes.id_keldes')
            ->select(
                'pengajuans.*',
                'provinsi.provinsi',
                'kabkota.kabkota',
                'kecamatan.kecamatan',
                'keldes.keldes'
            )
            ->where([
              ['status', '=', 'acc'],
              ['no_reg_asosiasi', '=', $request->no_reg_asosiasi],
              ['tgl_surat', '=', $request->tanggal]
            ])
            ->orderByRaw('created_at DESC')
            ->get();
            $asosiasi = asosiasi($request->no_reg_asosiasi);
            if ($ustkm->isEmpty()) {
                return redirect('laporan/ustkm')->with('warning', 'Data tidak ditemukan');
            }
        }

        $kualifikasi = SubKlasifikasi::all();
        return view('laporan.ustkm', ['ustkm' => $ustkm, 'kualifikasi' => $kualifikasi, 'asosiasi' => $asosiasi]);
    }

    public function getUstkm(Request $request)
    {
        $ustkm = DB::table('pengajuans')
            ->join('provinsi', 'pengajuans.id_provinsi', '=', 'provinsi.id_provinsi')
            ->join('kabkota', 'pengajuans.id_kabkota', '=', 'kabkota.id_kabkota')
            ->join('kecamatan', 'pengajuans.id_kecamatan', '=', 'kecamatan.id_kecamatan')
            ->join('keldes', 'pengajuans.id_keldes', '=', 'keldes.id_keldes')
            ->select(
                'pengajuans.*',
                'provinsi.provinsi',
                'kabkota.kabkota',
                'kecamatan.kecamatan',
                'keldes.keldes'
            )
            ->whereIn('id', $request->idPengajuan)
            ->orderByRaw('created_at DESC')
            ->get();

        $kualifikasi = SubKlasifikasi::all();

        // Create excel file
        $spreadsheet = new Spreadsheet();
        $objWorkSheet = $spreadsheet->getActiveSheet();

        $styleArray = array(
              'borders' => array(
                  'allBorders' => array(
                      'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                      'color' => array('argb' => '000000'),
                  ),
              ),
          );
        $objWorkSheet->getStyle('A5:M5')->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setRGB('54d1ff');

        $objWorkSheet->getStyle('N4:Y5')->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setRGB('73de90');

        $objWorkSheet->getStyle('A5:Y15')->applyFromArray($styleArray);
        $objWorkSheet->getStyle('N4:Y4')->applyFromArray($styleArray);
        $objWorkSheet->getStyle('AA5:AF15')->applyFromArray($styleArray);

        $objWorkSheet->setTitle('USTKM');
        $objWorkSheet->setCellValue('B2', 'Rekapitulasi USTKM - '.$ustkm[0]->tgl_surat)
        ->getStyle('B2')->getFont()->setSize(22)->getColor()->setRGB('54d1ff');

        $objWorkSheet->mergeCells('B2:F2');
        $objWorkSheet->mergeCells('V4:Y4');
        $objWorkSheet->mergeCells('N4:Q4');
        $objWorkSheet->mergeCells('R4:U4');
        $objWorkSheet->mergeCells('AA4:AB4');
        $objWorkSheet->mergeCells('AC4:AD4');
        $objWorkSheet->mergeCells('AE4:AF4');

        $objWorkSheet->setCellValue("N4", strtoupper("BARU / TAMBAH SUB BID 1"));
        $objWorkSheet->setCellValue("R4", strtoupper("BARU / TAMBAH SUB BID 2"));
        $objWorkSheet->setCellValue("V4", strtoupper("BARU / TAMBAH SUB BID 3"));

        $objWorkSheet->setCellValue('A5', strtoupper('No'));
        $objWorkSheet->setCellValue('B5', strtoupper('Asosiasi'));
        $objWorkSheet->setCellValue('C5', strtoupper('Nama'));
        $objWorkSheet->setCellValue('D5', strtoupper('No KTP'));
        $objWorkSheet->setCellValue('E5', strtoupper('No. NPWP'));
        $objWorkSheet->setCellValue('F5', strtoupper('Tempat Lahir'));
        $objWorkSheet->setCellValue('G5', strtoupper('Tanggal Lahir'));
        $objWorkSheet->setCellValue('H5', strtoupper('Propinsi Sesuai KTP'));
        $objWorkSheet->setCellValue('I5', strtoupper('Kabupaten Sesuai KTP'));
        $objWorkSheet->setCellValue('J5', strtoupper('Alamat'));
        $objWorkSheet->setCellValue('K5', strtoupper('Jurusan'));
        $objWorkSheet->setCellValue('L5', strtoupper('Jenjang_P'));
        $objWorkSheet->setCellValue('M5', strtoupper('HP_PEMOHON'));
        $objWorkSheet->setCellValue('N5', strtoupper('Klasifikasi'));
        $objWorkSheet->setCellValue('O5', strtoupper('Sub Klasifikasi'));
        $objWorkSheet->setCellValue('P5', strtoupper('Kode'));
        $objWorkSheet->setCellValue('Q5', strtoupper('Kualifikasi'));
        $objWorkSheet->setCellValue('R5', strtoupper('Klasifikasi'));
        $objWorkSheet->setCellValue('S5', strtoupper('Sub Klasifikasi'));
        $objWorkSheet->setCellValue('T5', strtoupper('Kode'));
        $objWorkSheet->setCellValue('U5', strtoupper('Kualifikasi'));
        $objWorkSheet->setCellValue('V5', strtoupper('Klasifikasi'));
        $objWorkSheet->setCellValue('W5', strtoupper('Sub Klasifikasi'));
        $objWorkSheet->setCellValue('X5', strtoupper('Kode'));
        $objWorkSheet->setCellValue('Y5', strtoupper('Kualifikasi'));

        $objWorkSheet->setCellValue('AA4', 'Sub Bidang 1');
        $objWorkSheet->setCellValue('AA5', 'Kualifikasi');
        $objWorkSheet->setCellValue('AB5', 'Harga');

        $objWorkSheet->setCellValue('AC4', 'Sub Bidang 2');
        $objWorkSheet->setCellValue('AC5', 'Kualifikasi');
        $objWorkSheet->setCellValue('AD5', 'Harga');

        $objWorkSheet->setCellValue('AE4', 'Sub Bidang 2');
        $objWorkSheet->setCellValue('AE5', 'Kualifikasi');
        $objWorkSheet->setCellValue('AF5', 'Harga');

        $a = 1;
        foreach ($ustkm as $data) :
            $objWorkSheet->getColumnDimension('A')->setWidth(4);
            $objWorkSheet->getColumnDimension('B')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('C')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('D')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('E')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('F')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('G')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('H')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('I')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('J')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('K')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('L')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('M')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('N')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('O')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('P')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('Q')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('R')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('S')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('T')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('U')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('V')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('W')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('X')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('Y')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AA')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AB')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AC')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AD')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AE')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AF')->setAutoSize(true);

            $objWorkSheet->setCellValue('A'.(5+($a)), ($a));
            $objWorkSheet->setCellValue('B'.(5+($a)), asosiasi($data->no_reg_asosiasi));
            $objWorkSheet->setCellValue('C'.(5+($a)), strtoupper($data->nama_pemohon));
            $objWorkSheet->setCellValueExplicit(
                'D'.(5+($a)),
                $data->nik,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objWorkSheet->setCellValueExplicit(
                'E'.(5+($a)),
                $data->npwp,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objWorkSheet->setCellValue('F'.(5+($a)), strtoupper($data->tempat_lahir));
            $objWorkSheet->setCellValue('G'.(5+($a)), strtoupper($data->tgl_lahir));
            $objWorkSheet->setCellValue('H'.(5+($a)), strtoupper($data->provinsi));
            $objWorkSheet->setCellValue('I'.(5+($a)), strtoupper($data->kabkota));
            $objWorkSheet->setCellValue('J'.(5+($a)), strtoupper($data->alamat));
            $objWorkSheet->setCellValue('K'.(5+($a)), strtoupper($data->jurusan));
            $objWorkSheet->setCellValue('L'.(5+($a)), strtoupper($data->jenjang_pendidikan));
            $objWorkSheet->setCellValue(
                'M'.(5+($a)),
                $data->no_hp,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
              );

            $clear = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_subklasifikasi);
            $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
            $expTingkatan = explode(",", $tingkatan);
            $exp = explode(",", $clear);

            // Kualifikasi klasifikasi dan harga setoran ustkm 1
            foreach ($kualifikasi as $key) :
                if ($key->sk_id == $exp[0]) :
                    // Kualifikasi 1
                    $objWorkSheet->setCellValue('N'.(5+($a)), strtoupper($key->sk_klasifikasi));
                    $objWorkSheet->setCellValue('O'.(5+($a)), strtoupper($key->sk_nama));
                    $objWorkSheet->setCellValue('P'.(5+($a)), strtoupper($key->sk_kode));
                    $objWorkSheet->setCellValue('Q'.(5+($a)), kualifikasi($expTingkatan[0], $data->no_reg_asosiasi));
                    $objWorkSheet->setCellValue('AA'.(5+($a)), kualifikasi($expTingkatan[0], $data->no_reg_asosiasi));
                    $objWorkSheet->setCellValue('AB'.(5+($a)), hargaUstkm($expTingkatan[0], $data->no_reg_asosiasi));
                endif;
            endforeach;


            // Kualifikasi klasifikasi dan harga setoran ustkm 2
            if (isset($exp[1])) {
                $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
                $expTingkatan = explode(",", $tingkatan);
                foreach ($kualifikasi as $key) :
                    if ($key->sk_id == $exp[1]) :
                    // Kualifikasi 2
                    $objWorkSheet->setCellValue('R'.(5+($a)), strtoupper($key->sk_klasifikasi));
                    $objWorkSheet->setCellValue('S'.(5+($a)), strtoupper($key->sk_nama));
                    $objWorkSheet->setCellValue('T'.(5+($a)), strtoupper($key->sk_kode));
                    $objWorkSheet->setCellValue('U'.(5+($a)), kualifikasi($expTingkatan[1], $data->no_reg_asosiasi));
                    $objWorkSheet->setCellValue('AC'.(5+($a)), kualifikasi($expTingkatan[1], $data->no_reg_asosiasi));
                    $objWorkSheet->setCellValue('AD'.(5+($a)), hargaUstkm($expTingkatan[1], $data->no_reg_asosiasi));
                endif;
                endforeach;
            }
            // Kualifikasi klasifikasi dan harga setoran ustkm 3
            if (isset($exp[2])) {
                $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
                $expTingkatan = explode(",", $tingkatan);
                foreach ($kualifikasi as $key) :
                    if ($key->sk_id == $exp[2]) :
                        // Kualifikasi 2
                        $objWorkSheet->setCellValue('V'.(5+($a)), strtoupper($key->sk_klasifikasi));
                        $objWorkSheet->setCellValue('W'.(5+($a)), strtoupper($key->sk_nama));
                        $objWorkSheet->setCellValue('X'.(5+($a)), strtoupper($key->sk_kode));
                        $objWorkSheet->setCellValue('Y'.(5+($a)), kualifikasi($expTingkatan[2], $data->no_reg_asosiasi));
                        $objWorkSheet->setCellValue('AE'.(5+($a)), kualifikasi($expTingkatan[2], $data->no_reg_asosiasi));
                        $objWorkSheet->setCellValue('AF'.(5+($a)), hargaUstkm($expTingkatan[2], $data->no_reg_asosiasi));
                    endif;
                endforeach;
            }

        $a++;
        endforeach;

        // _____________________________________________________________________
        // New sheet/Klasifikasi
        // AA = 1->Arsitektur
        // AS = 6->Sipil
        // AE = 3->Elektrikal
        // AM = 2->Mekanikal
        // AT = 4->TataLingkungan
        // AL = 5->Manajemen

        $Klasifikasi = array("AA", "AS", "AE", "AM", "AT", "AL");
        $klasifikasiId = array("1", "6", "3", "2", "4", "5");
        // Each array for title
        for ($x = 1; $x <= 6; $x++) :
            $spreadsheet->createSheet();
            $spreadsheet->setActiveSheetIndex($x);
            $objWorkSheet = $spreadsheet->getActiveSheet()->setTitle($Klasifikasi[$x-1]);

            $styleArray = array(
                  'borders' => array(
                      'allBorders' => array(
                          'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                          'color' => array('argb' => '000000'),
                      ),
                  ),
              );
            $objWorkSheet->getStyle('A5:M5')->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setRGB('54d1ff');

            $objWorkSheet->getStyle('N4:Y5')->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setRGB('73de90');

            $objWorkSheet->getStyle('A5:Y15')->applyFromArray($styleArray);
            $objWorkSheet->getStyle('N4:Y4')->applyFromArray($styleArray);
            $objWorkSheet->getStyle('AA5:AF15')->applyFromArray($styleArray);

            $objWorkSheet->setCellValue('B2', 'Rekapitulasi USTKM - '.$Klasifikasi[$x-1])
            ->getStyle('B2')->getFont()->setSize(22)->getColor()->setRGB('54d1ff');

            $objWorkSheet->mergeCells('B2:F2');
            $objWorkSheet->mergeCells('V4:Y4');
            $objWorkSheet->mergeCells('N4:Q4');
            $objWorkSheet->mergeCells('R4:U4');
            $objWorkSheet->mergeCells('AA4:AB4');
            $objWorkSheet->mergeCells('AC4:AD4');
            $objWorkSheet->mergeCells('AE4:AF4');

            $objWorkSheet->setCellValue("N4", strtoupper("BARU / TAMBAH SUB BID 1"));
            $objWorkSheet->setCellValue("R4", strtoupper("BARU / TAMBAH SUB BID 2"));
            $objWorkSheet->setCellValue("V4", strtoupper("BARU / TAMBAH SUB BID 3"));

            $objWorkSheet->setCellValue('A5', strtoupper('No'));
            $objWorkSheet->setCellValue('B5', strtoupper('Asosiasi'));
            $objWorkSheet->setCellValue('C5', strtoupper('Nama'));
            $objWorkSheet->setCellValue('D5', strtoupper('No KTP'));
            $objWorkSheet->setCellValue('E5', strtoupper('No. NPWP'));
            $objWorkSheet->setCellValue('F5', strtoupper('Tempat Lahir'));
            $objWorkSheet->setCellValue('G5', strtoupper('Tanggal Lahir'));
            $objWorkSheet->setCellValue('H5', strtoupper('Propinsi Sesuai KTP'));
            $objWorkSheet->setCellValue('I5', strtoupper('Kabupaten Sesuai KTP'));
            $objWorkSheet->setCellValue('J5', strtoupper('Alamat'));
            $objWorkSheet->setCellValue('K5', strtoupper('Jurusan'));
            $objWorkSheet->setCellValue('L5', strtoupper('Jenjang_P'));
            $objWorkSheet->setCellValue('M5', strtoupper('HP_PEMOHON'));
            $objWorkSheet->setCellValue('N5', strtoupper('Klasifikasi'));
            $objWorkSheet->setCellValue('O5', strtoupper('Sub Klasifikasi'));
            $objWorkSheet->setCellValue('P5', strtoupper('Kode'));
            $objWorkSheet->setCellValue('Q5', strtoupper('Kualifikasi'));
            $objWorkSheet->setCellValue('R5', strtoupper('Klasifikasi'));
            $objWorkSheet->setCellValue('S5', strtoupper('Sub Klasifikasi'));
            $objWorkSheet->setCellValue('T5', strtoupper('Kode'));
            $objWorkSheet->setCellValue('U5', strtoupper('Kualifikasi'));
            $objWorkSheet->setCellValue('V5', strtoupper('Klasifikasi'));
            $objWorkSheet->setCellValue('W5', strtoupper('Sub Klasifikasi'));
            $objWorkSheet->setCellValue('X5', strtoupper('Kode'));
            $objWorkSheet->setCellValue('Y5', strtoupper('Kualifikasi'));

            $objWorkSheet->setCellValue('AA4', 'Sub Bidang 1');
            $objWorkSheet->setCellValue('AA5', 'Kualifikasi');
            $objWorkSheet->setCellValue('AB5', 'Harga');

            $objWorkSheet->setCellValue('AC4', 'Sub Bidang 2');
            $objWorkSheet->setCellValue('AC5', 'Kualifikasi');
            $objWorkSheet->setCellValue('AD5', 'Harga');

            $objWorkSheet->setCellValue('AE4', 'Sub Bidang 2');
            $objWorkSheet->setCellValue('AE5', 'Kualifikasi');
            $objWorkSheet->setCellValue('AF5', 'Harga');

            $a = 1;
            foreach ($ustkm as $data) :
                if($data->id_klasifikasi == $klasifikasiId[$x-1]) :
                    $objWorkSheet->getColumnDimension('A')->setWidth(4);
                    $objWorkSheet->getColumnDimension('B')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('C')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('D')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('E')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('F')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('G')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('H')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('I')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('J')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('K')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('L')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('M')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('N')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('O')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('P')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('Q')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('R')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('S')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('T')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('U')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('V')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('W')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('X')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('Y')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AA')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AB')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AC')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AD')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AE')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AF')->setAutoSize(true);

                    $objWorkSheet->setCellValue('A'.(5+($a)), ($a));
                    $objWorkSheet->setCellValue('B'.(5+($a)), asosiasi($data->no_reg_asosiasi));
                    $objWorkSheet->setCellValue('C'.(5+($a)), strtoupper($data->nama_pemohon));
                    $objWorkSheet->setCellValueExplicit(
                        'D'.(5+($a)),
                        $data->nik,
                        \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $objWorkSheet->setCellValueExplicit(
                        'E'.(5+($a)),
                        $data->npwp,
                        \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $objWorkSheet->setCellValue('F'.(5+($a)), strtoupper($data->tempat_lahir));
                    $objWorkSheet->setCellValue('G'.(5+($a)), strtoupper($data->tgl_lahir));
                    $objWorkSheet->setCellValue('H'.(5+($a)), strtoupper($data->provinsi));
                    $objWorkSheet->setCellValue('I'.(5+($a)), strtoupper($data->kabkota));
                    $objWorkSheet->setCellValue('J'.(5+($a)), strtoupper($data->alamat));
                    $objWorkSheet->setCellValue('K'.(5+($a)), strtoupper($data->jurusan));
                    $objWorkSheet->setCellValue('L'.(5+($a)), strtoupper($data->jenjang_pendidikan));
                    $objWorkSheet->setCellValue(
                        'M'.(5+($a)),
                        $data->no_hp,
                        \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                      );

                    $clear = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_subklasifikasi);
                    $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
                    $expTingkatan = explode(",", $tingkatan);
                    $exp = explode(",", $clear);

                    // Kualifikasi klasifikasi dan harga setoran ustkm 1
                    foreach ($kualifikasi as $key) :
                        if ($key->sk_id == $exp[0]) :
                            // Kualifikasi 1
                            $objWorkSheet->setCellValue('N'.(5+($a)), strtoupper($key->sk_klasifikasi));
                            $objWorkSheet->setCellValue('O'.(5+($a)), strtoupper($key->sk_nama));
                            $objWorkSheet->setCellValue('P'.(5+($a)), strtoupper($key->sk_kode));
                            $objWorkSheet->setCellValue('Q'.(5+($a)), kualifikasi($expTingkatan[0], $data->no_reg_asosiasi));
                            $objWorkSheet->setCellValue('AA'.(5+($a)), kualifikasi($expTingkatan[0], $data->no_reg_asosiasi));
                            $objWorkSheet->setCellValue('AB'.(5+($a)), hargaUstkm($expTingkatan[0], $data->no_reg_asosiasi));
                        endif;
                    endforeach;


                    // Kualifikasi klasifikasi dan harga setoran ustkm 2
                    if (isset($exp[1])) {
                        $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
                        $expTingkatan = explode(",", $tingkatan);
                        foreach ($kualifikasi as $key) :
                            if ($key->sk_id == $exp[1]) :
                            // Kualifikasi 2
                            $objWorkSheet->setCellValue('R'.(5+($a)), strtoupper($key->sk_klasifikasi));
                            $objWorkSheet->setCellValue('S'.(5+($a)), strtoupper($key->sk_nama));
                            $objWorkSheet->setCellValue('T'.(5+($a)), strtoupper($key->sk_kode));
                            $objWorkSheet->setCellValue('U'.(5+($a)), kualifikasi($expTingkatan[1], $data->no_reg_asosiasi));
                            $objWorkSheet->setCellValue('AC'.(5+($a)), kualifikasi($expTingkatan[1], $data->no_reg_asosiasi));
                            $objWorkSheet->setCellValue('AD'.(5+($a)), hargaUstkm($expTingkatan[1], $data->no_reg_asosiasi));
                        endif;
                        endforeach;
                    }
                    // Kualifikasi klasifikasi dan harga setoran ustkm 3
                    if (isset($exp[2])) {
                        $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
                        $expTingkatan = explode(",", $tingkatan);
                        foreach ($kualifikasi as $key) :
                            if ($key->sk_id == $exp[2]) :
                                // Kualifikasi 2
                                $objWorkSheet->setCellValue('V'.(5+($a)), strtoupper($key->sk_klasifikasi));
                                $objWorkSheet->setCellValue('W'.(5+($a)), strtoupper($key->sk_nama));
                                $objWorkSheet->setCellValue('X'.(5+($a)), strtoupper($key->sk_kode));
                                $objWorkSheet->setCellValue('Y'.(5+($a)), kualifikasi($expTingkatan[2], $data->no_reg_asosiasi));
                                $objWorkSheet->setCellValue('AE'.(5+($a)), kualifikasi($expTingkatan[2], $data->no_reg_asosiasi));
                                $objWorkSheet->setCellValue('AF'.(5+($a)), hargaUstkm($expTingkatan[2], $data->no_reg_asosiasi));
                            endif;
                        endforeach;
                    }

                    $a++;
                endif;
            endforeach;
        endfor;

        $spreadsheet->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Rekapitulasi USTKM "'.$ustkm[0]->tgl_surat.'".xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');

    }

    // End USTKM

    public function dpp()
    {
        $dpp = 0;
        return view('laporan.dpp', ['dpp' => $dpp]);
    }

    public function storeDpp(Request $request)
    {
        if ($request->no_reg_asosiasi == 0) {
            $dpp = DB::table('pengajuans')
                      ->join('provinsi', 'pengajuans.id_provinsi', '=', 'provinsi.id_provinsi')
                      ->join('kabkota', 'pengajuans.id_kabkota', '=', 'kabkota.id_kabkota')
                      ->join('kecamatan', 'pengajuans.id_kecamatan', '=', 'kecamatan.id_kecamatan')
                      ->join('keldes', 'pengajuans.id_keldes', '=', 'keldes.id_keldes')
                      ->select('pengajuans.*', 'provinsi.provinsi', 'kabkota.kabkota', 'kecamatan.kecamatan', 'keldes.keldes')
                      ->where([
                        ['status', '=', 'acc'],
                        ['tgl_surat', '=', $request->tanggal]
                      ])
                      ->orderByRaw('created_at DESC')->get();
            if ($dpp->isEmpty()) {
                return redirect('laporan/dpp')->with('warning', 'Data tidak ditemukan');
            }
        } else {
            $dpp = DB::table('pengajuans')
                      ->join('provinsi', 'pengajuans.id_provinsi', '=', 'provinsi.id_provinsi')
                      ->join('kabkota', 'pengajuans.id_kabkota', '=', 'kabkota.id_kabkota')
                      ->join('kecamatan', 'pengajuans.id_kecamatan', '=', 'kecamatan.id_kecamatan')
                      ->join('keldes', 'pengajuans.id_keldes', '=', 'keldes.id_keldes')
                      ->select('pengajuans.*', 'provinsi.provinsi', 'kabkota.kabkota', 'kecamatan.kecamatan', 'keldes.keldes')
                      ->where([
                        ['status', '=', 'acc'],
                        ['no_reg_asosiasi', '=', $request->no_reg_asosiasi],
                        ['tgl_surat', '=', $request->tanggal]
                      ])
                      ->orderByRaw('created_at DESC')
                      ->get();
            if ($dpp->isEmpty()) {
                return redirect('laporan/dpp')->with('warning', 'Data tidak ditemukan');
            }
        }

        $this->rowData = count($dpp)+5;
        $kualifikasi = SubKlasifikasi::all();

        // Create excel file
        $spreadsheet = new Spreadsheet();
        $objWorkSheet = $spreadsheet->getActiveSheet();

        $styleArray = array(
              'borders' => array(
                  'allBorders' => array(
                      'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                      'color' => array('argb' => '000000'),
                  ),
              ),
          );
        $objWorkSheet->getStyle('A5:M5')->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setRGB('68d471');

        $objWorkSheet->getStyle('N4:Y5')->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setRGB('d4d068');

        $objWorkSheet->getStyle('A5:Y'.$this->rowData)->applyFromArray($styleArray);
        $objWorkSheet->getStyle('N4:Y4')->applyFromArray($styleArray);
        $objWorkSheet->getStyle('AA5:AF'.$this->rowData)->applyFromArray($styleArray);

        $objWorkSheet->setTitle('DPP - APROVAL');
        $objWorkSheet->setCellValue('B2', 'Rekapitulasi DPP/APROVAL - '.$dpp[0]->tgl_surat)
        ->getStyle('B2')->getFont()->setSize(22)->getColor()->setRGB('68d471');

        $objWorkSheet->mergeCells('B2:F2');
        $objWorkSheet->mergeCells('V4:Y4');
        $objWorkSheet->mergeCells('N4:Q4');
        $objWorkSheet->mergeCells('R4:U4');
        $objWorkSheet->mergeCells('AA4:AB4');
        $objWorkSheet->mergeCells('AC4:AD4');
        $objWorkSheet->mergeCells('AE4:AF4');

        $objWorkSheet->setCellValue("N4", strtoupper("BARU / TAMBAH SUB BID 1"));
        $objWorkSheet->setCellValue("R4", strtoupper("BARU / TAMBAH SUB BID 2"));
        $objWorkSheet->setCellValue("V4", strtoupper("BARU / TAMBAH SUB BID 3"));

        $objWorkSheet->setCellValue('A5', strtoupper('No'));
        $objWorkSheet->setCellValue('B5', strtoupper('Asosiasi'));
        $objWorkSheet->setCellValue('C5', strtoupper('Nama'));
        $objWorkSheet->setCellValue('D5', strtoupper('No KTP'));
        $objWorkSheet->setCellValue('E5', strtoupper('No. NPWP'));
        $objWorkSheet->setCellValue('F5', strtoupper('Tempat Lahir'));
        $objWorkSheet->setCellValue('G5', strtoupper('Tanggal Lahir'));
        $objWorkSheet->setCellValue('H5', strtoupper('Propinsi Sesuai KTP'));
        $objWorkSheet->setCellValue('I5', strtoupper('Kabupaten Sesuai KTP'));
        $objWorkSheet->setCellValue('J5', strtoupper('Alamat'));
        $objWorkSheet->setCellValue('K5', strtoupper('Jurusan'));
        $objWorkSheet->setCellValue('L5', strtoupper('Jenjang_P'));
        $objWorkSheet->setCellValue('M5', strtoupper('HP_PEMOHON'));
        $objWorkSheet->setCellValue('N5', strtoupper('Klasifikasi'));
        $objWorkSheet->setCellValue('O5', strtoupper('Sub Klasifikasi'));
        $objWorkSheet->setCellValue('P5', strtoupper('Kode'));
        $objWorkSheet->setCellValue('Q5', strtoupper('Kualifikasi'));
        $objWorkSheet->setCellValue('R5', strtoupper('Klasifikasi'));
        $objWorkSheet->setCellValue('S5', strtoupper('Sub Klasifikasi'));
        $objWorkSheet->setCellValue('T5', strtoupper('Kode'));
        $objWorkSheet->setCellValue('U5', strtoupper('Kualifikasi'));
        $objWorkSheet->setCellValue('V5', strtoupper('Klasifikasi'));
        $objWorkSheet->setCellValue('W5', strtoupper('Sub Klasifikasi'));
        $objWorkSheet->setCellValue('X5', strtoupper('Kode'));
        $objWorkSheet->setCellValue('Y5', strtoupper('Kualifikasi'));

        $objWorkSheet->setCellValue('AA4', 'Sub Bidang 1');
        $objWorkSheet->setCellValue('AA5', 'Kualifikasi');
        $objWorkSheet->setCellValue('AB5', 'Harga');

        $objWorkSheet->setCellValue('AC4', 'Sub Bidang 2');
        $objWorkSheet->setCellValue('AC5', 'Kualifikasi');
        $objWorkSheet->setCellValue('AD5', 'Harga');

        $objWorkSheet->setCellValue('AE4', 'Sub Bidang 2');
        $objWorkSheet->setCellValue('AE5', 'Kualifikasi');
        $objWorkSheet->setCellValue('AF5', 'Harga');

        $a = 1;
        foreach ($dpp as $data) :
            $objWorkSheet->getColumnDimension('A')->setWidth(4);
            $objWorkSheet->getColumnDimension('B')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('C')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('D')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('E')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('F')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('G')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('H')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('I')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('J')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('K')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('L')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('M')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('N')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('O')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('P')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('Q')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('R')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('S')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('T')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('U')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('V')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('W')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('X')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('Y')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AA')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AB')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AC')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AD')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AE')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AF')->setAutoSize(true);

            $objWorkSheet->setCellValue('A'.(5+($a)), ($a));
            $objWorkSheet->setCellValue('B'.(5+($a)), asosiasi($data->no_reg_asosiasi));
            $objWorkSheet->setCellValue('C'.(5+($a)), strtoupper($data->nama_pemohon));
            $objWorkSheet->setCellValueExplicit(
                'D'.(5+($a)),
                $data->nik,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objWorkSheet->setCellValueExplicit(
                'E'.(5+($a)),
                $data->npwp,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objWorkSheet->setCellValue('F'.(5+($a)), strtoupper($data->tempat_lahir));
            $objWorkSheet->setCellValue('G'.(5+($a)), strtoupper($data->tgl_lahir));
            $objWorkSheet->setCellValue('H'.(5+($a)), strtoupper($data->provinsi));
            $objWorkSheet->setCellValue('I'.(5+($a)), strtoupper($data->kabkota));
            $objWorkSheet->setCellValue('J'.(5+($a)), strtoupper($data->alamat));
            $objWorkSheet->setCellValue('K'.(5+($a)), strtoupper($data->jurusan));
            $objWorkSheet->setCellValue('L'.(5+($a)), strtoupper($data->jenjang_pendidikan));
            $objWorkSheet->setCellValue(
                'M'.(5+($a)),
                $data->no_hp,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
              );

            $clear = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_subklasifikasi);
            $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
            $expTingkatan = explode(",", $tingkatan);
            $exp = explode(",", $clear);

            // Kualifikasi klasifikasi dan harga setoran ustkm 1
            foreach ($kualifikasi as $key) :
                if ($key->sk_id == $exp[0]) :
                    // Kualifikasi 1
                    $objWorkSheet->setCellValue('N'.(5+($a)), strtoupper($key->sk_klasifikasi));
                    $objWorkSheet->setCellValue('O'.(5+($a)), strtoupper($key->sk_nama));
                    $objWorkSheet->setCellValue('P'.(5+($a)), strtoupper($key->sk_kode));
                    $objWorkSheet->setCellValue('Q'.(5+($a)), kualifikasi($expTingkatan[0], $data->no_reg_asosiasi));
                    $objWorkSheet->setCellValue('AA'.(5+($a)), kualifikasi($expTingkatan[0], $data->no_reg_asosiasi));
                    $objWorkSheet->setCellValue('AB'.(5+($a)), hargaDpp($expTingkatan[0], $data->no_reg_asosiasi));
                endif;
            endforeach;

            // Kualifikasi klasifikasi dan harga setoran ustkm 2
            if (isset($exp[1])) {
                $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
                $expTingkatan = explode(",", $tingkatan);
                foreach ($kualifikasi as $key) :
                    if ($key->sk_id == $exp[1]) :
                    // Kualifikasi 2
                    $objWorkSheet->setCellValue('R'.(5+($a)), strtoupper($key->sk_klasifikasi));
                    $objWorkSheet->setCellValue('S'.(5+($a)), strtoupper($key->sk_nama));
                    $objWorkSheet->setCellValue('T'.(5+($a)), strtoupper($key->sk_kode));
                    $objWorkSheet->setCellValue('U'.(5+($a)), kualifikasi($expTingkatan[1], $data->no_reg_asosiasi));
                    $objWorkSheet->setCellValue('AC'.(5+($a)), kualifikasi($expTingkatan[1], $data->no_reg_asosiasi));
                    $objWorkSheet->setCellValue('AD'.(5+($a)), hargaDpp($expTingkatan[1], $data->no_reg_asosiasi));
                endif;
                endforeach;
            }
            // Kualifikasi klasifikasi dan harga setoran ustkm 3
            if (isset($exp[2])) :
                $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
                $expTingkatan = explode(",", $tingkatan);
                foreach ($kualifikasi as $key) :
                    if ($key->sk_id == $exp[2]) :
                        // Kualifikasi 2
                        $objWorkSheet->setCellValue('V'.(5+($a)), strtoupper($key->sk_klasifikasi));
                        $objWorkSheet->setCellValue('W'.(5+($a)), strtoupper($key->sk_nama));
                        $objWorkSheet->setCellValue('X'.(5+($a)), strtoupper($key->sk_kode));
                        $objWorkSheet->setCellValue('Y'.(5+($a)), kualifikasi($expTingkatan[2], $data->no_reg_asosiasi));
                        $objWorkSheet->setCellValue('AE'.(5+($a)), kualifikasi($expTingkatan[2], $data->no_reg_asosiasi));
                        $objWorkSheet->setCellValue('AF'.(5+($a)), hargaDpp($expTingkatan[2], $data->no_reg_asosiasi));
                    endif;
                endforeach;
            endif;
            $a++;
        endforeach;

        // _____________________________________________________________________
        // New sheet/Klasifikasi
        // AA = 1->Arsitektur
        // AS = 6->Sipil
        // AE = 3->Elektrikal
        // AM = 2->Mekanikal
        // AT = 4->TataLingkungan
        // AL = 5->Manajemen

        $Klasifikasi = array("AA", "AS", "AE", "AM", "AT", "AL");
        $klasifikasiId = array("1", "6", "3", "2", "4", "5");
        // Each array for title
        for ($x = 1; $x <= 6; $x++) :
            // Create excel file
            $spreadsheet->createSheet();
            $spreadsheet->setActiveSheetIndex($x);
            $objWorkSheet = $spreadsheet->getActiveSheet()->setTitle($Klasifikasi[$x-1]);

            $styleArray = array(
                  'borders' => array(
                      'allBorders' => array(
                          'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                          'color' => array('argb' => '000000'),
                      ),
                  ),
              );
            $objWorkSheet->getStyle('A5:M5')->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setRGB('68d471');

            $objWorkSheet->getStyle('N4:Y5')->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setRGB('d4d068');

            $objWorkSheet->getStyle('A5:Y'.$this->rowData)->applyFromArray($styleArray);
            $objWorkSheet->getStyle('N4:Y4')->applyFromArray($styleArray);
            $objWorkSheet->getStyle('AA5:AF'.$this->rowData)->applyFromArray($styleArray);

            $objWorkSheet->setCellValue('B2', 'Rekapitulasi DPP/APROVAL - '.$Klasifikasi[$x-1])
            ->getStyle('B2')->getFont()->setSize(22)->getColor()->setRGB('68d471');

            $objWorkSheet->mergeCells('B2:F2');
            $objWorkSheet->mergeCells('V4:Y4');
            $objWorkSheet->mergeCells('N4:Q4');
            $objWorkSheet->mergeCells('R4:U4');
            $objWorkSheet->mergeCells('AA4:AB4');
            $objWorkSheet->mergeCells('AC4:AD4');
            $objWorkSheet->mergeCells('AE4:AF4');

            $objWorkSheet->setCellValue("N4", strtoupper("BARU / TAMBAH SUB BID 1"));
            $objWorkSheet->setCellValue("R4", strtoupper("BARU / TAMBAH SUB BID 2"));
            $objWorkSheet->setCellValue("V4", strtoupper("BARU / TAMBAH SUB BID 3"));

            $objWorkSheet->setCellValue('A5', strtoupper('No'));
            $objWorkSheet->setCellValue('B5', strtoupper('Asosiasi'));
            $objWorkSheet->setCellValue('C5', strtoupper('Nama'));
            $objWorkSheet->setCellValue('D5', strtoupper('No KTP'));
            $objWorkSheet->setCellValue('E5', strtoupper('No. NPWP'));
            $objWorkSheet->setCellValue('F5', strtoupper('Tempat Lahir'));
            $objWorkSheet->setCellValue('G5', strtoupper('Tanggal Lahir'));
            $objWorkSheet->setCellValue('H5', strtoupper('Propinsi Sesuai KTP'));
            $objWorkSheet->setCellValue('I5', strtoupper('Kabupaten Sesuai KTP'));
            $objWorkSheet->setCellValue('J5', strtoupper('Alamat'));
            $objWorkSheet->setCellValue('K5', strtoupper('Jurusan'));
            $objWorkSheet->setCellValue('L5', strtoupper('Jenjang_P'));
            $objWorkSheet->setCellValue('M5', strtoupper('HP_PEMOHON'));
            $objWorkSheet->setCellValue('N5', strtoupper('Klasifikasi'));
            $objWorkSheet->setCellValue('O5', strtoupper('Sub Klasifikasi'));
            $objWorkSheet->setCellValue('P5', strtoupper('Kode'));
            $objWorkSheet->setCellValue('Q5', strtoupper('Kualifikasi'));
            $objWorkSheet->setCellValue('R5', strtoupper('Klasifikasi'));
            $objWorkSheet->setCellValue('S5', strtoupper('Sub Klasifikasi'));
            $objWorkSheet->setCellValue('T5', strtoupper('Kode'));
            $objWorkSheet->setCellValue('U5', strtoupper('Kualifikasi'));
            $objWorkSheet->setCellValue('V5', strtoupper('Klasifikasi'));
            $objWorkSheet->setCellValue('W5', strtoupper('Sub Klasifikasi'));
            $objWorkSheet->setCellValue('X5', strtoupper('Kode'));
            $objWorkSheet->setCellValue('Y5', strtoupper('Kualifikasi'));

            $objWorkSheet->setCellValue('AA4', 'Sub Bidang 1');
            $objWorkSheet->setCellValue('AA5', 'Kualifikasi');
            $objWorkSheet->setCellValue('AB5', 'Harga');

            $objWorkSheet->setCellValue('AC4', 'Sub Bidang 2');
            $objWorkSheet->setCellValue('AC5', 'Kualifikasi');
            $objWorkSheet->setCellValue('AD5', 'Harga');

            $objWorkSheet->setCellValue('AE4', 'Sub Bidang 2');
            $objWorkSheet->setCellValue('AE5', 'Kualifikasi');
            $objWorkSheet->setCellValue('AF5', 'Harga');

            $a = 1;
            foreach ($dpp as $data) :
                if($data->id_klasifikasi == $klasifikasiId[$x-1]) {
                    $objWorkSheet->getColumnDimension('A')->setWidth(4);
                    $objWorkSheet->getColumnDimension('B')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('C')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('D')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('E')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('F')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('G')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('H')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('I')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('J')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('K')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('L')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('M')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('N')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('O')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('P')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('Q')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('R')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('S')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('T')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('U')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('V')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('W')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('X')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('Y')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AA')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AB')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AC')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AD')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AE')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AF')->setAutoSize(true);

                    $objWorkSheet->setCellValue('A'.(5+($a)), ($a));
                    $objWorkSheet->setCellValue('B'.(5+($a)), asosiasi($data->no_reg_asosiasi));
                    $objWorkSheet->setCellValue('C'.(5+($a)), strtoupper($data->nama_pemohon));
                    $objWorkSheet->setCellValueExplicit(
                        'D'.(5+($a)),
                        $data->nik,
                        \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $objWorkSheet->setCellValueExplicit(
                        'E'.(5+($a)),
                        $data->npwp,
                        \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $objWorkSheet->setCellValue('F'.(5+($a)), strtoupper($data->tempat_lahir));
                    $objWorkSheet->setCellValue('G'.(5+($a)), strtoupper($data->tgl_lahir));
                    $objWorkSheet->setCellValue('H'.(5+($a)), strtoupper($data->provinsi));
                    $objWorkSheet->setCellValue('I'.(5+($a)), strtoupper($data->kabkota));
                    $objWorkSheet->setCellValue('J'.(5+($a)), strtoupper($data->alamat));
                    $objWorkSheet->setCellValue('K'.(5+($a)), strtoupper($data->jurusan));
                    $objWorkSheet->setCellValue('L'.(5+($a)), strtoupper($data->jenjang_pendidikan));
                    $objWorkSheet->setCellValue(
                        'M'.(5+($a)),
                        $data->no_hp,
                        \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                      );

                    $clear = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_subklasifikasi);
                    $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
                    $expTingkatan = explode(",", $tingkatan);
                    $exp = explode(",", $clear);

                    // Kualifikasi klasifikasi dan harga setoran dpp 1
                    foreach ($kualifikasi as $key) {
                        if ($key->sk_id == $exp[0]) :
                            // Kualifikasi 1
                            $objWorkSheet->setCellValue('N'.(5+($a)), strtoupper($key->sk_klasifikasi));
                            $objWorkSheet->setCellValue('O'.(5+($a)), strtoupper($key->sk_nama));
                            $objWorkSheet->setCellValue('P'.(5+($a)), strtoupper($key->sk_kode));
                            $objWorkSheet->setCellValue('Q'.(5+($a)), kualifikasi($expTingkatan[0], $data->no_reg_asosiasi));
                            $objWorkSheet->setCellValue('AA'.(5+($a)), kualifikasi($expTingkatan[0], $data->no_reg_asosiasi));
                            $objWorkSheet->setCellValue('AB'.(5+($a)), hargaDpp($expTingkatan[0], $data->no_reg_asosiasi));
                        endif;
                    }

                    // Kualifikasi klasifikasi dan harga setoran dpp 2
                    if (isset($exp[1])) {
                        $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
                        $expTingkatan = explode(",", $tingkatan);
                        foreach ($kualifikasi as $key) :
                            if ($key->sk_id == $exp[1]) :
                            // Kualifikasi 2
                            $objWorkSheet->setCellValue('R'.(5+($a)), strtoupper($key->sk_klasifikasi));
                            $objWorkSheet->setCellValue('S'.(5+($a)), strtoupper($key->sk_nama));
                            $objWorkSheet->setCellValue('T'.(5+($a)), strtoupper($key->sk_kode));
                            $objWorkSheet->setCellValue('U'.(5+($a)), kualifikasi($expTingkatan[1], $data->no_reg_asosiasi));
                            $objWorkSheet->setCellValue('AC'.(5+($a)), kualifikasi($expTingkatan[1], $data->no_reg_asosiasi));
                            $objWorkSheet->setCellValue('AD'.(5+($a)), hargaDpp($expTingkatan[1], $data->no_reg_asosiasi));
                        endif;
                        endforeach;
                    }
                    // Kualifikasi klasifikasi dan harga setoran dpp 3
                    if(isset($exp[2])) {
                        $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
                        $expTingkatan = explode(",", $tingkatan);
                        foreach ($kualifikasi as $key) :
                            if ($key->sk_id == $exp[2]) :
                                // Kualifikasi 2
                                $objWorkSheet->setCellValue('V'.(5+($a)), strtoupper($key->sk_klasifikasi));
                                $objWorkSheet->setCellValue('W'.(5+($a)), strtoupper($key->sk_nama));
                                $objWorkSheet->setCellValue('X'.(5+($a)), strtoupper($key->sk_kode));
                                $objWorkSheet->setCellValue('Y'.(5+($a)), kualifikasi($expTingkatan[2], $data->no_reg_asosiasi));
                                $objWorkSheet->setCellValue('AE'.(5+($a)), kualifikasi($expTingkatan[2], $data->no_reg_asosiasi));
                                $objWorkSheet->setCellValue('AF'.(5+($a)), hargaDpp($expTingkatan[2], $data->no_reg_asosiasi));
                            endif;
                        endforeach;
                    }
                $a++;
                }
            endforeach;
        endfor;

        $spreadsheet->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="Rekap DPP/APROVAL "'.$dpp[0]->tgl_surat.'".xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');
    }

    public function periode()
    {
        return view('laporan.priode');
    }

    public function getPeriode(Request $request)
    {

        if ($request->no_reg_asosiasi == 0) {

            $laporan = DB::table('pengajuans')
                      ->join('provinsi', 'pengajuans.id_provinsi', '=', 'provinsi.id_provinsi')
                      ->join('kabkota', 'pengajuans.id_kabkota', '=', 'kabkota.id_kabkota')
                      ->join('kecamatan', 'pengajuans.id_kecamatan', '=', 'kecamatan.id_kecamatan')
                      ->join('keldes', 'pengajuans.id_keldes', '=', 'keldes.id_keldes')
                      ->select('pengajuans.*', 'provinsi.provinsi', 'kabkota.kabkota', 'kecamatan.kecamatan', 'keldes.keldes')
                      ->where([
                        ['status', '=', 'acc']
                      ])
                      ->whereBetween('tgl_surat', [$request->mulai, $request->selesai])
                      ->orderByRaw('created_at DESC')->get();
            if ($laporan->isEmpty()) {
                return redirect('laporan/periode')->with('warning', 'Data tidak ditemukan');
            }
        } else {

            $laporan = DB::table('pengajuans')
                      ->join('provinsi', 'pengajuans.id_provinsi', '=', 'provinsi.id_provinsi')
                      ->join('kabkota', 'pengajuans.id_kabkota', '=', 'kabkota.id_kabkota')
                      ->join('kecamatan', 'pengajuans.id_kecamatan', '=', 'kecamatan.id_kecamatan')
                      ->join('keldes', 'pengajuans.id_keldes', '=', 'keldes.id_keldes')
                      ->select('pengajuans.*', 'provinsi.provinsi', 'kabkota.kabkota', 'kecamatan.kecamatan', 'keldes.keldes')
                      ->where([
                        ['status', '=', 'acc'],
                        ['no_reg_asosiasi', '=', $request->no_reg_asosiasi]
                      ])
                      ->whereBetween('tgl_surat', [$request->mulai, $request->selesai])
                      ->orderByRaw('created_at DESC')
                      ->get();
            if ($laporan->isEmpty()) {
                return redirect('laporan/periode')->with('warning', 'Data tidak ditemukan');
            }
        }


        $this->rowData = count($laporan)+5;
        if($request->laporan == 'ustkm') {
            $bigTitle = 'USTKM';
            $title = 'Rekapitulasi USTKM - '.$request->mulai.' s/d '.$request->selesai.'';
        } else {
            $bigTitle = 'DPP-APROVAL';
            $title = 'Rekapitulasi DPP/APROVAL - '.$request->mulai.' s/d '.$request->selesai.'';
        }
        $kualifikasi = SubKlasifikasi::all();

        // Create excel file
        $spreadsheet = new Spreadsheet();
        $objWorkSheet = $spreadsheet->getActiveSheet();

        $styleArray = array(
              'borders' => array(
                  'allBorders' => array(
                      'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                      'color' => array('argb' => '000000'),
                  ),
              ),
          );
        $objWorkSheet->getStyle('A5:M5')->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setRGB('68d471');

        $objWorkSheet->getStyle('N4:Y5')->getFill()
                    ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                    ->getStartColor()->setRGB('d4d068');

        $objWorkSheet->getStyle('A5:Y'.$this->rowData)->applyFromArray($styleArray);
        $objWorkSheet->getStyle('N4:Y4')->applyFromArray($styleArray);
        $objWorkSheet->getStyle('AA5:AF'.$this->rowData)->applyFromArray($styleArray);

        $objWorkSheet->setTitle($bigTitle);
        $objWorkSheet->setCellValue('B2', $title)
        ->getStyle('B2')->getFont()->setSize(22)->getColor()->setRGB('68d471');

        $objWorkSheet->mergeCells('B2:F2');
        $objWorkSheet->mergeCells('V4:Y4');
        $objWorkSheet->mergeCells('N4:Q4');
        $objWorkSheet->mergeCells('R4:U4');
        $objWorkSheet->mergeCells('AA4:AB4');
        $objWorkSheet->mergeCells('AC4:AD4');
        $objWorkSheet->mergeCells('AE4:AF4');

        $objWorkSheet->setCellValue("N4", strtoupper("BARU / TAMBAH SUB BID 1"));
        $objWorkSheet->setCellValue("R4", strtoupper("BARU / TAMBAH SUB BID 2"));
        $objWorkSheet->setCellValue("V4", strtoupper("BARU / TAMBAH SUB BID 3"));

        $objWorkSheet->setCellValue('A5', strtoupper('No'));
        $objWorkSheet->setCellValue('B5', strtoupper('Asosiasi'));
        $objWorkSheet->setCellValue('C5', strtoupper('Nama'));
        $objWorkSheet->setCellValue('D5', strtoupper('No KTP'));
        $objWorkSheet->setCellValue('E5', strtoupper('No. NPWP'));
        $objWorkSheet->setCellValue('F5', strtoupper('Tempat Lahir'));
        $objWorkSheet->setCellValue('G5', strtoupper('Tanggal Lahir'));
        $objWorkSheet->setCellValue('H5', strtoupper('Propinsi Sesuai KTP'));
        $objWorkSheet->setCellValue('I5', strtoupper('Kabupaten Sesuai KTP'));
        $objWorkSheet->setCellValue('J5', strtoupper('Alamat'));
        $objWorkSheet->setCellValue('K5', strtoupper('Jurusan'));
        $objWorkSheet->setCellValue('L5', strtoupper('Jenjang_P'));
        $objWorkSheet->setCellValue('M5', strtoupper('HP_PEMOHON'));
        $objWorkSheet->setCellValue('N5', strtoupper('Klasifikasi'));
        $objWorkSheet->setCellValue('O5', strtoupper('Sub Klasifikasi'));
        $objWorkSheet->setCellValue('P5', strtoupper('Kode'));
        $objWorkSheet->setCellValue('Q5', strtoupper('Kualifikasi'));
        $objWorkSheet->setCellValue('R5', strtoupper('Klasifikasi'));
        $objWorkSheet->setCellValue('S5', strtoupper('Sub Klasifikasi'));
        $objWorkSheet->setCellValue('T5', strtoupper('Kode'));
        $objWorkSheet->setCellValue('U5', strtoupper('Kualifikasi'));
        $objWorkSheet->setCellValue('V5', strtoupper('Klasifikasi'));
        $objWorkSheet->setCellValue('W5', strtoupper('Sub Klasifikasi'));
        $objWorkSheet->setCellValue('X5', strtoupper('Kode'));
        $objWorkSheet->setCellValue('Y5', strtoupper('Kualifikasi'));

        $objWorkSheet->setCellValue('AA4', 'Sub Bidang 1');
        $objWorkSheet->setCellValue('AA5', 'Kualifikasi');
        $objWorkSheet->setCellValue('AB5', 'Harga');

        $objWorkSheet->setCellValue('AC4', 'Sub Bidang 2');
        $objWorkSheet->setCellValue('AC5', 'Kualifikasi');
        $objWorkSheet->setCellValue('AD5', 'Harga');

        $objWorkSheet->setCellValue('AE4', 'Sub Bidang 2');
        $objWorkSheet->setCellValue('AE5', 'Kualifikasi');
        $objWorkSheet->setCellValue('AF5', 'Harga');

        $a = 1;
        foreach ($laporan as $data) :
            $objWorkSheet->getColumnDimension('A')->setWidth(4);
            $objWorkSheet->getColumnDimension('B')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('C')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('D')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('E')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('F')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('G')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('H')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('I')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('J')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('K')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('L')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('M')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('N')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('O')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('P')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('Q')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('R')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('S')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('T')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('U')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('V')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('W')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('X')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('Y')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AA')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AB')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AC')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AD')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AE')->setAutoSize(true);
            $objWorkSheet->getColumnDimension('AF')->setAutoSize(true);

            $objWorkSheet->setCellValue('A'.(5+($a)), ($a));
            $objWorkSheet->setCellValue('B'.(5+($a)), asosiasi($data->no_reg_asosiasi));
            $objWorkSheet->setCellValue('C'.(5+($a)), strtoupper($data->nama_pemohon));
            $objWorkSheet->setCellValueExplicit(
                'D'.(5+($a)),
                $data->nik,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objWorkSheet->setCellValueExplicit(
                'E'.(5+($a)),
                $data->npwp,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
            $objWorkSheet->setCellValue('F'.(5+($a)), strtoupper($data->tempat_lahir));
            $objWorkSheet->setCellValue('G'.(5+($a)), strtoupper($data->tgl_lahir));
            $objWorkSheet->setCellValue('H'.(5+($a)), strtoupper($data->provinsi));
            $objWorkSheet->setCellValue('I'.(5+($a)), strtoupper($data->kabkota));
            $objWorkSheet->setCellValue('J'.(5+($a)), strtoupper($data->alamat));
            $objWorkSheet->setCellValue('K'.(5+($a)), strtoupper($data->jurusan));
            $objWorkSheet->setCellValue('L'.(5+($a)), strtoupper($data->jenjang_pendidikan));
            $objWorkSheet->setCellValue(
                'M'.(5+($a)),
                $data->no_hp,
                \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
              );

            $clear = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_subklasifikasi);
            $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
            $expTingkatan = explode(",", $tingkatan);
            $exp = explode(",", $clear);

            // Kualifikasi klasifikasi dan harga setoran ustkm 1
            foreach ($kualifikasi as $key) :
                if ($key->sk_id == $exp[0]) :
                    // Kualifikasi 1
                    $objWorkSheet->setCellValue('N'.(5+($a)), strtoupper($key->sk_klasifikasi));
                    $objWorkSheet->setCellValue('O'.(5+($a)), strtoupper($key->sk_nama));
                    $objWorkSheet->setCellValue('P'.(5+($a)), strtoupper($key->sk_kode));
                    $objWorkSheet->setCellValue('Q'.(5+($a)), kualifikasi($expTingkatan[0], $data->no_reg_asosiasi));
                    $objWorkSheet->setCellValue('AA'.(5+($a)), kualifikasi($expTingkatan[0], $data->no_reg_asosiasi));
                    if($request->laporan == 'ustkm') {
                        $objWorkSheet->setCellValue('AB'.(5+($a)), hargaUstkm($expTingkatan[0], $data->no_reg_asosiasi));
                    } else {
                        $objWorkSheet->setCellValue('AB'.(5+($a)), hargaDpp($expTingkatan[0], $data->no_reg_asosiasi));
                    }

                endif;
            endforeach;

            // Kualifikasi klasifikasi dan harga setoran ustkm 2
            if (isset($exp[1])) {
                $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
                $expTingkatan = explode(",", $tingkatan);
                foreach ($kualifikasi as $key) :
                    if ($key->sk_id == $exp[1]) :
                    // Kualifikasi 2
                    $objWorkSheet->setCellValue('R'.(5+($a)), strtoupper($key->sk_klasifikasi));
                    $objWorkSheet->setCellValue('S'.(5+($a)), strtoupper($key->sk_nama));
                    $objWorkSheet->setCellValue('T'.(5+($a)), strtoupper($key->sk_kode));
                    $objWorkSheet->setCellValue('U'.(5+($a)), kualifikasi($expTingkatan[1], $data->no_reg_asosiasi));
                    $objWorkSheet->setCellValue('AC'.(5+($a)), kualifikasi($expTingkatan[1], $data->no_reg_asosiasi));
                    if($request->laporan == 'ustkm') {
                        $objWorkSheet->setCellValue('AD'.(5+($a)), hargaUstkm($expTingkatan[1], $data->no_reg_asosiasi));
                    } else {
                        $objWorkSheet->setCellValue('AD'.(5+($a)), hargaDpp($expTingkatan[1], $data->no_reg_asosiasi));
                    }
                endif;
                endforeach;
            }
            // Kualifikasi klasifikasi dan harga setoran ustkm 3
            if (isset($exp[2])) :
                $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
                $expTingkatan = explode(",", $tingkatan);
                foreach ($kualifikasi as $key) :
                    if ($key->sk_id == $exp[2]) :
                        // Kualifikasi 2
                        $objWorkSheet->setCellValue('V'.(5+($a)), strtoupper($key->sk_klasifikasi));
                        $objWorkSheet->setCellValue('W'.(5+($a)), strtoupper($key->sk_nama));
                        $objWorkSheet->setCellValue('X'.(5+($a)), strtoupper($key->sk_kode));
                        $objWorkSheet->setCellValue('Y'.(5+($a)), kualifikasi($expTingkatan[2], $data->no_reg_asosiasi));
                        $objWorkSheet->setCellValue('AE'.(5+($a)), kualifikasi($expTingkatan[2], $data->no_reg_asosiasi));
                        if($request->laporan == 'ustkm') {
                            $objWorkSheet->setCellValue('AF'.(5+($a)), hargaUstkm($expTingkatan[2], $data->no_reg_asosiasi));
                        } else {
                            $objWorkSheet->setCellValue('AF'.(5+($a)), hargaDpp($expTingkatan[2], $data->no_reg_asosiasi));
                        }
                    endif;
                endforeach;
            endif;
            $a++;
        endforeach;

        // _____________________________________________________________________
        // New sheet/Klasifikasi
        // AA = 1->Arsitektur
        // AS = 6->Sipil
        // AE = 3->Elektrikal
        // AM = 2->Mekanikal
        // AT = 4->TataLingkungan
        // AL = 5->Manajemen

        $Klasifikasi = array("AA", "AS", "AE", "AM", "AT", "AL");
        $klasifikasiId = array("1", "6", "3", "2", "4", "5");
        // Each array for title
        for ($x = 1; $x <= 6; $x++) :
            // Create excel file
            $spreadsheet->createSheet();
            $spreadsheet->setActiveSheetIndex($x);
            $objWorkSheet = $spreadsheet->getActiveSheet()->setTitle($Klasifikasi[$x-1]);

            $styleArray = array(
                  'borders' => array(
                      'allBorders' => array(
                          'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THICK,
                          'color' => array('argb' => '000000'),
                      ),
                  ),
              );
            $objWorkSheet->getStyle('A5:M5')->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setRGB('68d471');

            $objWorkSheet->getStyle('N4:Y5')->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setRGB('d4d068');

            $objWorkSheet->getStyle('A5:Y'.$this->rowData)->applyFromArray($styleArray);
            $objWorkSheet->getStyle('N4:Y4')->applyFromArray($styleArray);
            $objWorkSheet->getStyle('AA5:AF'.$this->rowData)->applyFromArray($styleArray);

            $objWorkSheet->setCellValue('B2', 'Rekapitulasi DPP/APROVAL - '.$Klasifikasi[$x-1])
            ->getStyle('B2')->getFont()->setSize(22)->getColor()->setRGB('68d471');

            $objWorkSheet->mergeCells('B2:F2');
            $objWorkSheet->mergeCells('V4:Y4');
            $objWorkSheet->mergeCells('N4:Q4');
            $objWorkSheet->mergeCells('R4:U4');
            $objWorkSheet->mergeCells('AA4:AB4');
            $objWorkSheet->mergeCells('AC4:AD4');
            $objWorkSheet->mergeCells('AE4:AF4');

            $objWorkSheet->setCellValue("N4", strtoupper("BARU / TAMBAH SUB BID 1"));
            $objWorkSheet->setCellValue("R4", strtoupper("BARU / TAMBAH SUB BID 2"));
            $objWorkSheet->setCellValue("V4", strtoupper("BARU / TAMBAH SUB BID 3"));

            $objWorkSheet->setCellValue('A5', strtoupper('No'));
            $objWorkSheet->setCellValue('B5', strtoupper('Asosiasi'));
            $objWorkSheet->setCellValue('C5', strtoupper('Nama'));
            $objWorkSheet->setCellValue('D5', strtoupper('No KTP'));
            $objWorkSheet->setCellValue('E5', strtoupper('No. NPWP'));
            $objWorkSheet->setCellValue('F5', strtoupper('Tempat Lahir'));
            $objWorkSheet->setCellValue('G5', strtoupper('Tanggal Lahir'));
            $objWorkSheet->setCellValue('H5', strtoupper('Propinsi Sesuai KTP'));
            $objWorkSheet->setCellValue('I5', strtoupper('Kabupaten Sesuai KTP'));
            $objWorkSheet->setCellValue('J5', strtoupper('Alamat'));
            $objWorkSheet->setCellValue('K5', strtoupper('Jurusan'));
            $objWorkSheet->setCellValue('L5', strtoupper('Jenjang_P'));
            $objWorkSheet->setCellValue('M5', strtoupper('HP_PEMOHON'));
            $objWorkSheet->setCellValue('N5', strtoupper('Klasifikasi'));
            $objWorkSheet->setCellValue('O5', strtoupper('Sub Klasifikasi'));
            $objWorkSheet->setCellValue('P5', strtoupper('Kode'));
            $objWorkSheet->setCellValue('Q5', strtoupper('Kualifikasi'));
            $objWorkSheet->setCellValue('R5', strtoupper('Klasifikasi'));
            $objWorkSheet->setCellValue('S5', strtoupper('Sub Klasifikasi'));
            $objWorkSheet->setCellValue('T5', strtoupper('Kode'));
            $objWorkSheet->setCellValue('U5', strtoupper('Kualifikasi'));
            $objWorkSheet->setCellValue('V5', strtoupper('Klasifikasi'));
            $objWorkSheet->setCellValue('W5', strtoupper('Sub Klasifikasi'));
            $objWorkSheet->setCellValue('X5', strtoupper('Kode'));
            $objWorkSheet->setCellValue('Y5', strtoupper('Kualifikasi'));

            $objWorkSheet->setCellValue('AA4', 'Sub Bidang 1');
            $objWorkSheet->setCellValue('AA5', 'Kualifikasi');
            $objWorkSheet->setCellValue('AB5', 'Harga');

            $objWorkSheet->setCellValue('AC4', 'Sub Bidang 2');
            $objWorkSheet->setCellValue('AC5', 'Kualifikasi');
            $objWorkSheet->setCellValue('AD5', 'Harga');

            $objWorkSheet->setCellValue('AE4', 'Sub Bidang 2');
            $objWorkSheet->setCellValue('AE5', 'Kualifikasi');
            $objWorkSheet->setCellValue('AF5', 'Harga');

            $a = 1;
            foreach ($laporan as $data) :
                if($data->id_klasifikasi == $klasifikasiId[$x-1]) {
                    $objWorkSheet->getColumnDimension('A')->setWidth(4);
                    $objWorkSheet->getColumnDimension('B')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('C')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('D')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('E')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('F')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('G')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('H')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('I')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('J')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('K')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('L')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('M')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('N')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('O')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('P')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('Q')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('R')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('S')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('T')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('U')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('V')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('W')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('X')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('Y')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AA')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AB')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AC')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AD')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AE')->setAutoSize(true);
                    $objWorkSheet->getColumnDimension('AF')->setAutoSize(true);

                    $objWorkSheet->setCellValue('A'.(5+($a)), ($a));
                    $objWorkSheet->setCellValue('B'.(5+($a)), asosiasi($data->no_reg_asosiasi));
                    $objWorkSheet->setCellValue('C'.(5+($a)), strtoupper($data->nama_pemohon));
                    $objWorkSheet->setCellValueExplicit(
                        'D'.(5+($a)),
                        $data->nik,
                        \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $objWorkSheet->setCellValueExplicit(
                        'E'.(5+($a)),
                        $data->npwp,
                        \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING);
                    $objWorkSheet->setCellValue('F'.(5+($a)), strtoupper($data->tempat_lahir));
                    $objWorkSheet->setCellValue('G'.(5+($a)), strtoupper($data->tgl_lahir));
                    $objWorkSheet->setCellValue('H'.(5+($a)), strtoupper($data->provinsi));
                    $objWorkSheet->setCellValue('I'.(5+($a)), strtoupper($data->kabkota));
                    $objWorkSheet->setCellValue('J'.(5+($a)), strtoupper($data->alamat));
                    $objWorkSheet->setCellValue('K'.(5+($a)), strtoupper($data->jurusan));
                    $objWorkSheet->setCellValue('L'.(5+($a)), strtoupper($data->jenjang_pendidikan));
                    $objWorkSheet->setCellValue(
                        'M'.(5+($a)),
                        $data->no_hp,
                        \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_STRING
                      );

                    $clear = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_subklasifikasi);
                    $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
                    $expTingkatan = explode(",", $tingkatan);
                    $exp = explode(",", $clear);

                    // Kualifikasi klasifikasi dan harga setoran dpp 1
                    foreach ($kualifikasi as $key) {
                        if ($key->sk_id == $exp[0]) :
                            // Kualifikasi 1
                            $objWorkSheet->setCellValue('N'.(5+($a)), strtoupper($key->sk_klasifikasi));
                            $objWorkSheet->setCellValue('O'.(5+($a)), strtoupper($key->sk_nama));
                            $objWorkSheet->setCellValue('P'.(5+($a)), strtoupper($key->sk_kode));
                            $objWorkSheet->setCellValue('Q'.(5+($a)), kualifikasi($expTingkatan[0], $data->no_reg_asosiasi));
                            $objWorkSheet->setCellValue('AA'.(5+($a)), kualifikasi($expTingkatan[0], $data->no_reg_asosiasi));
                            if($request->laporan == 'ustkm') {
                                $objWorkSheet->setCellValue('AB'.(5+($a)), hargaUstkm($expTingkatan[0], $data->no_reg_asosiasi));
                            } else {
                                $objWorkSheet->setCellValue('AB'.(5+($a)), hargaDpp($expTingkatan[0], $data->no_reg_asosiasi));
                            }
                        endif;
                    }

                    // Kualifikasi klasifikasi dan harga setoran dpp 2
                    if (isset($exp[1])) {
                        $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
                        $expTingkatan = explode(",", $tingkatan);
                        foreach ($kualifikasi as $key) :
                            if ($key->sk_id == $exp[1]) :
                            // Kualifikasi 2
                            $objWorkSheet->setCellValue('R'.(5+($a)), strtoupper($key->sk_klasifikasi));
                            $objWorkSheet->setCellValue('S'.(5+($a)), strtoupper($key->sk_nama));
                            $objWorkSheet->setCellValue('T'.(5+($a)), strtoupper($key->sk_kode));
                            $objWorkSheet->setCellValue('U'.(5+($a)), kualifikasi($expTingkatan[1], $data->no_reg_asosiasi));
                            $objWorkSheet->setCellValue('AC'.(5+($a)), kualifikasi($expTingkatan[1], $data->no_reg_asosiasi));
                            if($request->laporan == 'ustkm') {
                                $objWorkSheet->setCellValue('AD'.(5+($a)), hargaUstkm($expTingkatan[1], $data->no_reg_asosiasi));
                            } else {
                                $objWorkSheet->setCellValue('AD'.(5+($a)), hargaDpp($expTingkatan[1], $data->no_reg_asosiasi));
                            }
                        endif;
                        endforeach;
                    }
                    // Kualifikasi klasifikasi dan harga setoran dpp 3
                    if(isset($exp[2])) {
                        $tingkatan = preg_replace("/[^A-Za-z0-9,]/", '', $data->id_kualifikasi);
                        $expTingkatan = explode(",", $tingkatan);
                        foreach ($kualifikasi as $key) :
                            if ($key->sk_id == $exp[2]) :
                                // Kualifikasi 2
                                $objWorkSheet->setCellValue('V'.(5+($a)), strtoupper($key->sk_klasifikasi));
                                $objWorkSheet->setCellValue('W'.(5+($a)), strtoupper($key->sk_nama));
                                $objWorkSheet->setCellValue('X'.(5+($a)), strtoupper($key->sk_kode));
                                $objWorkSheet->setCellValue('Y'.(5+($a)), kualifikasi($expTingkatan[2], $data->no_reg_asosiasi));
                                $objWorkSheet->setCellValue('AE'.(5+($a)), kualifikasi($expTingkatan[2], $data->no_reg_asosiasi));
                                if($request->laporan == 'ustkm') {
                                    $objWorkSheet->setCellValue('AF'.(5+($a)), hargaUstkm($expTingkatan[2], $data->no_reg_asosiasi));
                                } else {
                                    $objWorkSheet->setCellValue('AF'.(5+($a)), hargaDpp($expTingkatan[2], $data->no_reg_asosiasi));
                                }
                            endif;
                        endforeach;
                    }
                $a++;
                }
            endforeach;
        endfor;

        $spreadsheet->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$title.'.xls"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');

    }
}
