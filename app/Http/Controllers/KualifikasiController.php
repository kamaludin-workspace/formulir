<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kualifikasi;

class KualifikasiController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getcualification($id)
    {
      $kualifikasi = Kualifikasi::where('kul_js_id', $id)->pluck('kul_id', 'kul_nama');
      return json_encode($kualifikasi);
    }


}
