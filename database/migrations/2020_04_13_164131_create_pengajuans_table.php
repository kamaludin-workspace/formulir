<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengajuansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuans', function (Blueprint $table) {
            $table->id();
            $table->enum('tipe_permohonan', ['ska', 'skt']);
            $table->string('nama_pemohon');
            $table->string('tempat_lahir');
            $table->date('tgl_lahir');
            $table->string('alamat');
            $table->integer('id_provinsi');
            $table->integer('id_kabkota');
            $table->integer('id_kecamatan');
            $table->integer('id_keldes');
            $table->string('wilayah_kalimat')->nullable();
            $table->string('nik');
            $table->string('npwp')->nullable();
            $table->string('no_hp')->nullable();
            $table->string('email')->nullable();
            $table->string('no_reg_asosiasi')->nullable();
            $table->string('jenis_permohonan');
            $table->string('cv');
            // Pendidikan
            $table->string('jenjang_pendidikan');
            $table->string('jurusan');
            $table->string('nama_sekolah');
            $table->string('alamat_sekolah');
            $table->integer('tahun_lulus');
            $table->string('nomor_ijazah');
            // Kualifikasi
            $table->string('id_klasifikasi');
            $table->jsonb('id_subklasifikasi');
            $table->jsonb('id_kualifikasi');
            $table->jsonb('keterangan');
            $table->enum('status',['acc','menunggu']);
            $table->string('kode_asosiasi')->nullable();
            $table->string('pemeriksa')->nullable();
            $table->string('nomor_surat')->nullable();
            $table->string('tgl_surat')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuans');
    }
}
