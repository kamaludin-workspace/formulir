<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePekerjaansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pekerjaans', function (Blueprint $table) {
            $table->id();
            $table->integer('id_pengajuan');
            $table->string('rp_subKlas');
            $table->string('rp_namaProyek');
            $table->string('rp_lokasiProp');
            $table->string('rp_kodeLok');
            $table->string('rp_nilai');
            $table->date('rp_tglMulai');
            $table->date('rp_tglSelesai');
            $table->string('rp_tahun');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pekerjaans');
    }
}
