/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 *
 */

"use strict";

$('#pasteDate').on('click', function() {
    const getInput = document.querySelector('#tgl_lahir');
    if (getInput.getAttribute('type') === 'date') {
        getInput.setAttribute('type', 'text');
    } else {
        getInput.setAttribute('type', 'date');
    }
});