// Change kualifikasi
$(document).ready(function() {
    $('input[type=radio][name=tipe_permohonan]').on('change', function() {
        $('select[name="id_klasifikasi"]').val("pilih_klasifikasi");
        $('select[name="id_subklasifikasi[]"]').empty();
        $('select[name="id_subklasifikasi[]"]').append('<option>Kosong</option>');
        $('select[name="id_klasifikasi"]').prop("disabled", false);
        let permohonanId = $(this).val();
        if (permohonanId == 1) {
            window.jenis = "SKA";
        } else if (permohonanId == 2) {
            window.jenis = "SKT";
        }
        if (permohonanId) {
            jQuery.ajax({
                url: tingkat+permohonanId,
                type: "GET",
                dataType: "json",
                success: function(data) {
                    $('select[name="id_kualifikasi[]"]').empty();
                    $('select[name="id_kualifikasi[]"]').append('<option value="">Pilih kualifikasi</option>');
                    $.each(data, function(key, value) {
                        $('select[name="id_kualifikasi[]"]').append(
                            '<option value="' + value.id + '">' + value.nama + "/DPP-" + new Intl.NumberFormat().format(value.potongan1) + "/USKTM-" + new Intl.NumberFormat().format(value.potongan2) + "</option>"
                        );
                    });
                },
            });

        } else {
            $('select[name="tipe_permohonan"]').empty();
        }
    });
});

$(document).ready(function() {
    $('select[name="id_klasifikasi"]').on('change', function() {
        let tipe = window.jenis;
        let klasifikasiId = $(this).val();
        if (klasifikasiId) {
            jQuery.ajax({
                url: "/klasifikasi/" + klasifikasiId + "/subklasifikasi/" + tipe,
                type: "GET",
                dataType: "json",
                success: function(data) {
                    $('select[name="id_subklasifikasi[]"]').empty();
                    $('select[name="id_subklasifikasi[]"]').append('<option value="">Pilih sub</option>');
                    $.each(data, function (key, value) {
                        $('select[name="id_subklasifikasi[]"]').append('<option value="' + value.sk_id + '">' + value.sk_kode + "/" + value.sk_nama + "</option>");
                    });
                },
            });
        } else {
            $('select[name="tipe_permohonan"]').empty();
        }
    });
});
$('select[name="id_provinsi"]').on('change', function() {
    $('select[name="id_kecamatan"]').empty();
    $('select[name="id_keldes"]').empty();
});
$('select[name="id_kabkota"]').on('change', function() {
    $('select[name="id_keldes"]').empty();
});
$(document).ready(function() {
    $('select[name="id_provinsi"]').on('change', function() {
        $("option[class='pilih_provinsi']").remove();
        let provinceId = $(this).val();
        if (provinceId) {
            jQuery.ajax({
                url: '/daerah/kabupaten/' + provinceId,
                type: "GET",
                dataType: "json",
                success: function(data) {
                    $('select[name="id_kabkota"]').empty();
                    $('select[name="id_kabkota"]').append('<option value="pilih" class="pilih_kabupaten">Pilih Kabupaten</option>');
                    $.each(data, function(key, value) {
                        $('select[name="id_kabkota"]').append('<option value="' + value + '">' + key + '</option>');
                    });
                },
            });
        } else {
            $('select[name="id_provinsi"]').empty();
        }
    });
});
$(document).ready(function() {
    $('select[name="id_kabkota"]').on('change', function() {
        $("option[class='pilih_kabupaten']").remove();
        let kabupatenId = $(this).val();
        if (kabupatenId) {
            jQuery.ajax({
                url: '/daerah/kecamatan/' + kabupatenId,
                type: "GET",
                dataType: "json",
                success: function(data) {
                    $('select[name="id_kecamatan"]').empty();
                    $('select[name="id_kecamatan"]').append('<option value="pilih" class="pilih_kecamatan">Pilih Kecamatan</option>');
                    $.each(data, function(key, value) {
                        $('select[name="id_kecamatan"]').append('<option value="' + value + '">' + key + '</option>');
                    });
                },
            });
        } else {
            $('select[name="id_kabkota"]').empty();
        }
    });
});
$(document).ready(function() {
    $('select[name="id_kecamatan"]').on('change', function() {
        $("option[class='pilih_kecamatan']").remove();
        let kabupatenId = $(this).val();
        if (kabupatenId) {
            jQuery.ajax({
                url: '/daerah/kelurahan/' + kabupatenId,
                type: "GET",
                dataType: "json",
                success: function(data) {
                    $('select[name="id_keldes"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="id_keldes"]').append('<option value="' + value + '">' + key + '</option>');
                    });
                },
            });
        } else {
            $('select[name="id_kecamatan"]').empty();
        }
    });
});

function numberFilter(evt) {
    let charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

$(".getId").click(function() {
    $("#dataRow" + this.id).remove();
});

$(document).ready(function() {
    $('#addElement').click(function() {
        if($('#secondElement').children().length == 0) {
            // Create second element
            $('#secondElement').append('<div class="form-group col-4"><label for="">Sub klasifikasi 2</label> <select class="form-control select2" name="id_subklasifikasi[]" required><option>Subkualifikasi</option></select></div>');
            $('.select2').select2();
            $('#secondElement').append('<div class="form-group col-5"> <label>Kualifikasi 2</label> <select class="form-control" name="id_kualifikasi[]" required> <option value="">Kualifikasi</option> </select> </div>');
            $('#secondElement').append('<div class="form-group col-2"> <label>Keterangan 2</label> <input type="text" name="keterangan[]" class="form-control keterangan" value="Baru"> </div>');
            $('#secondElement').append('<div class="col-auto text-right"> <label>Hapus</label><br><button type="button" class="btn btn-icon btn-danger text-white" id="removeSecond"><li class="fa fa-trash"></li></button> </div>');
        } else if($('#thirdElement').children().length == 0) {
            // Create thir element
            $('#thirdElement').append('<div class="form-group col-4"><label for="">Sub klasifikasi 3</label> <select class="form-control select2" name="id_subklasifikasi[]" required><option>Subkualifikasi</option></select></div>');
            $('.select2').select2();
            $('#thirdElement').append('<div class="form-group col-5"> <label>Kualifikasi 3</label> <select class="form-control" name="id_kualifikasi[]" required> <option value="">Kualifikasi</option> </select> </div>');
            $('#thirdElement').append('<div class="form-group col-2"> <label>Keterangan 3</label> <input type="text" name="keterangan[]" class="form-control keterangan" value="Baru"> </div>');
            $('#thirdElement').append('<div class="col-auto text-right"> <label>Hapus</label><br><button type="button" class="btn btn-icon btn-danger text-white" id="removeThird"><li class="fa fa-trash"></li></button> </div>');
            $('#addElement').prop("disabled", true);
        } else {
            $('#addElement').prop("disabled", true);
        }
    });
    $("body").delegate("#removeSecond", "click", function(){
        $('#secondElement').empty();
        $('#addElement').prop("disabled", false);
    });
    $("body").delegate("#removeThird", "click", function(){
        $('#thirdElement').empty();
        $('#addElement').prop("disabled", false);
    });
});